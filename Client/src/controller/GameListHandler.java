package controller;

import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.SwingUtilities;

import model.gameList.GameList;
import network.NetworkClient;
import view.GameListPage;
/**
 * This class is the handler of the game list view, manage user interaction and send the corresponding message to the server.
 *
 */
public class GameListHandler implements ServerEventHandler {

    private GameList model;
    private GameListPage view;
    private int idSelectedGame;
    private String nameCreatedGame;

    public GameListHandler(String[] information) {
        this.model = new GameList();
        this.view = new GameListPage(this, this.model);
        for (String arguments : information) {
            String[] args = arguments.split(";");
            this.model.addGame(Integer.parseInt(args[0]),
                    args[1],
                    Integer.parseInt(args[2]),
                    args[3].equals("Playing") ? "En jeu" : "En attente");
        }
        this.setIdSelectedGame(-1);
        this.view.notifyGameListChanged();
        NetworkClient.getInstance().setCurrentHandler(this);
    }

    public static void createGameListHandler(String[] data) {
        GameListHandler controllerGameList = new GameListHandler(data);
        LauncherClient.getMainFrame().setContentPane(controllerGameList.getView());
        LauncherClient.getMainFrame().pack();
        LauncherClient.getMainFrame().setVisible(true);
        LauncherClient.getMainFrame().setJMenuBar(null);
    }

    /**
     * Allow the user to join a party
     */
    public void join() {
        if (this.isGameJoinable()) {
            System.out.println("Connexion à la partie");
            NetworkClient.getInstance().write("JoinGame#" + this.idSelectedGame);
        }

    }

    /**
     * Allow the user to quit the lobby, alerting the server that he leaves
     */
    public void quitter() {
        NetworkClient.getInstance().disconnect();
        SwingUtilities.getWindowAncestor(this.view).dispatchEvent(new WindowEvent(SwingUtilities.getWindowAncestor(this.view), WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void processMessage(String message) {
        String[] splitedHashtag = message.split("#");
        String[] data = Arrays.copyOfRange(splitedHashtag, 1, splitedHashtag.length);
        String instruction = splitedHashtag[0];

        switch (instruction) {
            case "LobbyCreateGame":
                this.model.addGame(Integer.parseInt(data[0]), data[1], 1, "En attente");
                this.view.notifyGameListChanged();
                break;

            case "LobbyPlayerChanged":
                this.model.updateGame(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
                if (Integer.parseInt(data[0]) == this.idSelectedGame && Integer.parseInt(data[1]) == 0)
                    this.setIdSelectedGame(-1);
                this.view.notifyGameListChanged();
                break;

            case "LobbyStatusChanged":
                this.model.updateGameStatus(Integer.parseInt(data[0]), data[1].equals("Playing") ? "En jeu" : "En attente");
                if (Integer.parseInt(data[0]) == this.idSelectedGame)
                    this.setIdSelectedGame(-1);
                this.view.notifyGameListChanged();
                break;

            case "EnterWaitingLobby":

                ArrayList<String> players = new ArrayList<>(Arrays.asList(data).subList(0, data.length - 1));
                String lobbyHost = data[data.length - 1];
                String gameName = lobbyHost.equals(LauncherClient.getUser()) ? this.nameCreatedGame : this.model.getGameNameFromID(this.idSelectedGame);
                LobbyHandler lobbyController = new LobbyHandler(players, lobbyHost, gameName);

                LauncherClient.getMainFrame().setContentPane(lobbyController.getView());
                LauncherClient.getMainFrame().pack();
                LauncherClient.getMainFrame().setVisible(true);
                break;

            default:
                throw new Error("Unhandled msg recieve in " + this.getClass() + " : " + message);
        }
    }

    /**
     * Returns the view managed by the class
     *
     * @return The view
     */
    public GameListPage getView() {
        return this.view;
    }

    @Override
    public void processError(Exception e) {
        // TODO Auto-generated method stub

    }

    /**
     * Executed when you create a game, after entering a name in the popup.
     * Send a message to the server.
     *
     * @param name Name of the game.
     */
    public void createGame(String name) {
        this.nameCreatedGame = name;
        NetworkClient.getInstance().write("CreateGame#" + name);
    }

    /**
     * Called when you click on a game in the game list interface.
     * Set the new ID.
     * If id is -1, disable button to join a game.
     *
     * @param idSelectedGame Id to set.
     */
    public void setIdSelectedGame(int idSelectedGame) {
        this.idSelectedGame = idSelectedGame;
        this.view.enableJoinButton(this.isGameJoinable());
    }

    public boolean isGameJoinable() {
        return this.idSelectedGame != -1 && this.model.getGameNumberOfPlayerFromID(idSelectedGame) < 6;
    }
}
