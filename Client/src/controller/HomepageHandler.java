package controller;

import network.NetworkClient;
import view.Homepage;

import javax.swing.*;
import java.util.Arrays;
/**
 * This class is the handler of the home page view, manage user interaction and send the corresponding message to the server.
 *
 */
public class HomepageHandler implements ServerEventHandler {

    private Homepage ihm;

    public HomepageHandler() {
        this.ihm = new Homepage(this);
        // Setting main panel layout
        JFrame mainFrame = LauncherClient.getMainFrame();
        mainFrame.setContentPane(ihm);
        ihm.setLayout(new BoxLayout(ihm, BoxLayout.PAGE_AXIS));

        // Print the frame
        mainFrame.pack();
        mainFrame.setVisible(true);


        NetworkClient.getInstance().setCurrentHandler(this);
    }


    @Override
    public void processMessage(String message) {
        // Process message from server
        String[] splitedHashtag = message.split("#");
        String[] data = Arrays.copyOfRange(splitedHashtag, 1, splitedHashtag.length);
        String instruction = splitedHashtag[0];
        switch (instruction) {
            case "LobbyJoined":
                GameListHandler.createGameListHandler(data);
                break;
            default:
                throw new Error("Unhandled msg recieve in " + this.getClass() + " : " + message);
        }
    }

    @Override
    public void processError(Exception e) {
        this.ihm.alerteMessage(e.getMessage());
        this.ihm.enablePlayedButton(true);
    }

    /**
     * Action called when pressing on "Jouer" button. Attempt to connect to the server with the chosen username
     *
     * @param pfUsername Username to set globally.
     */
    public void playAction(String pfUsername) {
        LauncherClient.setUser(pfUsername);
        this.ihm.enablePlayedButton(false);
        NetworkClient.getInstance().connect(pfUsername);
    }
}
