package controller;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import network.NetworkClient;
/**
 * This class is the launcher of the client.
 *
 */
public class LauncherClient {

	private static JFrame mainFrame;
	private static String user;
	
	public static void main(String[] args) {
		
		mainFrame = new JFrame("Hoax");
		mainFrame.setResizable(false);
		mainFrame.setLocation(300, 200);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				NetworkClient.getInstance().disconnect();
			}
		});
		
		new HomepageHandler();
	}
	
	public static JFrame getMainFrame() {
		return mainFrame;
	}
	
	public static String getUser() {
		return user;
	}
	
	public static void setUser(String newUser) {
		user = newUser;
	}
}
