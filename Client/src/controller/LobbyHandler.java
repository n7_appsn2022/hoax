package controller;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;

import model.Character;
import network.NetworkClient;
import view.Lobby;
/**
 * This class is the handler of the lobby view, manage user interaction and send the corresponding message to the server.
 *
 */
public class LobbyHandler implements ServerEventHandler {

    private String host;
    private ArrayList<String> player_list;
    private Lobby view;
    private String gameName;

    public LobbyHandler(ArrayList<String> players, String host, String gameName) {
        this.host = host;
        this.gameName = gameName;
        this.setPlayers(players);
        this.view = new Lobby(this);
        NetworkClient.getInstance().setCurrentHandler(this);
    }

    /**
     * Allow the user to quit the lobby, alerting the server that he leaves
     */
    public void quitter() {
        System.out.println("Action quitter");
        NetworkClient.getInstance().write("WaitingLobbyLeave");
        // NetworkClient.getInstance().disconnect();
        // SwingUtilities.getWindowAncestor(this.view).dispatchEvent(new
        // WindowEvent(SwingUtilities.getWindowAncestor(this.view),
        // WindowEvent.WINDOW_CLOSING));
    }

    /**
     * If the user if the host, alert the server that the game has to start
     */
    public void launch() {
        NetworkClient.getInstance().write("WaitingLobbyStart");
    }

    @Override
    public void processMessage(String message) {
        String[] splitedHashtag = message.split("#");
        String[] data = Arrays.copyOfRange(splitedHashtag, 1, splitedHashtag.length);
        String instruction = splitedHashtag[0];
        JFrame mainFrame = LauncherClient.getMainFrame();

        switch (instruction) {
            case "LobbyJoined":
                GameListHandler.createGameListHandler(data);
                break;

            case "Connect":
                // Update Player List
                // TODO A revoir ?
                this.player_list.add(data[0]);
                this.view.notifyPlayerChanged(host);
                break;

            case "Disconnect":
                // Update Player List
                // TODO A revoir ?
                this.player_list.remove(data[0]);
                if (data.length == 2)
                    this.host = data[1];
                this.view.notifyPlayerChanged(host);
                break;

            case "CreateGame":
                ArrayList<String> pseudos = new ArrayList<>(Arrays.asList(data[1].split(";")));
                Character role = Character.valueOf(data[0]);
                PlayerBoardHandler handlerPlayerBoard = new PlayerBoardHandler(pseudos, role);
                mainFrame.setContentPane(handlerPlayerBoard.getView());
                mainFrame.pack();
                mainFrame.setVisible(true);
                break;

            default:
                throw new Error("Unhandled msg recieve in " + this.getClass() + " : " + message);
        }
    }

    /**
     * Get the list of players who are waiting for a game to launch
     *
     * @return The list of players
     */
    public ArrayList<String> getPlayers() {
        return player_list;
    }

    /**
     * Set the player list to a new list
     *
     * @param players Player to set
     */
    public void setPlayers(ArrayList<String> players) {
        this.player_list = players;
    }

    /**
     * Gives the view manages by this class
     *
     * @return The View
     */
    public Lobby getView() {
        return this.view;
    }

    /**
     * Gives the player who is the host of the game
     *
     * @return The host
     */
    public String getHost() {
        return host;
    }

    /**
     * Retrieve the name of the game
     * @return game's name
     */
    public String getGameName() {
        return this.gameName;
    }

    /**
     * Check if you(current user) are the lobby host
     *
     * @return True is user is host, else otherwise
     */
    public boolean isLobbyHost() {
        return LauncherClient.getUser().equals(this.host);
    }

    @Override
    public void processError(Exception e) {
        // Nothing to do
    }
}
