package controller;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import model.Character;
import model.Game;
import model.Player;
import model.ResourcesType;
import network.NetworkClient;
import view.PlayerBoard;
/**
 * This class is the handler of the player board view.
 *
 */
public class PlayerBoardHandler implements ServerEventHandler {
    public static final Color AnnounceColor = new Color(57, 117, 127);
    public static final Color VoteColor = new Color(130, 60, 35);
    public static final Color InvestigateColor = new Color(144, 115, 49);
    public static final Color EliminateColor = new Color(95, 96, 91);
    public static final Color NewRoundColor = new Color(30, 34, 37);
    public static final Color ImmunityColor = new Color(228, 91, 39);
    // The game model
    private Game modelGame;
    private Character announcedCharacter;
    // The player view
    private PlayerBoard view;
    private boolean gameFinished;

    // Make queries to model and server

    /**
     * Constructor Initialize a PlayerGUI controller
     *
     * @param pseudos Players and their characters
     */
    public PlayerBoardHandler(List<String> pseudos, Character role) {
        NetworkClient.getInstance().setCurrentHandler(this);
        gameFinished = false;
        modelGame = new Game(role);
        Player currentUser = null;
        for (String pseudo : pseudos) {
            Player newPlayer = new Player(pseudo);
            modelGame.addNewRemainingPlayer(newPlayer);
            if (LauncherClient.getUser().equals(pseudo))
                currentUser = newPlayer;
        }
        modelGame.startGame(currentUser);
        view = new PlayerBoard(modelGame);
    }

    // =====PROCESS message from server=====

    @Override
    public void processMessage(String message) {
        String[] splitedHashtag = message.split("#");
        String[] data = Arrays.copyOfRange(splitedHashtag, 1, splitedHashtag.length);
        String instruction = splitedHashtag[0];
        switch (instruction) {
            case "NextRound":
                view.printLog("C'est au tour de " + data[0], NewRoundColor);
                this.modelGame.setCurrentlyPlaying(this.modelGame.getRemainingPlayerByName(data[0]));
                this.view.notifyNextRound();
                break;
            case "Eliminated":
                this.modelGame.eliminatePlayer(this.modelGame.getRemainingPlayerByName(data[0]));
                if (modelGame.getNbRemainingPlayer() != 1) {
                    this.view.notifyPlayerEliminated(data[0]);
                }
                break;
            case "Win":
                if (!gameFinished) {
                    this.view.notifyWin(data[0]);
                    gameFinished = true;
                }
                break;
            case "Investigate":

                this.view.notifyInvestigation(data);
                break;
            case "Announce":
                // Un autre joueur annonce
                String nomJoueurAnnonce = modelGame.getCurrentlyPlaying().getName();
                Character roleAnnonce = Character.getFromName(data[0]);
                announcedCharacter = roleAnnonce;
                if (!nomJoueurAnnonce.equals(LauncherClient.getUser())) {
                    view.notifyAnnounce(nomJoueurAnnonce, roleAnnonce, Arrays.copyOfRange(data, 1, data.length));
                }
                break;
            case "Vote":
                // Actualiser affichage des joueurs votant
                String resultat = data[1];
                if (resultat.equals("Agree")) {
                    // Afficher le vote du joueur dans la vue
                    view.printLog(data[0] + " a voté pour l'exécution du privilège", VoteColor);
                } else {
                    // Afficher le vote du joueur dans la vue
                    view.printLog(data[0] + " a voté contre l'exécution du privilège", VoteColor);
                }
                break;
            case "VoteResult":
                this.getView().closeCurrentJDialog();
                if (data[0].equals("Revoke")) {
                    if (data[1].equals("True")) {

                        // Bloquer le personnage
                        modelGame.getCurrentlyPlaying().blockCharacter(announcedCharacter);
                        this.view.notifyBlockedCharacter();

                        view.printLog("Le vote a conclu au blocage du rôle " + announcedCharacter.getFrenchName() + " \npour le joueur "
                                + modelGame.getCurrentlyPlaying().getName(), VoteColor);
                        view.notifyCanPassTurn();

                    } else {
                        if (!gameFinished) {
                            this.view.notifyWin(modelGame.getCurrentlyPlaying().getName());
                            gameFinished = true;
                        }

                    }

                } else if (data[0].equals("Agree")) {
                    // Executer le privilège
                    view.printLog("La majorité ne s'est pas opposé à l'annonce du rôle.", VoteColor);
                }
                break;
            case "Stealing":
                for (String players : data) {
                    if (LauncherClient.getUser().equals(players)) {
                        view.notifyStealing();
                    }
                }
                break;
            case "Immunity":
                message = data[0] + " indique être immunisé contre le privilège dont il est cible";
                view.printLog(message, ImmunityColor);
                break;
            case "AcceptationImmunity":
                if (modelGame.isMyTurn()) {
                    view.notifyAcceptationImmunity(data);
                }
                break;
            case "UpdateResources":
                if (data.length == 2) {
                    modelGame.setResource(ResourcesType.valueOf(data[0].toUpperCase()), Integer.parseInt(data[1]));
                    view.updateBank();
                } else {
                    Player updatePlayer = modelGame.getPlayerByName(data[2]);
                    int new_amount = Integer.parseInt(data[1]);
                    ResourcesType rType = ResourcesType.valueOf(data[0].toUpperCase());
                    updatePlayer.setResource(rType, new_amount);

                    view.updatePlayerResources();
                }
                break;
            case "DontBelieve":
                message = "L'immunité de " + data[0] + " n'a pas été crue. \n";
                if (data[1].contentEquals("Rightly")) {
                    message += "Son rôle est effectivement un autre : il ne pourra plus jouer le rôle clâmé";
                    if (data[0].contentEquals(LauncherClient.getUser())) {
                        Character role = announcedCharacter.getImmunity();
                        if (role != null) {
                            modelGame.getCurrentUser().blockCharacter(role);
                            this.view.notifyBlockedCharacter();
                        }
                    }
                } else {
                    message += "Son rôle est pourtant celui clâmé";
                    if (!gameFinished) {
                        this.view.notifyWin(data[0]);
                        gameFinished = true;
                    }
                }
                view.printLog(message, ImmunityColor);
                break;
            case "Execute":
                view.notifyCanPassTurn();
                view.printLog(modelGame.getCurrentlyPlaying().getName() + " execute son privilège", AnnounceColor);
                break;
            case "LobbyJoined":
                GameListHandler.createGameListHandler(data);
                break;
            case "Disconnect":
                // Do Nothing here, as intended
                break;
            default:
                throw new Error("Unhandled msg recieve in " + this.getClass() + " : " + message);
        }

    }


    // ===== View ACESS =====

    /**
     * This method retrieve the view
     *
     * @return view
     */
    public PlayerBoard getView() {
        return this.view;
    }

    /**
     * This method retrieve the model
     * @return model
     */
    public Game getModel() {
        return this.modelGame;
    }

    @Override
    public void processError(Exception e) {
        // Nothing to do
    }

}
