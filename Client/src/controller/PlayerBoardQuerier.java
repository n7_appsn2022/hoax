package controller;

import java.util.List;

import model.Game;
import network.NetworkClient;
/**
 * This class handle the user interaction and send the corresponding message to the server
 *
 */
public class PlayerBoardQuerier {
  private Game modelGame;

  public PlayerBoardQuerier(Game modelGame) {
    this.modelGame = modelGame;
  }

  // =====WRITE TO SERVER=====
  /**
   * This method will notify the server/client of the denounced player and the
   * character given The server will give a response and indicate that the player
   * was indeed eliminated
   * 
   * @param playerGiven    name of the player given by the user that he/she wants
   *                       to denounce
   * @param characterGiven character that the user think suit the targeted player
   */
  public void denounce(String playerGiven, String characterGiven) {
    NetworkClient.getInstance().write("Denounce#" + playerGiven + "#" + characterGiven);
  }

  /**
   * This method informed the server that the current player has pass his turn by
   * giving the message : PassRound In exchange the server will update the game
   */
  public void passTurn() {
    NetworkClient.getInstance().write("PassRound");
  }

  /**
   * 
   * This method pass to the server the targeted player the current one wants to
   * investigate on
   * 
   * @param targetedPlayer the player the current player wants to investigate
   */
  public void investigate(String targetedPlayer) {
    ServerEventHandler.networkClient.write("Investigate#" + targetedPlayer);

  }

  /**
   * Allow the current player to announce a character
   * 
   * @param characterGiven Giver character
   */
  public void announce(String characterGiven, String[] Args) {

    int i = 0;
    String message = "Announce#" + characterGiven;
    while (Args[i] != null) {
      message = message + "#" + Args[i];
      i++;
    }

    ServerEventHandler.networkClient.write(message);
  }

  /**
   * Return if the player is one of the remainings.
   * 
   * @param playerGiven Player to check
   * @return Boolean : true if Player is a remaining Player
   */
  public boolean isValid(String playerGiven) {
    return modelGame.getRemainingPlayerByName(playerGiven) != null;
  }

  /**
   * Send a vote message to the server with the value in parameter
   * 
   * @param vote Agree to agree the vote and revoke to revoke
   */
  public void vote(String vote) {

    ServerEventHandler.networkClient.write("Vote#" + vote);
  }

  /**
   * Send a immunity message to the server with the value in parameter
   * 
   * @param vote
   */
  public void immunity(String vote) {

    ServerEventHandler.networkClient.write("Immunity#" + vote);
  }

  /**
   * Send the list of players that are lying on their immunity for the current
   * player
   * 
   * @param result
   */
  public void acceptationImmunity(List<String> result) {
    String message = "AcceptationImmunity#";
    for (String player : result) {
      message += player + "#";
    }
    ServerEventHandler.networkClient.write(message);

  }

  /**
   * Sends a message to the server which mention that the player will leave the
   * game after clicking on ok
   */
  public void win() {
    ServerEventHandler.networkClient.write("Win");
  }

}
