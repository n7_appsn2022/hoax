package controller;

import network.NetworkClient;

public interface ServerEventHandler {

    NetworkClient networkClient = NetworkClient.getInstance();

    /**
     * Process the message received from the server.
     * Message has form "Instruction#Value1#SubValue21;SubValue22#...#ValueN"
     * @param message   Message to process
     */
    void processMessage(String message);

    /**
     * Process error from messagereceiving process.
     * @param e     Error to handle
     */
    void processError(Exception e);
}
