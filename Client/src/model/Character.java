package model;

import java.util.Arrays;

/**
 * A enumeration character that represent a role in the game
 * 
 * @author Awa Tidjani
 *
 */

public enum Character {
	
	theEx("The Ex",new PrivilegeEx(), "theEx", "L'ex"),
	theSonInLaw("The Son In Law",new PrivilegeSonInLaw(), "theSonInLaw", "Le beau fils"),
	theLover("The Lover",new PrivilegeLover(), "theLover", "L'amant"),
	theDistantCousin("The Distant Cousin",new PrivilegeDistantCousin(), "theDistantCousin", "Le cousin éloigné"),
	theChief("The Chief",new PrivilegeChief(), "theChief", "Le chef"),
	theGardener("The Gardener",new PrivilegeGardener(), "theGardener", "Le jardinier"),
	theButler("The Butler",new PrivilegeButler(), "theButler", "Le majordome");
	
	
	private String name;
	private String serverName;
	private Privilege privilege;
	private String frenchName;
	
	Character(String name, Privilege privilege, String serverName, String frenchName) {
		this.name=name;
		this.privilege = privilege;
		this.serverName = serverName;
		this.frenchName = frenchName;
	}
	
	/**
	 * Get the name of the character
	 * @return The name of the character
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Get the french name of the character
	 * @return	French name for this Character
	 */
	public String getFrenchName() {
	  return this.frenchName;
	}
	
	/**
   * Return an object privilege. This object contains the list of the differents actions that the character associated can do.
   * @return The privilege object
   */
  public Privilege getPrivilege() {
    return privilege;
  }
  /**
   * Gives the server name of a character
   * @return	Name of Character for the Server
   */
  public String getServerName() {
    return serverName;
  }
  
  /**
   * Gives the Character which is immune to the privilege of this object
   * @return	Character immune to this Character
   */
  public Character getImmunity() {
    return privilege.getImmunity();
  }
  
  /**
   * Gives all the french names of the enum
   * @return	French names for all Characters
   */
	public static String[] getFrenchNames() {
	  return Arrays.stream(Character.values()).map(Character::getFrenchName).toArray(String[]::new);
	}
	
	
	
	/**
	 * Get all names of the enum as a String table.
	 * @return The table of every name of the enum.
	 */
	public static String[] getNames() {
		return Arrays.stream(Character.values()).map(Character::getName).toArray(String[]::new);
	}

	/**
	 * Get a character from a String. If name correspond to one of the name in the Enum, it returns the element.
	 * @param name The string to get the element from
	 * @return An element of the Enum if the string is good, null otherwise
	 */
	public static Character getFromName(String name) {
		Character result = null;
		Character[] characters = Character.values();
		int length = characters.length;
		int i = 0;

		while(i < length && result == null) {
			if(characters[i].getName().equals(name)) {
				result = characters[i];
			// }else if(characters[i].getServerName().equals(name)){
			}else if(characters[i].toString().equals(name)){
			  result = characters[i];
			}else if(characters[i].getFrenchName().equals(name)) {
			  result = characters[i];
			}
			i++;
		}
		return result;
	}
	
	/**
	 * Translate the serverName given into a frenchname if exists
	 * @param servername	Server name of this Character
	 * @return	French name of this Character
	 */
	public static String getFrenchNameFromServerName(String servername) {
	  return getFromName(servername).getFrenchName();
	}
	
	
	/**
	 * This method allows to get the list of privileges for a given character
	 * @param character The character which we want to know different privileges
	 * @return A String table which contains each privileges
	 */
	public static String[] getPrivilege(Character character) {
    
    return character.getPrivilege().getPrivileges();
  }
}
