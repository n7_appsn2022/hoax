package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * This class represent the game itself.
 *
 */
public class Game implements ResourceOwner {
	private List<Player> remainingPlayers;
	private List<Player> eliminatedPlayers;
	private boolean gameStarted;
	private Player currentUser;
	private Player currentlyPlaying;
	private Map<ResourcesType, Integer> bank;
	int NB_INITIAL_RESOURCE = 13;
	private Character role;

	/**
	 * A game has 2 lists of players : remaining players, and eliminated players. It
	 * also has a state : started or not.
	 */
	public Game(Character pfRole) {
		this.remainingPlayers = new ArrayList<>();
		this.eliminatedPlayers = new ArrayList<>();
		this.gameStarted = false;
		this.role = pfRole;
		this.bank = new HashMap<>();
		initialiseResource(bank, NB_INITIAL_RESOURCE);
	}

	/**
	 * Start the game. Won't start if the number of remaining players is less than
	 * 3. Set the game to started.
	 * 
	 * @throws IllegalArgumentException Can't start game with less than 3.
	 */
	public void startGame(Player player) throws IllegalArgumentException {
		if (this.remainingPlayers.size() < 3)
			throw new IllegalArgumentException("No enough player, can't start the game.");
		this.currentUser = player;
		this.gameStarted = true;
	}

	/**
	 * Add a player the remaining player list.
	 * 
	 * @param playerToAdd Player to add.
	 * @throws IllegalArgumentException If the player is already in the list, if
	 *                                  there is already 6 players, or if the game
	 *                                  is already started.
	 */
	public void addNewRemainingPlayer(Player playerToAdd) throws IllegalArgumentException {
		if (this.remainingPlayers.contains(playerToAdd))
			throw new IllegalArgumentException("Player already in remaining list");
		if (this.remainingPlayers.size() == 6)
			throw new IllegalArgumentException("Already 6 players, you can't add more");
		if (this.isGameStarted())
			throw new IllegalArgumentException("Can't add a player after game started");
		this.remainingPlayers.add(playerToAdd);
	}

	/**
	 * Remove a player from remaining players list, and add it toeliminated players
	 * list.
	 * 
	 * @param playerToEliminate Player to eliminate
	 * // @return A boolean to check if the player is correctly moved.
	 * @throws IllegalArgumentException If the player isn't in the remaining player
	 *                                  list.
	 */
	public void eliminatePlayer(Player playerToEliminate) throws IllegalArgumentException {
		if (!this.remainingPlayers.contains(playerToEliminate))
			throw new IllegalArgumentException("This player isn't playing anymore. You can't eliminate him");
		playerToEliminate.setEliminated();
		this.eliminatedPlayers.add(playerToEliminate);
		this.remainingPlayers.remove(playerToEliminate);
	}

	/**
	 * Return a list of all players, with remaining firsts and eliminated after.
	 * 
	 * @return The list of all players.
	 */
	public List<Player> getPlayers() {
		List<Player> allPlayers = new ArrayList<>();
		allPlayers.addAll(this.remainingPlayers);
		allPlayers.addAll(this.eliminatedPlayers);
		return allPlayers;
	}

	/**
	 * Return the all the remaining players, as a list.
	 * 
	 * @return The list of all players.
	 */
	public List<Player> getRemainingPlayers() {
		return this.remainingPlayers;
	}

	/**
	 * Return the number of remaining players.
	 * 
	 * @return The number of remaining players.
	 */
	public int getNbRemainingPlayer() {
		return this.remainingPlayers.size();
	}

	/**
	 * Return the number of remaining and eliminated players.
	 * 
	 * @return The number of remaining and eliminated players.
	 */
	public int getNbPlayer() {
		return this.remainingPlayers.size() + this.eliminatedPlayers.size();
	}

	/**
	 * Return a player from the remaining list, searching by name.
	 * 
	 * @param name Name of the wanted player.
	 * @return The player having the specified name.
	 */
	public Player getRemainingPlayerByName(String name) {
		return this.remainingPlayers.stream().filter(e -> e.getName().equals(name)).findAny().orElse(null);
	}

	/**
	 * Return a player from the eliminated list, searching by name.
	 * 
	 * @param name Name of the wanted player.
	 * @return The player having the specified name.
	 */
	public Player getEliminatedPlayerByName(String name) {
		return this.eliminatedPlayers.stream().filter(e -> e.getName().equals(name)).findAny().orElse(null);
	}

	/**
	 * Return a player from the remaining list or the eliminated list, searching by
	 * name.
	 * 
	 * @param name Name of the wanted player.
	 * @return The player having the specified name.
	 */
	public Player getPlayerByName(String name) {
		return getEliminatedPlayerByName(name) == null ? getRemainingPlayerByName(name)
				: getEliminatedPlayerByName(name);
	}

	/**
	 * Return the current user.
	 * 
	 * @return The current user.
	 */
	public Player getCurrentUser() {
		return this.currentUser;
	}

	private Stream<Player> getOpponents() {
		return this.getRemainingPlayers().stream()
				.filter(i -> !i.equals(this.getCurrentUser()));
	}

	/**
	 * Gives a list of opponents players that are still in game
	 * @return	Opponents player list
	 */
	public List<Player> getOpponentsPlayers() {
		return this.getOpponents().collect(Collectors.toList());
	}
	/**
   * Gives a list of opponents players name that are still in game
   * @return	Opponents player name list
   */
	public List<String> getOpponentsNames() {
		return this.getOpponents().map(Player::getName).collect(Collectors.toList());
	}

	/**
	 * Show the current started state of the game.
	 * 
	 * @return True if the game already started.
	 */
	public boolean isGameStarted() {
		return this.gameStarted;
	}

	/**
	 * Return if the player is in the remaining list.
	 * 
	 * @param player Player to check if remaining.
	 * @return True if the player is in the remaining list, false otherwise.
	 */
	public boolean isRemainingPlayer(Player player) {
		return this.remainingPlayers.contains(player);
	}

	/**
	 * Return if the player is in the eliminated list.
	 * 
	 * @param player Player to check if eliminated.
	 * @return True if the player is in the eliminated list, false otherwise.
	 */
	public boolean isEliminated(Player player) {
		return this.eliminatedPlayers.contains(player);
	}

	/**
	 * Indicate if the game is over or not.
	 * 
	 * @return True if the game is over
	 * @throws IllegalArgumentException If the game has not already started.
	 */
	public boolean isGameOver() throws IllegalArgumentException {
		if (!this.isGameStarted())
			throw new IllegalArgumentException("Game isn't started, it can't be over");
		return this.remainingPlayers.size() == 1;
	}

	@Override
	public Map<ResourcesType, Integer> getResources() {
		return this.bank;
	}

	/**
	 * Return the application user Character
	 * @return	The Character
	 */
	public Character getCharacter() {
		return this.role;
	}

	/**
	 * Set currently playing player
	 * @param player	Player currently playing
	 */
	public void setCurrentlyPlaying(Player player) {
		this.currentlyPlaying = player;
	}

	/**
	 * Get currently playing Player
	 * @return	Player currently Playing
	 */
	public Player getCurrentlyPlaying() { return this.currentlyPlaying; }

	/**
	 * Indicates if the current user is playing
	 * @return	True if its user turn, false otherwise
	 */
	public boolean isMyTurn() {
		return getCurrentUser().equals(this.getCurrentlyPlaying());
	}



  
}
