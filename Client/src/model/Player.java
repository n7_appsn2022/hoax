package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class Player who implements PlayerInterface
 *
 * @author Awa Tidjani, Callune Gitenet
 */

public class Player implements PlayerInterface, ResourceOwner {

	private String name;
	private Map<ResourcesType, Integer> resources;
	private boolean eliminated;
	private Set<Character> blockedCharacter;

	public Player(String name) {
		this.name = name;
		this.eliminated = false;
		resources = new HashMap<>();
		initialiseResource(resources, NB_INITIAL_RESOURCE);
		this.blockedCharacter=new HashSet<>();
	}

	/**
	 * Says if the player is eliminated
	 */
	public boolean isEliminated() {
		return eliminated;
	}

	/**
	 * Set the status of the player to eliminated
	 */
	public void setEliminated() {
		this.eliminated = true;
	}

	/**
	 * Returns the name of the player
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Return a table of the blocked characters for the player
	 * @return	Blocked Characters for this Player
	 */
	public Character[] getBlockedCharacters() {
	  Character[] array = new Character[blockedCharacter.size()];
	  array =blockedCharacter.toArray(array);
	  return array;
	}

	@Override
	public Map<ResourcesType, Integer> getResources() {
		return this.resources;
	}

    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Player)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Player c = (Player) o;

        // Compare the data members and return accordingly
        return this.getName().equals(c.getName());
    }

    /**
     * Add a character to the list of blocked characters
     * @param role	Character to block for a player
     */
    public void blockCharacter(Character role) {
    	this.blockedCharacter.add(role);
    }

}
