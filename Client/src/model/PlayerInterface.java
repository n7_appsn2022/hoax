package model;

/**
 * Player's interface 
 * @author Awa Tidjani, Callune Gitenet
 *
 */
public interface PlayerInterface {
	int NB_INITIAL_RESOURCE=0;
	
	/**
	 * 
	 * Test if a player is eliminated
	 * @return return true if the player has been eliminated
	 */
	boolean isEliminated();

	/**
	 * 
	 * Eliminate a player
	 */
	void setEliminated();

	/**
	 * Get the player's name
	 * @return the name of a player
	 */
	String getName();
}
