package model;
/**
 * Represent the privilege that the character's user can use. 
 *
 */
public abstract class Privilege {
	/**
	 * Array of different privilege (for a Character)
	 */
	private String[] privileges;
	public Privilege(String[] privileges) {
		this.privileges = privileges;
	}

	/**
	 * Return the number of Privilege available.
	 * @return	The number of Privilege
	 */
	public int getNbPrivilege() {
		return privileges.length;
	}

	/**
	 * Return all the Privileges
	 * @return	Array of Privileges
	 */
	public String[] getPrivileges() {
		return privileges;
	}
	
	/**
	 * Gives the character immunity of this privilege
	 * @return	The Character immune for this Privilege
	 */
	abstract public Character getImmunity();
}
