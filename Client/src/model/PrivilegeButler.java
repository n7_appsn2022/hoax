package model;

public class PrivilegeButler extends Privilege {

	public PrivilegeButler() {
		super(new String[]{
				"<html>Désignez un joueur. " +
						"<br>Vous et ce joueur prenez chacun 1 ressource de n'importe quel type.</html>"
				} );
		}

  @Override
  public Character getImmunity() {
    return null;
  }
}
