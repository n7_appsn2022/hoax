package model;

public class PrivilegeChief extends Privilege {
	public PrivilegeChief() {
	super(new String[]{
			"<html>Echangez n'importe quel nombre de vos ressources d'un même type " +
					"<br>avec un nombre égal de ressources d'un autre type. " +
					"<br>Vous pouvez échanger ces ressources avec la banque ou avec d'autres joueurs.</html>"
			} );
	}

  @Override
  public Character getImmunity() {
    return Character.theDistantCousin;
  }
	
}
