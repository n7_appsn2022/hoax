package model;

public class PrivilegeDistantCousin extends Privilege {
	public PrivilegeDistantCousin() {
		super(new String[]{
				"Prendre 1 Argent à la banque."
				,"Prendre 1 Renommée à la banque."
				} );
	}

  @Override
  public Character getImmunity() {
    return null;
  }
}
