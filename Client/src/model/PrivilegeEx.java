package model;

public class PrivilegeEx extends Privilege {

	public PrivilegeEx() {
		super(new String[]{
				"Prendre 1 Argent à la banque."
				,"Prendre 1 Preuve à la banque."
				,"Chaque joueur à coté de vous doit vous donner 1 ressource de son choix."
				} );
	}

  @Override
  public Character getImmunity() {
    return Character.theButler;
  }

}
