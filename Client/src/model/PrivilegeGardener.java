package model;

public class PrivilegeGardener extends Privilege {
	public PrivilegeGardener() {
		super(new String[]{
				"Prenez jusqu'a 2 ressources d'un même type à un autre joueur."
				} );
		}

  @Override
  public Character getImmunity() {
    return Character.theChief;
  }
}
