package model;

public class PrivilegeLover extends Privilege {

	public PrivilegeLover() {
		super(new String[]{
				"Prendre 1 Preuve à la banque."
				,"Prendre 1 Renommée à la banque."
				} );
	}

  @Override
  public Character getImmunity() {
    return null;
  }
}
