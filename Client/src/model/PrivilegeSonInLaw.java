package model;

public class PrivilegeSonInLaw extends Privilege {

	public PrivilegeSonInLaw() {
		super(new String[]{
				"<html>Désigner un type de ressource." +
						"<br> Chaque joueur doit vous donner 1 ressource de ce type.</html>"
				} );
	}

  @Override
  public Character getImmunity() {
    return Character.theLover;
  }
}
