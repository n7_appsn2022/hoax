package model;

import java.util.Map;
/**
 * This class represent a resource owner. 
 * A resource owner can be the bank or a player. 
 *
 */
public interface ResourceOwner {
	int MIN_RESOURCE = 0;
	int MAX_RESOURCE = 13;

	/**
	 * Get the resources of a player. Resources are transmitted in a map. Key are
	 * "cash", "prestige", "evidence". Use CASH_KEY,EVIDENCE_KEY and PRESTIGE_KEY
	 * constant. Values are an integer between 0 and 13. Use MIN_RESOURCE and
	 * MAX_RESOURCE constant.
	 * 
	 * @return a map that contains the resources
	 */
	Map<ResourcesType, Integer> getResources();

	/**
	 * Take the quantity of the resourceKey in the target resources. If quantity >
	 * taget.resources you will take what you can.
	 * 
	 * @param type	Type of the ressource to take
	 * @param quantity	Quantity to take
	 * @param target	Target to get the resource from
	 */
	default void take(ResourcesType type, int quantity, ResourceOwner target) {
		int targetQuantity = target.getResource(type);
		// you can only take what target have
		int effectiveQuantity = Math.min(quantity, targetQuantity);
		target.setResource(type, targetQuantity - effectiveQuantity);
		setResource(type, getResource(type) + effectiveQuantity);
	}

	/**
	 * Initialise the map of resourceType, seting all resourceType to initialValue.
	 * 
	 * @param resources    the map of resourceType
	 * @param initialValue quantity to set
	 */
	default void initialiseResource(Map<ResourcesType, Integer> resources, int initialValue) {
		for (ResourcesType type : ResourcesType.values()) {
			resources.put(type, initialValue);
		}
	}

	/**
	 * Set the entity resourceType to nbset. Perform a check.
	 * 
	 * @param nbset cash to set.
	 */
	default void setResource(ResourcesType type, int nbset) {
		checkOnResourceSet(nbset);
		getResources().replace(type, nbset);
	}

	/**
	 * Return entity given resource quantity.
	 * 
	 * @param type type of resource.
	 * @return amount of the resource type the entity have.
	 */
	default int getResource(ResourcesType type) {
		return getResources().get(type);
	}

	/**
	 * This check should be performed before each set of resource. It maintains the
	 * resource between min and max range.
	 * 
	 * @param nbset amount to set on a resource.
	 */
	default void checkOnResourceSet(int nbset) {
		if (nbset < MIN_RESOURCE || nbset > MAX_RESOURCE) {
			throw new RuntimeException("Error: Failed to set resource. " + nbset + "is not in [" + MIN_RESOURCE + ";"
					+ MAX_RESOURCE + "].");
		}
	}
}
