package model;

import java.util.Arrays;
/**
 * This enum class represent the different kind of resources.
 *
 */
public enum ResourcesType {
	PRESTIGE("Renommée", "Prestige"),
	CASH("Argent", "Cash"),
	EVIDENCE("Preuve", "Evidence");
	
	private String frenchName;
	private String serverName;
  ResourcesType(String name, String serverName) {
    frenchName = name;
    this.serverName = serverName;
  }
  
  /**
   * Gives the french name of the ResourceType
   * @return  RessourceType name
   */
  public String getName() {
    return frenchName;
  }
  
  /**
   * Gives the name that will be used to communicate with the servers
   * @return  Server Name for this RessourceType
   */
  public String getServerName() {
    return serverName;
  }
  
  /**
   * Get a character from a String. If name correspond to one of the name in the Enum, it returns the element.
   * @param name The string to get the element from
   * @return An element of the Enum if the string is good, null otherwise
   */
  public static ResourcesType getFromName(String name) {
    ResourcesType result = null;
    ResourcesType[] resources = ResourcesType.values();
    int length = resources.length;
    int i = 0;

    while(i < length && result == null) {
      if(resources[i].getName().equals(name)) {
        result = resources[i];
      }else if(resources[i].getServerName().equals(name)){
        result = resources[i];
      }
      i++;
    }
    return result;
  }
  /**
   * Translate the serverName given into the name if exists
   * @param servername  ServerName to translate
   * @return  FrenchName for this RessourceType
   */
  public static String getFrenchNameFromServerName(String servername) {
    return getFromName(servername).getName();
  }
  
  /**
   * Gives a table with all names
   * @return  All RessourceType names, as array
   */
  public static String[] getNames() {
    return Arrays.stream(ResourcesType.values()).map(ResourcesType::getName).toArray(String[]::new);
  }
  
}
