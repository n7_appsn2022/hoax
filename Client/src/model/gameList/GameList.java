package model.gameList;

import java.util.*;

/**
 * This class represent the list of the "game".
 * In this case, the list contain some GameListInfo and not an object game itself.
 * An object GameListInfo contains the game's id, the game's title, the number of player and the game's status.
 *
 */
public class GameList {

    private Map<Integer, GameListInfo> listOfGames;

    public GameList() {
        listOfGames = new HashMap<>();
    }

    /**
     * Add new GameListInfo to the GameList.
     * @param gameID
     * @param title of the game
     * @param nbPlayers the number of players
     * @param status the game status
     */
    public void addGame(int gameID, String title, int nbPlayers, String status) {
        this.listOfGames.put(gameID, new GameListInfo(gameID, title, nbPlayers, status));
    }

    /**
     * Remove the "game" in the list using the game's ID.
     * @param gameID
     */
    public void removeGamebyID(int gameID) {
        this.listOfGames.remove(gameID);
    }

    /**
     * Retrieve the game's list.
     * @return an unmodifiable game list.
     */
    public List<GameListInfo> getGameList() {
        return Collections.unmodifiableList(new ArrayList<>(this.listOfGames.values()));
    }

    /**
     * Update the "game" in the game's list with the new number of players.
     * @param gameID
     * @param nbOfPlayer
     */
    public void updateGame(int gameID, int nbOfPlayer) {
        if (nbOfPlayer == 0)
			this.removeGamebyID(gameID);
        else
        	this.listOfGames.get(gameID).setNbPlayers(nbOfPlayer);
    }

    /**
     * Update the "game" in the game's list with the new status.
     * @param gameID
     * @param status the game status.
     */
    public void updateGameStatus(int gameID, String status) {
        this.listOfGames.get(gameID).setStatus(status);
    }

    /**
     * Retrieve the game's title based on the gameID.
     * @param gameID
     * @return the game's title.
     */
    public String getGameNameFromID(int gameID) {
        return listOfGames.get(gameID).getTitle();
    }

    /**
     * Retrieve the game's number of player based on the gameID.
     * @param gameID
     * @return the number of player for the given gameID. 
     */
    public int getGameNumberOfPlayerFromID(int gameID) {
        return this.listOfGames.get(gameID).getNbPlayers();
    }
}
