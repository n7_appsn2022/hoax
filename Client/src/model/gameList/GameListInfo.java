package model.gameList;

public class GameListInfo {

    private int idgame;
    private String GameTitle;
    private int nbPlayers;
    private String status;

    public GameListInfo(int id, String title, int nbPlayers, String status) {
        this.idgame = id;
        this.GameTitle = title;
        this.nbPlayers = nbPlayers;
        this.setStatus(status);
    }

    /**
     * Get the id of the game
     *
     * @return	Game ID
     */
    public int getId() {
        return idgame;
    }

    /**
     * Set the id of the game
     *
     * @param idgame	ID to set
     */
    public void setId(int idgame) {
        this.idgame = idgame;
    }

    /**
     * Get the title of the game
     *
     * @return	Game Title
     */
    public String getTitle() {
        return GameTitle;
    }

    /**
     * Set the title of the game
     *
     * @param gameTitle	Title to set
     */
    public void setTitle(String gameTitle) {
        GameTitle = gameTitle;
    }

    /**
     * Get the number of player of the game
     *
     * @return	Number of player
     */
    public int getNbPlayers() {
        return nbPlayers;
    }

    /**
     * Set the number of player of the game
     *
     * @param nbPlayers	Number of player to set
     */
    public void setNbPlayers(int nbPlayers) {
        this.nbPlayers = nbPlayers;
    }

    /**
     * Get the status of the game
     *
     * @return	Game status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the status of the game
     *
     * @param status	Status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
}