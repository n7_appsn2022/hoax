package model.gameList;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * This class allows to modelize a table which contains a list of games. These games are simplified here.
 * We have to know the number of players, its status and its name.
 * @author Anthony LAURENT , Maeva GUERRIER
 * @javadoc Michael MORA
 */
public class GameListTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1415562606328800664L;
	private static final String[] columnNames = {"Nom", "Nombre de joueurs", "Statut"};
	
	private List<Data> gameList;

    public GameListTableModel() {
		this.gameList = new ArrayList<>();
	}

    /**
     * Insert a row in the model. This row correspond to a game
     * @param row   Place of the row
     * @param rowData   Info about the game
     */
    public void insertRow(int row, GameListInfo rowData) {
        Data data = new Data();
        data.id = rowData.getId();
        data.gameListInfo = rowData;
        
        this.gameList.add(row, data);
        fireTableDataChanged();
    }
    /**
     * Remove a game of the list from its ID
     * @param id    Game ID
     */
    public void removeById(int id) {
    	int size = this.gameList.size();
    	gameList.removeIf(e -> e.id == id);
    	if(gameList.size() < size) {
    		fireTableDataChanged();
    	}
    }
    
    /**
     * Clear data from the model
     */
    public void clear() {
    	this.gameList.clear();
    	fireTableDataChanged();
    }
    
    /**
     * Get the ID of a game from its row
     * @param row   Row ID to get the status from
     * @return  ID of the game in the specified row
     */
    public int getIdFromRow(int row) {
    	return this.gameList.get(row).id;
    }

    /**
     * Get the status of a game from its row number
     * @param row   Row ID to get the status from
     * @return  Status of the game in the specified row
     */
    public String getStatusFromRow(int row) {
        return this.gameList.get(row).gameListInfo.getStatus();
    }
    
    @Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return this.gameList.size();
    }
    
    @Override
    public String getColumnName(int col) {
    	String result = null;
        if(col >= 0 && col < columnNames.length) {
        	result = columnNames[col];
        }
        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return gameList.get(rowIndex).gameListInfo.getTitle();
            case 1: return gameList.get(rowIndex).gameListInfo.getNbPlayers();
            case 2: return gameList.get(rowIndex).gameListInfo.getStatus();
        }
        return null;
    }
    /**
     * This class represent the data for the table model.
     *
     */
    class Data {
    	public int id;
    	public GameListInfo gameListInfo;
    }
}
