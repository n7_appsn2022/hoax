package network;

import controller.ServerEventHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NetworkClient implements NetworkListener {

    private static SocketManager manager = new SocketManager();

    private ServerEventHandler currentHandler;

    private static NetworkClient INSTANCE = new NetworkClient();

    ArrayList<String> messagesReceived;
    ArrayList<String> messagesSend;

    /**
     * Private constructor
     */
    private NetworkClient() {
        manager.addNetworkListener(this);
        this.messagesReceived = new ArrayList<>();
        this.messagesSend = new ArrayList<>();
    }

    /**
     * Return the unique instance of Network Client. Avoid duplicates.
     * @return  Unique instance of NetworkClient
     */
    public static NetworkClient getInstance()
    {
        return INSTANCE;
    }

    /**
     * Set the IP to join another Server.
     * @param IP    IP to set
     */
    public void setIP(String IP) {
    	manager.setIP(IP);
    }

    /**
     * Return the current IP used
     * @return      IP currently used
     */
    public String getIP() {
    	return manager.getIP();
    }

    @Override
    public void statusChanged(boolean status) {
        System.out.println(status ? "Connecté" : "Déconnecté");
    }

    @Override
    public void messageReceived(String message) {
        // System.out.println("Message reçu : " + message + ". C'est le controller suivant qui gère : " + currentController.getClass());
        System.out.println("Message reçu : " + message);
        this.messagesReceived.add(message);
        currentHandler.processMessage(message);
    }

    @Override
    public void error(Exception e) {
        this.currentHandler.processError(e);
    }

    public void setCurrentHandler(ServerEventHandler controller) {
        this.currentHandler = controller;
    }

    /**
     * Send a connect request to the Server
     * @param pseudo    Pseudo used to authenticate the Client
     */
    public void connect(String pseudo) {
        System.out.println("Connexion : " + pseudo);
        this.messagesSend.add("Connexion#" + pseudo);
        manager.connect(pseudo);
    }

    /**
     * Write a message to the server
     * @param message   Message to write to the server
     */
    public void write(String message) {
        System.out.println("Message envoyé : " + message + ". / " + currentHandler.getClass());
        this.messagesSend.add(message);
        manager.write(message);
    }

    /**
     * Send a disconnect request to the Server
     */
    public void disconnect() {
        manager.disconnect();
    }

    /**
     * Get all message received from the server
     * @return  Message received from the server
     */
    public List<String> getMessagesReceived() { 
    	return Collections.unmodifiableList(this.messagesReceived);
    }

    /**
     * Get all message send to the server
     * @return  Messages send to the server
     */
    public List<String> getMessagesSend() { 
    	return Collections.unmodifiableList(this.messagesSend);
    }

    /**
     * Get the current Handler.
     * The main JFrame is unique, only its JPAnel change, with its associated Handler.
     * Every time a new Handler take control of the user interaction, it subscribe to the NetworkClient as currently in use.
     * @return  The current Handler.
     */
    public ServerEventHandler getCurrentHandler() {
    	return this.currentHandler;
    }
}