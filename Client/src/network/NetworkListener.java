package network;

import java.util.EventListener;

public interface NetworkListener extends EventListener {
	
	/**
	 * Allow to know the state of the connection.
	 * 
	 * @param status True if the connection is esthablished, otherwise false.
	 */
	void statusChanged(boolean status);
	
	/**
	 * Handle messages sent by the server.
	 * 
	 * @param message Message received.
	 */
	void messageReceived(String message);
	
	/**
	 * Handle errors when it occurs.
	 * 
	 * @param e Error
	 */
	void error(Exception e);
}
