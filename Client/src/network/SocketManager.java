package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.event.EventListenerList;

import network.exception.DuplicateUserNameException;
import network.exception.MessageLostException;
import network.exception.ServerUnreachableException;

public class SocketManager {
	
	private String IP = "51.210.6.223";
	private final int PORT = 2022;
	
	private EventListenerList listeners;
	private boolean status;
	private Socket socket;
	private String pseudo;
	
	/**
	 * Constructor.
	 */
	public SocketManager() {
		this.listeners = new EventListenerList();
		this.status = false;
		this.socket = null;
		this.pseudo = null;
	}
	
	public String getIP() {
		return this.IP;
	}
	
	public void setIP(String IP) {
		this.IP = IP;
	}
	
	/**
	 * Allow to connect a pseudo to the server. If the pseudo already exists, you will be rejected.
	 * 
	 * @param pseudo Pseudo that you want use.
	 */
	public void connect(String pseudo) {
		if(this.status) {
			return;
		}
		
		this.pseudo = pseudo;
		
		new Thread(() -> {
			socket = null;
			try {
				socket = new Socket(IP, PORT);
				socket.setSoTimeout(5000);

				// Envoie la donnée au serveur
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				out.writeUTF("Co;" + pseudo);
				out.flush();

				// On récupère la donnée renvoyée
				DataInputStream in = new DataInputStream(socket.getInputStream());
				String received = in.readUTF();

				if(received.equals("AckCo")) {
					socket.setSoTimeout(0);
					setStatus(true);
					listenServer();
				}
				else if(received.equals("RejCo")) {
					error(new DuplicateUserNameException());
					socket.close();
				}
				else {
					throw new RuntimeException();
				}
			}
			catch(Exception e) {
				error(new ServerUnreachableException());

				try {
					if(socket != null) {
						socket.close();
					}
				} catch (IOException ignored) {}
			}
		}).start();
	}
	
	/**
	 * Allow to disconnect you from the server.
	 */
	public void disconnect() {
		send("Disc");
		this.pseudo = null;
		setStatus(false);
	}
	
	/**
	 * Write a message to the server.
	 * 
	 * @param message Message to send.
	 */
	public void write(String message) {
		send("Msg;" + message);
	}
	
	/**
	 * Add a new NetworkListener to the listeners list.
	 * 
	 * @param listener New listener to add.
	 */
	public void addNetworkListener(NetworkListener listener) {
		this.listeners.add(NetworkListener.class, listener);
	}
	
	/**
	 * Remove a listener from the listeners list.
	 * 
	 * @param listener Listener to remove.
	 */
	public void removeNetworkListener(NetworkListener listener) {
		this.listeners.remove(NetworkListener.class, listener);
	}
	
	/**
	 * Retrieves the current pseudo used.
	 * 
	 * @return the current pseudo if you are connected to the server, otherwise return null.
	 */
	public String getPseudo() {
		return this.pseudo;
	}
	
	/**
	 * Write to the server.
	 * 
	 * @param message Message to send.
	 */
	private void send(String message) {
		if(this.status) {
			new Thread(() -> {
				try {
					DataOutputStream out = new DataOutputStream(socket.getOutputStream());
					out.writeUTF(message);
					out.flush();
				}
				catch(Exception e) {
					error(new MessageLostException());
				}
			}).start();
		}
	}
	
	/**
	 * Wait a new message from the server.
	 */
	private void listenServer() {
		new Thread(() -> {
			try {
				while(status) {
					DataInputStream in = new DataInputStream(socket.getInputStream());
					String received = in.readUTF();

					if(received.matches("^Msg;.+$")) {
						String message = received.substring(4);
						for(NetworkListener listener : getNetworkListeners()) {
							listener.messageReceived(message);
						}
					}
				}
			}
			catch(IOException e) {
				setStatus(false);
			}
			finally {
				try {
					socket.close();
				} catch (IOException ignored) {}
			}
		}).start();
	}

	/**
	 * Retrieves an array of all listeners.
	 * 
	 * @return Array of listeners.
	 */
	private NetworkListener[] getNetworkListeners() {
        return listeners.getListeners(NetworkListener.class);
    }
	
	/**
	 * Notify all listeners that connection has changed.
	 * 
	 * @param status True if connected, false if disconnected.
	 */
	private void setStatus(boolean status) {
		if(status != this.status) {
			this.status = status;
			
			for(NetworkListener listener : getNetworkListeners()) {
				listener.statusChanged(status);
			}
		}
	}
	
	/**
	 * Notify all listeners that an error occurred.
	 * 
	 * @param e Error
	 */
	private void error(Exception e) {
		for(NetworkListener listener : getNetworkListeners()) {
			listener.error(e);
		}
	}
}
