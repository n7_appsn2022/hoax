package network.exception;

public class DuplicateUserNameException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DuplicateUserNameException() {
		super("Utilisateur déjà connecté.");
	}
}
