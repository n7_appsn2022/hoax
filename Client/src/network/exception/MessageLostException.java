package network.exception;

public class MessageLostException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MessageLostException() {
		super("Le message n'a pas pu être envoyé");
	}
}
