package network.exception;

public class ServerUnreachableException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ServerUnreachableException() {
		super("Connexion au serveur impossible...");
	}
}
