package test;

import model.Character;
import model.Game;
import model.Player;
import model.ResourcesType;

import org.junit.*;

import static org.junit.Assert.*;

public class GameTest {

	private Game game;
	private Player player1;
	private Player player2;
	private Player player3;
	private Player player4;

	@Before
	public void setUp() {
		game = new Game(Character.theEx);

		player1 = new Player("The Ex");
		player2 = new Player("The Lover");
		player3 = new Player("The Son-in-law");
		player4 = new Player("The Distant cousin");
	}

	@After
	public void tearDown() {
		game = null;
		player1 = null;
		player2 = null;
		player3 = null;
		player4 = null;
	}

	@Test
	public void testAddNewRemainingPlayer() {
		game.addNewRemainingPlayer(player1);
		assertTrue(game.isRemainingPlayer(player1));
		game.addNewRemainingPlayer(player2);
		assertTrue(game.isRemainingPlayer(player2));
		game.addNewRemainingPlayer(player3);
		game.addNewRemainingPlayer(player4);
		assertTrue(game.isRemainingPlayer(player3));
		assertTrue(game.isRemainingPlayer(player4));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNewRemainingPlayerAlreadyInListException() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player1);
		fail();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNewRemainingPlayerToAStartedGameException() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player2);
		game.addNewRemainingPlayer(player3);
		game.startGame(player1);
		game.addNewRemainingPlayer(player4);
		fail();
	}

	@Test
	public void testGetRemainingPlayerByName() {
		game.addNewRemainingPlayer(player1);
		assertEquals(player1, game.getRemainingPlayerByName("The Ex"));
		game.addNewRemainingPlayer(player2);
		assertEquals(player2, game.getRemainingPlayerByName("The Lover"));
		game.addNewRemainingPlayer(player3);
		game.addNewRemainingPlayer(player4);
		assertEquals(player3, game.getRemainingPlayerByName("The Son-in-law"));
		assertEquals(player4, game.getRemainingPlayerByName("The Distant cousin"));
	}

	@Test
	public void testGetRemainingPlayerByNameWithoutValidPlayer() {
		game.addNewRemainingPlayer(player1);
		assertEquals(player1, game.getRemainingPlayerByName("The Ex"));
		game.addNewRemainingPlayer(player2);
		assertNull(game.getRemainingPlayerByName("The Laver"));
	}

	@Test
	public void testEliminatePlayer() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player2);
		game.eliminatePlayer(player1);
		assertFalse(game.isRemainingPlayer(player1));
		assertTrue(game.isEliminated(player1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminatePlayerException() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player2);
		game.eliminatePlayer(player1);
		game.eliminatePlayer(player1);
		fail();
	}

	@Test
	public void testGetPlayerByName() {
		game.addNewRemainingPlayer(player1);
		assertEquals(player1, game.getPlayerByName("The Ex"));
		game.addNewRemainingPlayer(player2);
		assertEquals(player2, game.getPlayerByName("The Lover"));
		game.addNewRemainingPlayer(player3);
		game.addNewRemainingPlayer(player4);
		assertEquals(player3, game.getPlayerByName("The Son-in-law"));
		assertEquals(player4, game.getPlayerByName("The Distant cousin"));
		game.eliminatePlayer(player1);
		assertEquals(player1, game.getPlayerByName("The Ex"));
	}

	@Test
	public void testGetNbPlayer() {
		game.addNewRemainingPlayer(player1);
		assertEquals(1, game.getNbPlayer());
		game.addNewRemainingPlayer(player2);
		assertEquals(2, game.getNbPlayer());
		game.addNewRemainingPlayer(player3);
		game.addNewRemainingPlayer(player4);
		assertEquals(4, game.getNbPlayer());
		game.eliminatePlayer(player1);
		assertEquals(4, game.getNbPlayer());
	}

	@Test
	public void testsGetNbRemainingPlayer() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player2);
		assertEquals(2, game.getNbRemainingPlayer());
		game.eliminatePlayer(player1);
		assertEquals(1, game.getNbRemainingPlayer());
	}


	@Test
	public void testGameOver() {
		game.addNewRemainingPlayer(player1);
		game.addNewRemainingPlayer(player2);
		game.addNewRemainingPlayer(player3);
		game.startGame(player1);
		game.eliminatePlayer(player1);
		game.eliminatePlayer(player2);
		assertTrue(game.isGameOver());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGameOverException() {
		game.addNewRemainingPlayer(player1);
		game.isGameOver();
	}

	// ResourceOwner
	@Test
	public void testTakeRessource() {
		int testedValue = 4;
		int bankedCash = game.getResource(ResourcesType.CASH);
		int playerCash = player1.getResource(ResourcesType.CASH);
		game.take(ResourcesType.CASH, testedValue, player1);
		assertEquals(bankedCash + Math.min(playerCash, testedValue), game.getResource(ResourcesType.CASH));
		player1.take(ResourcesType.CASH, testedValue, game);
		assertEquals(playerCash + Math.min(bankedCash, testedValue), player1.getResource(ResourcesType.CASH));
	}
}
