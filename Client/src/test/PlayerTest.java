package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import org.junit.*;

import model.Player;
import model.PlayerInterface;

import model.ResourceOwner;
import model.ResourcesType;

/**
 * Test the class Player1
 * 
 * @author Awa Tidjani
 * 
 */

public class PlayerTest {

	// precision for the comparison
	public final static double EPSILON = 0.001;

	private Player myPlayer;

	@Before
	public void setUp() {
		this.myPlayer = new Player("toto");

	}

	@Test
	public void testNamePlayer() {
		assertEquals("Error : name of the player not set", myPlayer.getName(), "toto");
	}

	@Test
	public void testEliminated() {
		myPlayer.setEliminated();
		assertTrue("Error : Player should be eliminated", myPlayer.isEliminated());
	}
/*
	@Test
	public void testDenounce() {
		boolean result;
		result = myPlayer.denounce(mySecondPlayer, theAccusation);
		assertTrue("Error : denouncing the player", result);
	}
*/

	// ResourceOwner
	@Test
	public void testInitializeResources() {
		assertEquals("Error : cash not initialised", PlayerInterface.NB_INITIAL_RESOURCE, myPlayer.getResource(ResourcesType.CASH), EPSILON);
		assertEquals("Error : evidence not initialised", PlayerInterface.NB_INITIAL_RESOURCE, myPlayer.getResource(ResourcesType.EVIDENCE),
				EPSILON);
		assertEquals("Error : prestige not initialised", PlayerInterface.NB_INITIAL_RESOURCE, myPlayer.getResource(ResourcesType.PRESTIGE),
				EPSILON);
		Map<ResourcesType, Integer> resource = myPlayer.getResources();
		for (int value : resource.values()) {
			assertEquals("Error : ressource not initialised", PlayerInterface.NB_INITIAL_RESOURCE, value, EPSILON);
		}
	}

	@Test
	public void testSetCash() {
		int testedValue = 6;
		myPlayer.setResource(ResourcesType.CASH,testedValue);
		assertEquals("Error : Cash of the player not set",myPlayer.getResource(ResourcesType.CASH), testedValue, EPSILON);

	}

	@Test
	public void testSetPrestige() {
		int testedValue = ResourceOwner.MAX_RESOURCE;
		myPlayer.setResource(ResourcesType.PRESTIGE,testedValue);
		assertEquals("Error : Cash of the player not set", myPlayer.getResource(ResourcesType.PRESTIGE), testedValue, EPSILON);
	}

	@Test
	public void testSetEvidence() {
		int testedValue = ResourceOwner.MIN_RESOURCE;
		myPlayer.setResource(ResourcesType.PRESTIGE,testedValue);
		assertEquals("Error : Cash of the player not set", myPlayer.getResource(ResourcesType.EVIDENCE), testedValue, EPSILON);
	}

	@Test(expected = RuntimeException.class)
	public void testMin() {
		myPlayer.setResource(ResourcesType.CASH,ResourceOwner.MIN_RESOURCE - 1);
	}

	@Test(expected = RuntimeException.class)
	public void testMax() {
		myPlayer.setResource(ResourcesType.PRESTIGE,ResourceOwner.MAX_RESOURCE + 1);
	}
}
