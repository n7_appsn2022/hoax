package test;

import controller.LauncherClient;
import controller.PlayerBoardHandler;
import model.Character;
import network.NetworkClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import view.PlayerBoard;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TestHandlerPlayerBoard {

    private PlayerBoardHandler handler;


    @Before
    public void setUp(){
        LauncherClient.setUser("Anna");
        this.handler = new PlayerBoardHandler(new ArrayList<>(Arrays.asList("Joe","Anna","Jack")), Character.theSonInLaw);
    }

    @After
    public void tearDown(){
        this.handler = null;
    }

    @Test
    public void testNotPlayerTurn() {
        this.handler.processMessage("NextRound#Joe");
        try {
            assertFalse(((PlayerBoard) this.handler.getView()).areButtonEnabled());
        } catch (Exception e) {
            System.out.println("Unconsistant state. Some button are enabled, and some others not. Shouldn't be happening.");
        }
    }

    @Test
    public void testPlayerTurn() throws Exception {
        this.handler.processMessage("NextRound#Anna");
        assertTrue(((PlayerBoard) this.handler.getView()).areButtonEnabled());
    }

    @Test
    public void testEliminatedPlayer() {
        assertNotNull(this.handler.getModel().getRemainingPlayerByName("Jack"));
        this.handler.processMessage("Eliminated#Jack");
        assertNull(this.handler.getModel().getRemainingPlayerByName("Jack"));
    }

    @Test
    public void testWin() {
        // TODO
    }

    @Test
    public void TestDenouncePlayer() {
        this.handler.getView().getQuierier().denounce("Jack", "The Lover");
        assertEquals("Denounce#Jack#The Lover", NetworkClient.getInstance().getMessagesSend().get(0));
    }
}
