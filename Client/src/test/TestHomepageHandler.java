package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controller.GameListHandler;
import controller.HomepageHandler;
import controller.LauncherClient;
import network.NetworkClient;

public class TestHomepageHandler {

    private HomepageHandler handler;
    /* Using those because the NetworkListener is static, they are static because don't work if they're not. */
    private static int idMessageSendToCheck = 0;


    @Before
    public void setUp(){
        LauncherClient.main(new String[]{});
        this.handler = new HomepageHandler();
    }

    @After
    public void tearDown(){
        this.handler = null;
    }

    @Test
    public void testPlayAction() {
        this.handler.playAction("Anna");
        assertEquals("Connexion#Anna", NetworkClient.getInstance().getMessagesSend().get(idMessageSendToCheck));
        idMessageSendToCheck++;
    }

    @Test
    public void testLobbyEnterWaitingLobbyMessageReceived() {
        LauncherClient.setUser("Anna");
        NetworkClient.getInstance().messageReceived("LobbyJoined#1;Hoax;1;Waiting");
        System.out.println(NetworkClient.getInstance().getCurrentHandler().getClass());
        assertTrue(NetworkClient.getInstance().getCurrentHandler() instanceof GameListHandler);
    }

}
