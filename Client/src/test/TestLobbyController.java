package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import controller.LauncherClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controller.LobbyHandler;
import controller.PlayerBoardHandler;
import model.Player;
import network.NetworkClient;

public class TestLobbyController {
    private LobbyHandler handler;
    private String host;
    /* Using those because the NetworkListener is static, they are static because don't work if they're not. */
    private static int idMessageSendToCheck = 0;


    @Before
    public void setUp() {
        LauncherClient.main(new String[]{});
        LauncherClient.setUser("Anna");
        this.host = "Joe";
        this.handler = new LobbyHandler(new ArrayList<>(Arrays.asList("Joe", "Anna")), this.host, "Game de test");
    }

    @After
    public void tearDown() {
        this.handler = null;
    }

    @Test
    public void testConnectMessageReceived() {
        this.handler.processMessage("Connect#Jack");
        assertEquals(new ArrayList<>(Arrays.asList("Joe", "Anna", "Jack")), this.handler.getPlayers());
    }

    @Test
    public void testDisconnectMessageReceived() {
        this.handler.processMessage("Connect#Jack");
        this.handler.processMessage("Disconnect#Jack");
        assertEquals(new ArrayList<>(Arrays.asList("Joe", "Anna")), this.handler.getPlayers());
    }

    @Test
    public void testCreateGameMessageReceived() {
        this.handler.processMessage("Connect#Jack");
        NetworkClient.getInstance().messageReceived("CreateGame#theEx#Joe;Anna;Jack");
        assertTrue(NetworkClient.getInstance().getCurrentHandler() instanceof PlayerBoardHandler);
        PlayerBoardHandler controller = ((PlayerBoardHandler) NetworkClient.getInstance().getCurrentHandler());
        assertArrayEquals(controller.getModel().getPlayers().toArray(),
                new Player[]{new Player("Joe"),
                        new Player("Anna"),
                        new Player("Jack")});
    }

    @Test
    public void testLaunch() {
        this.handler.launch();
        assertEquals("WaitingLobbyStart", NetworkClient.getInstance().getMessagesSend().get(idMessageSendToCheck));
        idMessageSendToCheck++;
    }

    @Test
    public void testLeave() {
        JFrame mainFrame = new JFrame();
        mainFrame.setContentPane(this.handler.getView());

        this.handler.quitter();
        assertEquals("WaitingLobbyLeave", NetworkClient.getInstance().getMessagesSend().get(idMessageSendToCheck));
        idMessageSendToCheck++;
        assertFalse(SwingUtilities.getWindowAncestor(this.handler.getView()).isActive());
    }
}
