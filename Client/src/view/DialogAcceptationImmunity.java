package view;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * This class manage the immunity dialog.
 *
 */
public class DialogAcceptationImmunity extends JDialog{
 
  private static final long serialVersionUID = -5434708987872951504L;
  private ArrayList<PanelPlayer> players;
  public DialogAcceptationImmunity(Frame frame, String[] players) {
    super(frame, true);
    this.players = new ArrayList<>();
    JPanel content = new JPanel();
    content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
    JButton accepter = new JButton("Valider");
    accepter.addActionListener(e->dispose());
    for(String p:players) {
      PanelPlayer panel = new PanelPlayer(p);
      this.players.add(panel);
      content.add(panel);
    }
    JPanel panelAccepter = new JPanel();
    panelAccepter.add(accepter);
    content.add(panelAccepter);
    setLocationRelativeTo(getParent());
    setTitle("Accepter les immunités");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setContentPane(content);
    pack();
    setVisible(true);
    
    
  }

  /**
   * Returns the choices of the current player concerning the immunities received which he must validate or not.
   * @return  Player choice
   */
  public List<String> getResult() {
    ArrayList<String> liars = new ArrayList<>();
     
    for(PanelPlayer p : players) {
      if(p.getLiarChoice()) {
        liars.add(p.getPlayer());
      }
    }
    
    return liars;
  }
  

  private class PanelPlayer extends JPanel{
 
    private static final long serialVersionUID = -1608183600037578082L;
    private JLabel player;
    private JCheckBox refuserChoix;
    
    public PanelPlayer(String player) {
      this.player = new JLabel(player);
      refuserChoix = new JCheckBox("Refuser l'immunité");
      refuserChoix.setSelected(false);
      
      add(this.player);
      add(refuserChoix);
    }

    /**
     * Choice for the player with his pseudo above checkbox
     * @return  Choice for the Player immunity
     */
    public boolean getLiarChoice() {
      return refuserChoix.isSelected();
    }
    
    public String getPlayer() {
      return player.getText();
    }
  }

}
