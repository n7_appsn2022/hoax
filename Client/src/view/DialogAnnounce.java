package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Character;
import model.Game;
import model.Player;
import model.ResourcesType;

/**
 * This class displays a dialog to announce a character. It allows to not use
 * some JOptionPane.
 * 
 * @author Michael MORA
 *
 */
public class DialogAnnounce extends JDialog {

	private boolean result = false;
	private static final long serialVersionUID = 1L;
	private JComboBox<String> choseCharacter;
	private JComboBox<String> chosePrivilege;
	private JComboBox<String> choseResourceChief;
	private JComboBox<String> chosePlayer;
	private JButton OKButton;
	private JPanel content;
	private JComboBox<String> choseResourceGardener;
	private JComboBox<String> choseResource;
	private JPanel layoutOtherPlayer;
	private JPanel layoutResources;

	private JPanel layoutChief;

	private Character characterSelected;
	private int indexChoice = 0;

	private JComboBox<Integer> resourceQuantity;
	private JComboBox<String> choseOtherResource;
	private JLabel labelEchange;

	private Game currentGame;

	public DialogAnnounce(Frame parent, Game currentGame) {
		super(parent, true);
		this.currentGame = currentGame;
		content = new JPanel(new BorderLayout());
		content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
		content.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		layoutOtherPlayer = new JPanel();
		layoutOtherPlayer.setLayout(new BoxLayout(layoutOtherPlayer, BoxLayout.PAGE_AXIS));

		chosePlayer = new JComboBox<>();
		updatePlayers();
		choseResource = new JComboBox<>(ResourcesType.getNames());
		choseResource.setVisible(false);
		choseResourceGardener = new JComboBox<>(ResourcesType.getNames());
		choseResourceGardener.setVisible(false);

		layoutResources = new JPanel();
		layoutResources.setLayout(new BoxLayout(layoutResources, BoxLayout.PAGE_AXIS));
		choseResourceChief = new JComboBox<>(this.getAvailableResources());
		choseResourceChief.addActionListener(e -> updateOtherResourcePossibility());
		chosePlayer.addActionListener(e -> updateResourceQuantity());
		choseResourceChief.addActionListener(e -> updateResourceQuantity());

		layoutChief = new JPanel();
		layoutChief.setLayout(new BoxLayout(layoutChief, BoxLayout.PAGE_AXIS));
		choseOtherResource = new JComboBox<>(this.getOtherResourcePossibility());
		choseOtherResource.addActionListener(e -> updateResourceQuantity());
		choseOtherResource.addActionListener(e -> updatePlayers());
		choseResourceGardener.addActionListener(e -> updatePlayers());
		resourceQuantity = new JComboBox<>();
		JPanel labelOtherResource = new JPanel();
		labelEchange = new JLabel();
		layoutChief.add(choseResourceChief);
		labelOtherResource.add(labelEchange);
		layoutChief.add(labelOtherResource);
		layoutChief.add(choseOtherResource);
		JPanel labelQuantity = new JPanel();
		labelQuantity.add(new JLabel("Choisissez la quantité à échanger"));
		layoutChief.add(labelQuantity);
		layoutChief.add(resourceQuantity);
		// updateResourceQuantity();

		ArrayList<String> characters = new ArrayList<>(Arrays.asList(Character.getFrenchNames()));
		Player a = currentGame.getCurrentUser();
		Character[] blockedC = a.getBlockedCharacters();
		ArrayList<String> nameBlocked = new ArrayList<>();
		for (Character character : blockedC) {
			nameBlocked.add(character.getFrenchName());
		}
		characters.removeAll(nameBlocked);
		String[] charactersAnnouncable = new String[characters.size()];
		charactersAnnouncable = characters.toArray(charactersAnnouncable);

		choseCharacter = new JComboBox<>(charactersAnnouncable);
		chosePrivilege = new JComboBox<>();
		updatePrivileges();

		OKButton = new JButton("Announce");
		JPanel panelLabelCharacter = new JPanel();
		panelLabelCharacter.add(new JLabel("Veuillez choisir le rôle à annoncer :"));
		JPanel labelPrivilege = new JPanel();
		labelPrivilege.add(new JLabel("Veuillez choisir le privilège à exécuter :"));

		content.add(panelLabelCharacter);
		content.add(choseCharacter);
		content.add(labelPrivilege);
		content.add(chosePrivilege);

		JPanel panelRessource = new JPanel();
		panelRessource.add(new JLabel("Veuillez sélectionner la ressource :"));
		layoutResources.add(panelRessource);
		layoutResources.add(choseResourceGardener);
		layoutResources.add(choseResource);
		layoutResources.add(layoutChief);
		JPanel panelJoueur = new JPanel();
		panelJoueur.add(new JLabel("Veuillez sélectionner le joueur cible :"));
		layoutOtherPlayer.add(panelJoueur);
		layoutOtherPlayer.add(chosePlayer);

		layoutResources.setVisible(false);
		layoutOtherPlayer.setVisible(false);
		content.add(layoutResources);
		content.add(layoutOtherPlayer);
		JPanel panelButton = new JPanel();

		panelButton.add(OKButton);
		content.add(panelButton);

		choseCharacter.addActionListener(e -> updatePrivileges());

		chosePrivilege.addActionListener(e -> indexChoice = chosePrivilege.getSelectedIndex());

		OKButton.addActionListener(e -> validateClose());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(getParent());
		setTitle("Faire une annonce");
		// setContentPane(content);

		JPanel dialog = new JPanel();

		dialog.add(content);
		dialog.setPreferredSize(new Dimension(500, 450));
		for (Component c : content.getComponents()) {
			c.setMaximumSize(dialog.getPreferredSize());
		}
		setContentPane(dialog);
		pack();
		setVisible(true);
	}

	private String[] getAvailableResources() {
		List<String> list = new ArrayList<>();
		for (ResourcesType resource : ResourcesType.values())
			if (currentGame.getCurrentUser().getResource(resource) > 0)
				list.add(resource.getName());
		return list.toArray(new String[0]);
	}

	/**
	 * Updates the number of resources that can be exchanged for the chief privilege
	 */
	private void updateResourceQuantity() {
		Player currentPlayer = currentGame.getCurrentUser();
		String cibledPlayerName = (String) chosePlayer.getSelectedItem();
		Player cibledPlayer = currentGame.getRemainingPlayerByName(cibledPlayerName);
		ResourcesType cibledResource = ResourcesType
				.getFromName(choseResourceChief.getSelectedItem() == null ? choseResourceChief.getItemAt(0)
						: (String) choseResourceChief.getSelectedItem());
		ResourcesType echangedResources = ResourcesType
				.getFromName(choseOtherResource.getSelectedItem() == null ? choseOtherResource.getItemAt(0)
						: (String) choseOtherResource.getSelectedItem());

		labelEchange
		.setText("Choisissez la ressource à échanger contre " + choseResourceChief.getSelectedItem() + " :");
		if (cibledResource != null && echangedResources != null) {
			int quantityCurrentPlayer = currentPlayer.getResource(cibledResource);

			int quantityCibledPlayer;
			if (cibledPlayer != null) {
				quantityCibledPlayer = cibledPlayer.getResource(echangedResources);

			} else {
				// If the bank is selected

				quantityCibledPlayer = currentGame.getResource(echangedResources);
			}
			int quantityMax = Math.min(quantityCurrentPlayer, quantityCibledPlayer);
			resourceQuantity.removeAllItems();
			for (int i = 1; i <= quantityMax; i++) {
				resourceQuantity.addItem(i);
				resourceQuantity.setSelectedItem(i);
			}
		}
	}

	/**
	 * Returns the quantity of the resource selected to be used for the privilege
	 * 
	 * @return Specific resource quantity
	 */
	public int getResourceQuantity() {
		return (int) resourceQuantity.getSelectedItem();
	}

	/**
	 * Close the JDialog and put the result to true
	 */
	private void validateClose() {
		result = true;
		setModal(false);
		this.dispose();
	}

	/**
	 * Returns the result value
	 * 
	 * @return True if the Announce button has been pressed
	 */
	public boolean getAnnounceResult() {
		return result;
	}

	/**
	 * Gives the index of the privilege selected
	 * 
	 * @return Privilege index
	 */
	public int getIndexPrivilege() {
		return indexChoice;
	}

	/**
	 * Gives the selected character
	 * 
	 * @return Selected Character
	 */
	public Character getCharacterSelected() {
		return characterSelected;
	}

	/**
	 * Get the resource wanted when there is the choice (for the Butler, the Chief
	 * and the Gardener)
	 * 
	 * @return Cibled Resource
	 */
	public String getResourceCibled() {
		String cibleName;
		if (characterSelected == Character.theChief) {
			cibleName = (String) choseResourceChief.getSelectedItem();
		} else if (characterSelected == Character.theGardener) {
			cibleName = (String) choseResourceGardener.getSelectedItem();
		} else {
			cibleName = (String) choseResource.getSelectedItem();
		}

		return getStringResource(ResourcesType.getFromName((cibleName)));

	}

	/**
	 * Returns a string for a resourcetype given
	 * 
	 * @param resource The Resource to convert
	 * @return "Prestige" for PRESTIGE, "Evidence" for EVIDENCE and "Cash" for CASH
	 */
	private String getStringResource(ResourcesType resource) {
		return resource.getServerName();
	}

	/**
	 * Returns the name of the player selected
	 * 
	 * @return Player's name
	 */
	public String getPlayerCibled() {
		String value = (String) chosePlayer.getSelectedItem();
		assert value != null;
		return (value.equals("Banque") ? "" : value);
	}

	/**
	 * Update the player combobox when each character is selected
	 */
	private void updatePlayers() {
		String[] opponents = new String[currentGame.getNbRemainingPlayer() - 1];
		opponents = currentGame.getOpponentsNames().toArray(opponents);
		chosePlayer.removeAllItems();
		if (characterSelected == Character.theChief || characterSelected == Character.theGardener) {
			String selected;
			ResourcesType resource;
			if (characterSelected == Character.theChief) {
				selected = (String) choseOtherResource.getSelectedItem();
				resource = ResourcesType.getFromName(selected);
				chosePlayer.addItem("Banque");
			}else {
				selected = (String) choseResourceGardener.getSelectedItem();
				resource = ResourcesType.getFromName(selected);
			}

			for (String opponent : opponents) {

				if (currentGame.getRemainingPlayerByName(opponent).getResource(resource) > 0)
					chosePlayer.addItem(opponent);
			}
		} else {
			for (String opponent : opponents) {
				chosePlayer.addItem(opponent);
			}
		}
	}

	/**
	 * Update the combobox privilege in function of the character selected
	 */
	private void updatePrivileges() {
		String choice = (String) choseCharacter.getSelectedItem();
		Character chosenCharacter = Character.getFromName(choice);

		if (chosenCharacter != null) {
			switch (chosenCharacter) {
			case theChief:
				layoutOtherPlayer.setVisible(true);
				layoutResources.setVisible(true);
				layoutChief.setVisible(true);
				choseResource.setVisible(false);
				choseResourceGardener.setVisible(false);
				break;
			case theButler:
				layoutOtherPlayer.setVisible(true);
				layoutResources.setVisible(true);
				layoutChief.setVisible(false);
				choseResource.setVisible(true);
				choseResourceGardener.setVisible(false);
				break;
			case theGardener:
				layoutOtherPlayer.setVisible(true);
				layoutResources.setVisible(true);
				layoutChief.setVisible(false);
				choseResource.setVisible(false);
				choseResourceGardener.setVisible(true);
				break;
			case theSonInLaw:
				layoutResources.setVisible(true);
				layoutOtherPlayer.setVisible(false);
				layoutChief.setVisible(false);
				choseResource.setVisible(true);
				choseResourceGardener.setVisible(false);
				break;
			default:
				layoutResources.setVisible(false);
				layoutOtherPlayer.setVisible(false);
				layoutChief.setVisible(false);
				choseResourceGardener.setVisible(false);

				break;
			}

			chosePrivilege.removeAllItems();
			this.characterSelected = chosenCharacter;
			String[] privilegeTable = Character.getPrivilege(chosenCharacter);
			for (String s : privilegeTable) {
				chosePrivilege.addItem(s);
			}
			updatePlayers();
		}

	}

	/**
	 * Returns the other resource type selected, to make the trade.
	 * 
	 * @return Other resource type
	 */
	public String getOtherResourceCibled() {
		String selected = (String) choseOtherResource.getSelectedItem();
		ResourcesType resource = ResourcesType.getFromName(selected);
		return getStringResource(resource);
	}

	/**
	 * When a resource is selected for a trad, yhe other resource to be trade won't
	 * be the same. So it return the 2 others.
	 * 
	 * @return The 2 resources that can be trade to the first selected.
	 */
	private String[] getOtherResourcePossibility() {
		ArrayList<String> possibilities = new ArrayList<>(Arrays.asList(ResourcesType.getNames()));
		String OtherRessource = (String) choseResourceChief.getSelectedItem();
		possibilities.removeIf(e -> e.equals(OtherRessource));

		return possibilities.toArray(new String[2]);
	}

	/**
	 * Update the choseOtherResource combobox with resources that can actually be
	 * exchanged for the chief privilege
	 */
	private void updateOtherResourcePossibility() {

		int length = choseResourceChief.getItemCount();
		boolean hasDeleted = false;
		for (int i = 0; i < length; i++) {
			if (choseOtherResource.getItemAt(i) == choseResourceChief.getSelectedItem()) {
				choseOtherResource.removeItemAt(i);
				hasDeleted = true;
			}
		}
		if (hasDeleted) {
			String[] values = ResourcesType.getNames();
			for (int i = 0; i < 3; i++) {
				if (values[i] != choseResourceChief.getSelectedItem()
						&& !values[i].equals(choseOtherResource.getItemAt(0))) {
					choseOtherResource.addItem(values[i]);
				}
			}
		}
	}

}
