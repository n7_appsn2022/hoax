package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import controller.GameListHandler;
import model.gameList.GameList;
import model.gameList.GameListInfo;
import model.gameList.GameListTableModel;
/**
 * This class represent the view GameListPage.
 *
 */
public class GameListPage extends JPanel {

	private static final long serialVersionUID = 1L;
	/**
	 * Table which contains the list of games available
	 */
	private JTable table;
	private GameListTableModel tableModel;

	private GameListHandler controller;
	
	private GameList model;

	/**
	 * Button to quit the list game lobby
	 */
	private JButton buttonQuit;
	/**
	 * Button to join the selected game
	 */
	private JButton buttonJoin;
	/**
	 * Button to join the create a game
	 */
	private JButton buttonCreate;

	public GameListPage(GameListHandler controller, GameList model) {
		this.controller = controller;
		this.model = model;

		this.buttonQuit = new JButton("Quitter");
		this.buttonJoin = new JButton("Rejoindre partie");
		this.buttonCreate = new JButton("Créer partie");

		JPanel contentPane = new JPanel(new BorderLayout());
		JPanel buttonContentPane = new JPanel(new FlowLayout());
		JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		JLabel title = new JLabel("Liste des parties");
		titlePanel.add(title);
		contentPane.add(titlePanel, BorderLayout.NORTH);
		
		buttonContentPane.add(buttonJoin);
		buttonContentPane.add(buttonQuit);
		buttonContentPane.add(buttonCreate);
		contentPane.add(buttonContentPane, BorderLayout.SOUTH);

		this.tableModel = new GameListTableModel();
		
		this.table = new JTable(tableModel);
		this.table.getTableHeader().setReorderingAllowed(false);
		this.table.setShowGrid(false);
		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer)table.getDefaultRenderer(Object.class);
	    renderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.addMouseListener(new MouseAdapter() {
			   public void mouseClicked(MouseEvent e)
			   {
				   int row = table.rowAtPoint(e.getPoint());
				   GameListPage.this.changeGameID(row);
				   
				   if(e.getClickCount() == 2) {
					   controller.join();
				   }
			   }
		   }
		);
		
		JScrollPane gameListPane = new JScrollPane(table);
		contentPane.add(gameListPane, BorderLayout.CENTER);
		
		buttonJoin.addActionListener(new ActionJoin());
		buttonCreate.addActionListener(new ActionCreate());
		buttonQuit.addActionListener(e -> this.controller.quitter());
		this.add(contentPane);
		gameListPane.setPreferredSize(new Dimension(400, 200));
	}

	/**
	 * Add a game to the Game list Table
	 * @param gameInfo	Game to add.
	 */
	public void addGame(GameListInfo gameInfo) {
		this.tableModel.insertRow(tableModel.getRowCount(), gameInfo);
	}

	public void deleteGame(int id) {
		this.tableModel.removeById(id);
	}

	/**
	 * Action performed when click on 'Join' button
	 */
	public class ActionJoin implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			controller.join();
		}
	}

	/**
	 * Action performed when clicking on 'Créer partie" button
	 */
	public class ActionCreate implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String  name = JOptionPane.showInputDialog("Veuillez entrer le nom de votre partie");
			if(name != null) {
				controller.createGame(name);
			}
		}
	}

	/**
	 * Called by the controller to notify that model data has changed
	 */
	public void notifyGameListChanged() {
		tableModel.clear();
		List<GameListInfo> listInfo = model.getGameList();
		for(GameListInfo gameInfo : listInfo) {
			this.addGame(gameInfo);
		}
		this.enableJoinButton(this.controller.isGameJoinable());
	}

	/**
	 * Change the track of the current selected game. Send it to controller.
	 * @param row	Row which has been clicked
	 */
	private void changeGameID(int row) {
		if(!tableModel.getStatusFromRow(row).equals("En jeu")) {
			controller.setIdSelectedGame(tableModel.getIdFromRow(row));
		} else {
			controller.setIdSelectedGame(-1);
		}
	}

	/**
	 * Change button state
	 * @param boobool	New state
	 */
	public void enableJoinButton(boolean boobool) {
		this.buttonJoin.setEnabled(boobool);
	}

}
