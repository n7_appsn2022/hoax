package view;

import javax.swing.JFrame;

import model.Character;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/**
 * This class manage the roles dialog.
 *
 */
public class GameRolesDialog extends HTMLDialog {

	private static final long serialVersionUID = 7492670607377641125L;
	
	private String baseText;

	public GameRolesDialog(JFrame parent, String title, String path) {
		super(parent, title, path);
		
		this.baseText = this.textPane.getText();

		this.textPane.setText(this.baseText);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				GameRolesDialog.this.setVisible(false);
			}
		});
	}
	/**
	 * This method set the panel text with the newly block characters.
	 * @param blockedCharacter
	 */
	public void setBlockedCharacters(Character[] blockedCharacter) {

		StringBuilder sb = new StringBuilder();
		sb.append(this.baseText);
		
		for(Character character : blockedCharacter) {
			sb.insert(1148, "#" + character.getServerName() + " td {background-color: #A9A9A9}");
		}
		
		this.textPane.setText(sb.toString());
	}
}
