package view;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
/**
 * With this class we can print an HTML page within a dialog.
 *
 */
public class HTMLDialog extends JDialog {
	private static final long serialVersionUID = -888159308559911062L;
	
	protected JTextPane textPane;
	
	public HTMLDialog(JFrame parent, String title, String path) {
		this(parent, title, path, false);
	}

	public HTMLDialog(JFrame parent, String title, String path, boolean modal) {
		super(parent, title, modal);
		
		BufferedReader reader;
		String line;
		StringBuilder sb = new StringBuilder();
		
		try {
			reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path),"UTF-8"));
			
			while((line = reader.readLine()) != null) {
				sb.append(line);
			}
		}
		catch(FileNotFoundException e) {
			System.err.println("Fichier des rôles introuvable.");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		this.textPane = new JTextPane();
		this.textPane.setContentType("text/html");
		this.textPane.setText(sb.toString());
		this.textPane.setEditable(false);
		this.textPane.setCaretPosition(0);
	    
	    JScrollPane scrollPane = new JScrollPane(this.textPane);
	    add(scrollPane);
		
	    setSize(600, 615);
	    setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
}
