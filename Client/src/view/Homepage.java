package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.*;

import javax.swing.*;

import controller.HomepageHandler;
import controller.LauncherClient;
import network.NetworkClient;
/**
 * This class manage the view Homepage.
 *
 */
public class Homepage extends JPanel {

	private static final long serialVersionUID = 2131886491745757891L;

	/**
	 * Handler
	 */
	private HomepageHandler handler;

	/**
	 * Path to game main image.
	 */
	private static final String mainImgPath = "/hoaxMainImg.jpeg";

	/**
	 * Basic placeholder.
	 */
	private final String PSEUDOPLACEHOLDER = "Pseudo";
	
	/**
	 * Button to click to play once the user has chosen its pseudo.
	 */
	JButton playButton;

	// Constructor
	public Homepage(HomepageHandler handler) {
		this.handler = handler;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		// Print tittle img
		JLabel imgPanel = new JLabel(new ImageIcon(getClass().getResource(mainImgPath)));
		imgPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(imgPanel);

		// Pseudo textfield
		JTextField usernameField = new PlaceholderTextField(PSEUDOPLACEHOLDER);
		usernameField.setMaximumSize(new Dimension(usernameField.getMaximumSize().width, usernameField.getPreferredSize().height));
		usernameField.addKeyListener(validateUsernameByKeyPressing(usernameField));
		this.add(usernameField);

		// Play button
		playButton = new JButton("Jouer");
		playButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		playButton.addActionListener(playActionListener(usernameField));
		this.add(playButton);

		// Rules button
		JButton rulesButton = new JButton("Régles du jeu");
		rulesButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		rulesButton.addActionListener(rulesActionListener());
		this.add(rulesButton);

		JMenuBar mb = new JMenuBar();
		JMenu options = new JMenu("Options");
		JMenuItem ipChangeItem = new JMenuItem("Changer d'IP");
		ipChangeItem.addActionListener(e -> {
			String IP = JOptionPane.showInputDialog(LauncherClient.getMainFrame(), "Modifier l'adresse IP du serveur", NetworkClient.getInstance().getIP());
			if(IP != null && IP.matches("^(\\d{1,3}\\.){3}\\d{1,3}$")) {
				NetworkClient.getInstance().setIP(IP);
			}
		});
		
		mb.add(options);
		options.add(ipChangeItem);
		LauncherClient.getMainFrame().setJMenuBar(mb);
	}

	/**
	 * Return the listener to display rules
	 * @return The Listener
	 */
	private ActionListener rulesActionListener() {
		return e -> new HTMLDialog(LauncherClient.getMainFrame(), "Règles", "/text/rules.html", true);
	}

	/**
	 * Return the listener to connect to the server.
	 * @param pfComponent	The username to enter in the lobby
	 * @return 	The Listener
	 */
	private ActionListener playActionListener(JTextField pfComponent) {
		return e -> validateUsername(pfComponent);
	}

	/**
	 * Listener to validate username on key pressing "Enter"
	 * @param pfComponent	Username to validate
	 * @return	The Listener
	 */
	private KeyListener validateUsernameByKeyPressing(JTextField pfComponent) {
		return new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					validateUsername(pfComponent);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					validateUsername(pfComponent);
				}
			}
		};
	}

	/**
	 * Check if the chosen username is valid.
	 * @param pfComponent	Username to check.
	 */
	private void validateUsername(JTextField pfComponent) {
		String username = pfComponent.getText();
		if (username == null ||  username.isEmpty()) {
			this.noPseudoAlert();
		} 
		else if (!username.matches("^[a-zA-Z0-9]+$")) {
			this.invalidPseudoAlert(username);
		}else if(username.length() > 20){
			this.tooLongPseudo();
		}
		else {
			username = pfComponent.getText();
			handler.playAction(username);
		}
	}

	/**
	 * Function called when chosen pseudo is too long.
	 * @param message	Message to display
	 */
	private void tooLongPseudo() {
		JOptionPane.showMessageDialog(this.getParent(), "Pseudo trop long ! Il doit être inférieur à 20 caractères.");
	}

	/**
	 * Function called when chosen pseudo is already chosen on server side
	 * @param message	Message to display
	 */
	public void alerteMessage(String message) {
		JOptionPane.showMessageDialog(this.getParent(), message);
	}

	/**
	 * Function called when pseudo is invalid.
	 * @param pfUsername	Invalid username
	 */
	public void invalidPseudoAlert(String pfUsername) {
		JOptionPane.showMessageDialog(this.getParent(),
				pfUsername + " est invalide. Veuillez n'utiliser que des caractères alphanumériques.");
	}

	/**
	 * Function called when pseudo is empty
	 */
	public void noPseudoAlert() {
		JOptionPane.showMessageDialog(this.getParent(), "Veuillez saisir votre pseudo!");
	}
	
	public void enablePlayedButton(boolean e) {
		playButton.setEnabled(e);
	}
}
