package view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import model.Character;
/**
 * This class represent the dialog of the investigation process.
 *
 */
public class InvestigationDialog extends JDialog {
	
	private static final long serialVersionUID = 7465743892861427202L;

	public InvestigationDialog(JFrame parent, Character c1, Character c2, Character c3, Character c4) {
		super(parent, "Enquête", true);
		
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		JButton okButton = new JButton("Ok");
		okButton.setMaximumSize(new Dimension(100, 100));
		okButton.addActionListener(e -> dispose());
		
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panel.add(okButton);
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JLabel title = new JLabel("Résultat de l'enquête", SwingConstants.CENTER);
		Font font = new Font(title.getFont().getName(), Font.BOLD, 30);
		title.setFont(font);
		contentPane.add(title, BorderLayout.NORTH);
		
		JPanel picturesArea = new JPanel(new FlowLayout());
		contentPane.add(picturesArea, BorderLayout.CENTER);
		
		picturesArea.add(getPictureFromCharacter(c1));
		picturesArea.add(getPictureFromCharacter(c2));
		picturesArea.add(getPictureFromCharacter(c3));
		picturesArea.add(getPictureFromCharacter(c4));
		pack();
		setVisible(true);
		
	}
	/**
	 * From a given character, take a picture with its name and open it.
	 * Creates a JLabel which contains it in a certain size
	 * @param character	Character to get picture from
	 * @return	A JLabel with the picture as JIcon of it
	 */
	private JLabel getPictureFromCharacter(Character character) {
		String pathToPicture = "/" + character.getName() + ".png";
		Image image = new ImageIcon(getClass().getResource(pathToPicture)).getImage();
		image = image.getScaledInstance(240, 355, Image.SCALE_SMOOTH);
		
	    return new JLabel(new ImageIcon(image));
	}
}
