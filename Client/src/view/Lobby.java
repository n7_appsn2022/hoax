package view;

import controller.LobbyHandler;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

public class Lobby extends JPanel {

  /**
   *
   */
  private static final long serialVersionUID = 1165560960464297581L;
  static final int NB_PLAYER_MAX = 6;
  /**
   * Controller of the view
   */

  private LobbyHandler controller;
  /**
   * Table which contains the list of player
   */

  private JTable table;
  private DefaultTableModel tableModel;
  
  private JLabel hostName;
  /**
   * Button to quit the lobby
   */
  private JButton boutonQuitter;
  /**
   * Button for the user if he's the host to launch the game
   */
  private JButton boutonLancer;

  /**
   * Create the UI of the waiting room.
   * @param controller  Controller
   */
  public Lobby(LobbyHandler controller) {
    this.controller = controller;

    ArrayList<String>  data = controller.getPlayers();
    /*
     * Table of data
    */
    this.tableModel = new DefaultTableModel();
    this.table = new JTable(tableModel);
    this.table.setShowGrid(false);
    tableModel.addColumn("Joueurs");
    for (String dataString : data) {
    	this.addPlayer(dataString);
    }
    this.table.setEnabled(false);

    /*
     * Button which allow to quit the room
     */
    this.boutonQuitter = new JButton("Quitter");
    this.boutonQuitter.addActionListener(e -> this.controller.quitter());
    /*
     * Button which allow to the host to launch the game for every other player
     */
    this.boutonLancer = new JButton("Lancer la partie");
    this.boutonLancer.addActionListener(e -> controller.launch());
    this.setIsLobbyHostAndEnoughPlayers();

    JLabel title = new JLabel("Partie " + this.controller.getGameName());
    JPanel north = new JPanel(new BorderLayout());
    north.add(title, BorderLayout.WEST);
    hostName = new JLabel("Host : " + this.controller.getHost());
    north.add(hostName, BorderLayout.EAST);
    
    /*
     * contentPane of the frame
     */
    JPanel contentPane = new JPanel();

    contentPane.setLayout(new BorderLayout());
    /*
     * Displays the table
     */
    JScrollPane centre = new JScrollPane(table);
    centre.setPreferredSize(new Dimension(400, 200));
    /*
     * Space under the table, it displays different buttons
     */
    JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
    south.add(this.boutonLancer);
    south.add(this.boutonQuitter);

    contentPane.add(north, BorderLayout.NORTH);
    contentPane.add(centre, BorderLayout.CENTER);
    contentPane.add(south, BorderLayout.SOUTH);

    add(contentPane);

  }

  /**
   * Add a string in the model of the table
   * @param players Add a new player to the list of player
   */
  public void addPlayer(String players) {
    this.tableModel.insertRow(tableModel.getRowCount(), new String[] { players });
  }
  /**
   * Set a new value for the host label
   */
  private void setHost(String newHost) {
      hostName.setText("Host : "+newHost);
  }

  /**
   * Remove a String from the model
   * @param player  Delete a player from the list
   */
  public void deletePlayer(String player) {
    for(int i = 0 ; i < this.tableModel.getRowCount() ; i++)
      if(player.equals(tableModel.getValueAt(i, 0))) {
        this.tableModel.removeRow(i);
      }
  }
/**
 * Verify if the user is the host of the game, if he is, enable the possibilité to launch the game. Else, the button is disabled.
 */
  public void setIsLobbyHostAndEnoughPlayers() {
    if (this.controller.isLobbyHost() && this.controller.getPlayers().size() > 2)
      this.boutonLancer.setEnabled(true);
    else
      this.boutonLancer.setEnabled(false);
  }
  
  public void notifyPlayerChanged(String newHost) {
    int nbPlayers = this.tableModel.getRowCount();
    setHost(newHost);
    for(int i = 0 ; i < nbPlayers ; i++)
        this.tableModel.removeRow(0);
    for (String player : this.controller.getPlayers())
      this.tableModel.insertRow(tableModel.getRowCount(), new String[] { player });
    this.setIsLobbyHostAndEnoughPlayers();
  }

}
