package view;

import controller.LauncherClient;
import controller.PlayerBoardHandler;
import controller.PlayerBoardQuerier;
import model.Character;
import model.Game;
import model.Player;
import model.ResourcesType;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;

/**
 * This class is built to manage the UI for a playing game. It represents the
 * player board of the player. It allows the user to see informations of himself
 * and other players.
 *
 */
public class PlayerBoard extends JPanel {

    private static final long serialVersionUID = 1L;

    private Player player;

    // the controller
    private PlayerBoardQuerier controller;

    // button that allow the player to achieve certain actions
    private JButton bAnnounce;
    private JButton bDenounce;
    private JButton bInvestiagate;
    private JButton bPassTurn;
    private JTextPane log;

    // Jtable that represent the resources tab or the bank tab
    private JTable tabResource;

    // Label to display the bank ressources
    private JLabel cashLabel;
    private JLabel prestigeLabel;
    private JLabel evidenceLabel;
    private DefaultTableModel tableModel;
    private Game gameModel;
    private JDialog currentJDialog;
    private GameRolesDialog gameRolesDialog;

    /**
     * PalyerGUI's constructor
     *
     * @param gameModel model
     */
    public PlayerBoard(Game gameModel) {
        this.gameModel = gameModel;
        this.controller = new PlayerBoardQuerier(gameModel);
        this.player = gameModel.getCurrentUser();
        // view buttons

        bAnnounce = new JButton("Annoncer");
        bDenounce = new JButton("Dénoncer");
        bInvestiagate = new JButton("Enquêter");

        bAnnounce.setEnabled(true);
        bDenounce.setEnabled(true);
        bInvestiagate.setEnabled(false);

        log = new JTextPane();
        log.setPreferredSize(new Dimension(200, 400));
        log.setEditable(false);

        JScrollPane scrollLog = new JScrollPane(log);

        this.tableModel = new DefaultTableModel();
        // array of each player's resources
        String[] title = {"Joueur", ResourcesType.EVIDENCE.getName(), ResourcesType.PRESTIGE.getName(),
                ResourcesType.CASH.getName()};

        for (String s : title) {
            tableModel.addColumn(s);
        }
        Object[][] resources = this.getResourceTab();
        for (int i = 0; i < resources.length; i++) {
            tableModel.insertRow(i, resources[i]);
        }
        this.updatePlayerResources();

        // view elements

        this.setLayout(new BorderLayout());
        JPanel playerNamePane = new JPanel(new FlowLayout());
        JPanel buttonContentPane = new JPanel(new FlowLayout());
        JPanel characterInfoContentPane = new JPanel(new BorderLayout());

        // resources container
        JPanel resourceContentPane = new JPanel(new BorderLayout());

        // initialize the resource's pictures

        ImageIcon cashIcon = getRessourcePicture("/cash.png");
        ImageIcon prestigeIcon = getRessourcePicture("/prestige.png");
        ImageIcon evidenceIcon = getRessourcePicture("/evidence.png");

        // Initialize labels which will display the amount of resources
        cashLabel = new JLabel();
        prestigeLabel = new JLabel();
        evidenceLabel = new JLabel();

        this.updateBank();

        // Initialize labels which will display pictures for resources
        JLabel cashIconLabel = new JLabel(cashIcon);
        cashIconLabel.setToolTipText(ResourcesType.CASH.getName());
        JLabel prestigeIconLabel = new JLabel(prestigeIcon);
        prestigeIconLabel.setToolTipText(ResourcesType.PRESTIGE.getName());
        JLabel evidenceIconLabel = new JLabel(evidenceIcon);
        evidenceIconLabel.setToolTipText(ResourcesType.EVIDENCE.getName());

        Dimension dimensionDisplayBank = new Dimension(110, 120);

        // Initialize layouts to display the amount of resources below each picture
        JPanel cashLayout = new JPanel();
        cashLayout.setLayout(new BoxLayout(cashLayout, BoxLayout.Y_AXIS));
        cashLayout.add(cashIconLabel);
        JPanel labelCLayout = new JPanel();
        labelCLayout.add(cashLabel);
        cashLayout.add(labelCLayout);

        cashLayout.setPreferredSize(dimensionDisplayBank);

        JPanel evidenceLayout = new JPanel();
        evidenceLayout.setLayout(new BoxLayout(evidenceLayout, BoxLayout.Y_AXIS));
        evidenceLayout.add(evidenceIconLabel);
        JPanel labelELayout = new JPanel();
        labelELayout.add(evidenceLabel);
        evidenceLayout.add(labelELayout);
        evidenceLayout.setPreferredSize(dimensionDisplayBank);

        JPanel prestigeLayout = new JPanel();
        prestigeLayout.setLayout(new BoxLayout(prestigeLayout, BoxLayout.Y_AXIS));
        prestigeLayout.add(prestigeIconLabel);
        JPanel labelPLayout = new JPanel();
        labelPLayout.add(prestigeLabel);
        prestigeLayout.add(labelPLayout);
        prestigeLayout.setPreferredSize(dimensionDisplayBank);

        this.bPassTurn = new JButton("Fin du tour");
        JButton bShowGrid = new JButton("Montrer la grille");

        // initialize the picture of the player
        JLabel playerCharacter = getPicture(gameModel.getCharacter().getName());

        // initialization of the bank tab
        tabResource = new JTable(tableModel);
        tabResource.setPreferredScrollableViewportSize(tabResource.getPreferredSize());
        tabResource.setEnabled(false);

        // Initialize the panel which will contains the bank
        JPanel displayImages = new JPanel();
        displayImages.setLayout(new FlowLayout());

        displayImages.add(evidenceLayout);
        displayImages.add(prestigeLayout);
        displayImages.add(cashLayout);
        displayImages.setBorder(BorderFactory.createTitledBorder("Banque"));

        resourceContentPane.add(displayImages, BorderLayout.NORTH);
        resourceContentPane.add(scrollLog, BorderLayout.CENTER);
        resourceContentPane.add(new JScrollPane(tabResource), BorderLayout.SOUTH);
        resourceContentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        buttonContentPane.add(bAnnounce);
        buttonContentPane.add(bDenounce);
        buttonContentPane.add(bInvestiagate);
        buttonContentPane.add(bPassTurn);

        characterInfoContentPane.add(playerCharacter, BorderLayout.CENTER);
        characterInfoContentPane.add(bShowGrid, BorderLayout.SOUTH);

        playerNamePane.add(new JLabel("Vous êtes : " + player.getName()));

        this.add(playerNamePane, BorderLayout.NORTH);
        this.add(characterInfoContentPane, BorderLayout.WEST);
        this.add(resourceContentPane, BorderLayout.EAST);
        this.add(buttonContentPane, BorderLayout.SOUTH);

        // Action Listenners
        bAnnounce.addActionListener(new ActionAnnounceCharacter());
        bDenounce.addActionListener(new ActionDenouncePlayer());
        bInvestiagate.addActionListener(new ActionInvestigate());
        bPassTurn.addActionListener(new ActionPass());
        bShowGrid.addActionListener(new ActionShowGrid());
        this.disableActions();

        this.gameRolesDialog = new GameRolesDialog(null, "Rôles", "/text/role.html");
        this.gameRolesDialog.setVisible(false);
    }

    /**
     * Ask the model for the new bank state and then update the view.
     */
    public void updateBank() {
        int[] resources = this.getTheBank();
        evidenceLabel.setText(Integer.toString(resources[0]));
        cashLabel.setText(Integer.toString(resources[1]));
        prestigeLabel.setText(Integer.toString(resources[2]));

    }

    /**
     * Update the JTable which contains resources of players
     */
    public void updatePlayerResources() {
        Object[][] resources = this.getResourceTab();
        for (int i = 0; i < resources.length; i++) {
            for (int j = 0; j < 4; j++) {
                tableModel.setValueAt(resources[i][j], i, j);
            }
        }
        if (canInvestigate() && gameModel.isMyTurn()) {
            bInvestiagate.setEnabled(true);
        } else {
            bInvestiagate.setEnabled(false);
        }
    }

    /**
     * Resizes an image with the name imageName
     *
     * @param imageName Path to get resource from
     * @return The icon with the size 100*100
     */
    private ImageIcon getRessourcePicture(String imageName) {
        ImageIcon icon = new ImageIcon(getClass().getResource(imageName));
        Image image = icon.getImage();
        BufferedImage bl = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bl.createGraphics();
        g.drawImage(image, 0, 0, 100, 100, null);

        return new ImageIcon(bl);
    }

    /**
     * Get the picture for a character
     *
     * @param character Character name to identify the right image
     * @return A JLabel with the picture in it
     */
    public JLabel getPicture(String character) {
        String pathToPicture = "/" + character + ".png";
        Image image = new ImageIcon(getClass().getResource(pathToPicture)).getImage();
        image = image.getScaledInstance(240, 355, Image.SCALE_SMOOTH);

        return new JLabel(new ImageIcon(image));
    }

    /**
     * Notify when a new Round start, meaning the last player finished his turn.
     */
    public void notifyNextRound() {
        if (this.gameModel.isMyTurn())
            this.startTurn();
        else
            this.disableActions();
    }

    /**
     * Called when receive from the server a player Elimination update Model and
     * notify View
     *
     * @param eliminatedPlayer Player eliminated
     */
    public void notifyPlayerEliminated(String eliminatedPlayer) {
        printLog(eliminatedPlayer + " a été éliminé", PlayerBoardHandler.EliminateColor);
        if (LauncherClient.getUser().contentEquals(eliminatedPlayer)) {
            disableActions();
            JOptionPane.showMessageDialog(this, "Vous avez été éliminé");
        }
        this.updatePlayerResources();

    }

    /**
     * Called when a Player win the party (received from server)
     *
     * @param winner Pseudo of the winner
     */
    public void notifyWin(String winner) {
        JOptionPane.showMessageDialog(null, "Le joueur " + winner + " a gagné ! ");
        this.disableActions();

        controller.win();

    }

    /**
     * Called when the server send us the result of our investigation.
     *
     * @param characters Character resulting from our Investigation
     */
    public void notifyInvestigation(String[] characters) {
        printLog("Vous effectuez une enquête", PlayerBoardHandler.InvestigateColor);
        Character c1 = Character.getFromName(characters[0]);
        Character c2 = Character.getFromName(characters[1]);
        Character c3 = Character.getFromName(characters[2]);
        Character c4 = Character.getFromName(characters[3]);
        new InvestigationDialog(LauncherClient.getMainFrame(), c1, c2, c3, c4);
    }

    /**
     * Write information in the logs
     *
     * @param message message to write.
     * @param color   color of the message
     */
    public void printLog(String message, Color color) {
        StyledDocument doc = this.log.getStyledDocument();
        Style style = this.log.addStyle(null, null);
        StyleConstants.setForeground(style, color);

        try {
            doc.insertString(doc.getLength(), message + "\n", style);
            this.log.setCaretPosition(doc.getLength());
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Change the state of buttons. It disables them
     */
    public void disableActions() {
        this.disableAnnonce();
        this.disableDenunciation();
        this.bInvestiagate.setEnabled(false);
        this.bPassTurn.setEnabled(false);
    }

    /**
     * Change the state of the buttons at the begining of the turn. It enables them
     */
    public void startTurn() {
        this.bAnnounce.setEnabled(true);
        this.bDenounce.setEnabled(true);
        if (canInvestigate()) {
            this.bInvestiagate.setEnabled(true);
        } else {
            this.bInvestiagate.setEnabled(false);
        }
        this.bPassTurn.setEnabled(false);
    }

    /**
     * Says if the current player can investigate. It verifies if the player has 1
     * of each resources.
     */
    private boolean canInvestigate() {
        boolean canInvestigate = true;
        Player user = gameModel.getCurrentUser();
        Set<ResourcesType> currentResources = user.getResources().keySet();
        for (ResourcesType r : currentResources) {
            if (user.getResource(r) == 0) {
                canInvestigate = false;
            }
        }
        return canInvestigate;
    }

    /**
     * Verify if buttons are enabled. If all of then are enabled, throws an
     * exception
     *
     * @return The state of the button announce
     * @throws Exception If every buttons are enabled
     */
    public boolean areButtonEnabled() throws Exception {
        if (this.bAnnounce.isEnabled() == this.bDenounce.isEnabled() == this.bInvestiagate.isEnabled() != this.bPassTurn
                .isEnabled())
            throw new Exception();
        return this.bAnnounce.isEnabled();
    }

    /**
     * Disable the button Denounce. This occurs when it's not the turn of the owner
     * of the client.
     */
    public void disableDenunciation() {
        this.bDenounce.setEnabled(false);
    }

    /**
     * Return the state of the bank.
     *
     * @return A table of int of length 3, the index 0 is for the amount of
     * evidence, 1 for cash and 2 for prestige
     */
    public int[] getTheBank() {
        int[] dataBank = new int[3];

        dataBank[0] = gameModel.getResources().get(ResourcesType.EVIDENCE);
        dataBank[1] = gameModel.getResources().get(ResourcesType.CASH);
        dataBank[2] = gameModel.getResources().get(ResourcesType.PRESTIGE);

        return dataBank;
    }

    /**
     * Disable the button announce. It occurs when it's not the turn of the user.
     */
    public void disableAnnonce() {
        this.bAnnounce.setEnabled(false);
    }

    /**
     * Gives a table with the amount of resources for each player
     *
     * @return Return updated data for bank view
     */
    public Object[][] getResourceTab() {
        Object[][] data = new Object[gameModel.getNbPlayer()][4];
        int cpt = 0;
        for (Player x : gameModel.getRemainingPlayers()) {

            data[cpt][0] = x.getName();
            data[cpt][1] = x.getResources().get(ResourcesType.EVIDENCE);

            data[cpt][2] = x.getResources().get(ResourcesType.PRESTIGE);

            data[cpt][3] = x.getResources().get(ResourcesType.CASH);
            cpt++;

        }

        return data;
    }

    /**
     * This method allow us to manage the case when an announce has been made by an other player.
     *
     * @param nomJoueurAnnonce Name of the Announcing Player
     * @param roleAnnonce      Announced Character
     * @param data             Data relative to the Announce
     */
    public void notifyAnnounce(String nomJoueurAnnonce, Character roleAnnonce, String[] data) {
        String message = nomJoueurAnnonce + " annonce être " + roleAnnonce.getFrenchName() + " ";
        switch (roleAnnonce) {
            case theButler:

                message += "et souhaite partager un " + ResourcesType.getFrenchNameFromServerName(data[0]) + " avec "
                        + data[1];

                break;
            case theChief:
                int quantite = Integer.parseInt(data[2]);
                String cible;
                if (data.length == 4) {
                    cible = data[3];
                } else {
                    cible = "Banque";
                }
                message += "et souhaite échanger " + quantite + " de " + ResourcesType.getFrenchNameFromServerName(data[0])
                        + "avec autant de " + ResourcesType.getFrenchNameFromServerName(data[1]) + " appartenant à "
                        + cible;
                break;
            case theGardener:
                Player joueurCible = gameModel.getRemainingPlayerByName(data[1]);
                int nbResource = joueurCible.getResource(ResourcesType.getFromName(data[0]));
                int quantiteVolee = (nbResource >= 2) ? 2 : 1;
                message += "et souhaite prendre " + quantiteVolee + " de "
                        + ResourcesType.getFrenchNameFromServerName(data[0]) + " à " + data[1];

                break;
            case theEx:

                if (data.length == 1) {
                    message += "et va prendre 1 " + ResourcesType.getFrenchNameFromServerName(data[0]) + " à la banque";

                } else {
                    Player annonceur = gameModel.getCurrentlyPlaying();
                    List<Player> remainingPlayers = gameModel.getRemainingPlayers();
                    int indexAnnonceur = remainingPlayers.indexOf(annonceur);
                    int indexInf = Math.floorMod((indexAnnonceur - 1), remainingPlayers.size());
                    int indexSup = Math.floorMod((indexAnnonceur + 1), remainingPlayers.size());
                    String playerInf = remainingPlayers.get(indexInf).getName();
                    String playerSup = remainingPlayers.get(indexSup).getName();
                    message += "et va prendre une ressource aléatoire à " + playerInf + " et " + playerSup;
                }
                break;
            case theSonInLaw:
                message += "et va prendre 1 " + ResourcesType.getFrenchNameFromServerName(data[0]) + " à chaque joueur";
                break;
            default:
                message += "et va prendre 1 " + ResourcesType.getFrenchNameFromServerName(data[0]) + " à la banque";
                break;
        }

        String messageVote = message + "\nVeuillez indiquer si vous votez pour ou contre cette annonce";
        String[] voteResult = {"Pour", "Contre"};
        printLog(message, PlayerBoardHandler.AnnounceColor);

        if (!gameModel.getCurrentUser().isEliminated()) {

            new Thread(() -> {
                final JOptionPane optionPane = new JOptionPane(messageVote, JOptionPane.QUESTION_MESSAGE,
                        JOptionPane.YES_NO_CANCEL_OPTION, null, voteResult, voteResult[0]);

                final JDialog dialog = new JDialog(LauncherClient.getMainFrame(), "Click a button", true);
                dialog.setLocationRelativeTo(LauncherClient.getMainFrame());
                currentJDialog = dialog;

                dialog.setContentPane(optionPane);
                dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                dialog.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent we) {
                    }
                });
                optionPane.addPropertyChangeListener(e -> {
                    String prop = e.getPropertyName();
                    if (dialog.isVisible() && (e.getSource() == optionPane)
                            && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                        dialog.dispose();
                    }
                });
                dialog.pack();
                dialog.setVisible(true);

                String vote = optionPane.getValue() == null ? "Pour" : (String) optionPane.getValue();

                if (vote != null) {
                    vote = (vote.equals(voteResult[0])) ? "Agree" : "Revoke";

                    controller.vote(vote);
                }
                currentJDialog = null;
            }).start();
        }
    }

    /**
     * Ask the player if he wants to be immune to a stealing action
     */
    public void notifyStealing() {

        String[] voteResult = {"Oui", "Non"};
        String messageVote = gameModel.getCurrentlyPlaying().getName()
                + " souhaite vous voler une ressource\nVoulez-vous clamer une immunité ?";
        final JOptionPane optionPane = new JOptionPane(
                messageVote,
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_CANCEL_OPTION, null, voteResult, voteResult[0]);

        final JDialog dialog = new JDialog(LauncherClient.getMainFrame(), "Click a button", true);
        dialog.setLocationRelativeTo(this);
        currentJDialog = dialog;

        dialog.setContentPane(optionPane);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent we) {
                //Have to do nothing when attempting to close
            }
        });


        optionPane.addPropertyChangeListener(e -> {
            String prop = e.getPropertyName();
            if (dialog.isVisible() && (e.getSource() == optionPane) && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                dialog.dispose();
            }
        });
        dialog.pack();
        dialog.setVisible(true);
        String vote = optionPane.getValue() == null ? "Non" : (String) optionPane.getValue();
        if (vote != null) {
            vote = (vote.equals(voteResult[0])) ? "Yes" : "No";

            controller.immunity(vote);
        }
        currentJDialog = null;
    }

    /**
     * Ask the player if he thinks that players are lying on their immunity
     */
    public void notifyAcceptationImmunity(String[] players) {
        DialogAcceptationImmunity dialog = new DialogAcceptationImmunity(LauncherClient.getMainFrame(), players);
        controller.acceptationImmunity(dialog.getResult());
        bPassTurn.setEnabled(true);
        bDenounce.setEnabled(true);

    }

    /**
     * Return the controller for the user interaction
     *
     * @return The Controller
     */
    public PlayerBoardQuerier getQuierier() {
        return this.controller;
    }

    /**
     * Notify when your Announce and Privilege had been executed
     */
    public void notifyCanPassTurn() {
        if (gameModel.getCurrentlyPlaying().equals(gameModel.getCurrentUser())) {
            bPassTurn.setEnabled(true);
            bDenounce.setEnabled(true);
        }
    }

    public void closeCurrentJDialog() {
        if (currentJDialog != null)
            currentJDialog.dispose();
    }

    public void notifyBlockedCharacter() {
        gameRolesDialog.setBlockedCharacters(gameModel.getCurrentUser().getBlockedCharacters());
    }

    /**
     * This class is built to manage the action of announcing a character
     *
     * @author Awa Tidjani, Michael MORA
     */
    public class ActionAnnounceCharacter implements ActionListener {
        String[] args = new String[10];

        @Override
        public void actionPerformed(ActionEvent e) {
            this.args = new String[this.args.length];
            DialogAnnounce dialogAnnounce = new DialogAnnounce(LauncherClient.getMainFrame(), gameModel);
            if (dialogAnnounce.getAnnounceResult()) {

                Character chosenCharacter = dialogAnnounce.getCharacterSelected();
                int indexPrivilege = dialogAnnounce.getIndexPrivilege();
                String character;
                if (chosenCharacter != null) {
                    // character = chosenCharacter.getServerName();
                    character = chosenCharacter.toString();
                    switch (chosenCharacter) {
                        case theEx:

                            if (indexPrivilege == 0) {
                                args[0] = "Cash";
                            } else if (indexPrivilege == 1) {
                                args[0] = "Evidence";
                            } else {
                                args[0] = "";

                            }
                            break;

                        case theLover:
                            if (indexPrivilege == 0) {
                                args[0] = "Evidence";
                            } else {
                                args[0] = "Prestige";

                            }
                            break;
                        case theDistantCousin:

                            if (indexPrivilege == 0) {
                                args[0] = "Cash";
                            } else {
                                args[0] = "Prestige";

                            }
                            break;
                        case theSonInLaw:

                            args[0] = dialogAnnounce.getResourceCibled();
                            break;
                        case theChief:
                            args[0] = dialogAnnounce.getResourceCibled();
                            args[1] = dialogAnnounce.getOtherResourceCibled();
                            args[2] = Integer.toString(dialogAnnounce.getResourceQuantity());
                            args[3] = dialogAnnounce.getPlayerCibled();
                            break;
                        case theGardener:
                        case theButler:

                            args[0] = dialogAnnounce.getResourceCibled();
                            args[1] = dialogAnnounce.getPlayerCibled();
                            break;
                    }
                    // pass to the controller the announced character and the args
                    controller.announce(character, args);
                    disableAnnonce();
                    disableDenunciation();

                }
            }
        }

    }

    /**
     * This class is built to manage the action of denouncing
     *
     * @author Awa Tidjani
     */
    public class ActionDenouncePlayer implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFrame frame = new JFrame();
            String playerGiven;
            playerGiven = (String) JOptionPane.showInputDialog(frame, "Choisir le joueur à dénoncer",
                    "Sélection de joueur", JOptionPane.PLAIN_MESSAGE, null, gameModel.getOpponentsNames().toArray(),
                    "");
            if (controller.isValid(playerGiven)) {
                String characterGiven = (String) JOptionPane.showInputDialog(frame, "Choisir le rôle dénoncé :",
                        "Choisir le rôle", JOptionPane.PLAIN_MESSAGE, null, Character.getFrenchNames(), "The Ex");
                controller.denounce(playerGiven, Character.getFromName(characterGiven).toString());
                PlayerBoard.this.disableDenunciation();
                disableDenunciation();

            }
        }
    }

    /**
     * This class is built to manage the action of investigate
     *
     * @author Awa Tidjani , Michael MORA
     */
    public class ActionInvestigate implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFrame frame = new JFrame();
            String targetedPlayer;

            targetedPlayer = (String) JOptionPane.showInputDialog(frame, "Choisir la cible de l'enquête",
                    "Sélection de la cible", JOptionPane.PLAIN_MESSAGE, null, gameModel.getOpponentsNames().toArray(),
                    "");
            if (targetedPlayer != null) {
                controller.investigate(targetedPlayer);
            }
        }
    }

    /**
     * This class is built to manage the action of passing the turn
     *
     * @author Awa Tidjani
     */
    public class ActionPass implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            controller.passTurn();
        }

    }

    /**
     * This class is built to manage the action when the button "Show grid" is
     * activated. That class will show all the available characters and their
     * privilege, but also the character that they are immune to if they possess any
     * immunity
     */
    public class ActionShowGrid implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!gameRolesDialog.isVisible())
                gameRolesDialog.setVisible(true);
            else
                gameRolesDialog.toFront();
        }
    }

}
