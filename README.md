# Hoax

Le jeu Hoax est un jeu d’enquête où chaque joueur se voit attribuer un rôle secret et doit faire en sorte d’éliminer les autres joueurs en trouvant leur rôle via un système d’enquête.

Une démonstration vidéo est disponible [ici sur youtube](https://youtu.be/5QTwoL6hlks).

Ce projet a été réalisé dans un cadre d'études en conception et programmation objet à l'[ENSEEIHT](http://www.enseeiht.fr/fr/index.html).

[Le rapport de projet](https://gitlab.com/n7_appsn2022/hoax/-/blob/Dev/resources/rapport.md) présente le code sources et l'architecture.  

[Le manuel utilisateur](https://gitlab.com/n7_appsn2022/hoax/-/blob/Dev/resources/rapport.md) présente le jeu et les règles.  

Le client est téléchargeable [ici](https://gitlab.com/n7_appsn2022/hoax/-/blob/Dev/livrables/Client.jar).

Nous disposons d'un serveur en ligne, mais si vous souhaiter jouer en local le serveur est téléchargeable [ici](https://gitlab.com/n7_appsn2022/hoax/-/blob/Dev/livrables/Server.jar).

Bon jeu !