
import java.util.HashMap;
import java.util.Map;

import compartment.Compartment;
import compartment.User;

import compartment.gameList.GameList;

import network.NetworkListener;
import network.SocketManager;

public class Server implements NetworkListener {
	
	private GameList gameList;
	private Map<String, User> users;
	
	/**
	 * Constructor.
	 */
	public Server() {
		this.gameList = new GameList();
		this.users = new HashMap<String, User>();
		
		SocketManager manager = SocketManager.getInstance();
		manager.addNetworkListener(this);
		manager.run();
	}

	@Override
	public void connected(String username) {
		User user = new User(username);
		this.users.put(username, user);
		this.gameList.join(user);
	}

	@Override
	public void disconnected(String username) {
		User user = this.users.remove(username);
		if(user != null) {
			Compartment compartment = user.getCompartment();
			if(compartment != null) {
				compartment.quit(user);
			}
		}
	}

	@Override
	public void messageReceived(String username, String message) {
		System.out.println(username + " -> " + message);
		try {
			User user = this.users.get(username);
			if(user != null) {
				Compartment compartment = user.getCompartment();
				if(compartment != null) {
					compartment.processMessage(username, message);
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
