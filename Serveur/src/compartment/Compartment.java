package compartment;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import network.SocketManager;

/**
 * Abstraction of a compartment of the application.
 * An user can be only in one compartment at the time.
 */
public abstract class Compartment implements MessageHandler {
	
	/**
	 * List of users in this compartment.
	 */
	protected Map<String, User> users;
	
	/**
	 * Constructor.
	 */
	public Compartment() {
		this.users = new HashMap<String, User>();
	}
	
	/**
	 * Manage when a user want to join a new compartment.
	 * 
	 * @param user User that join this compartment.
	 */
	public void join(User user) {
		Compartment previousCompartment = user.getCompartment();
		if(previousCompartment != null) {
			previousCompartment.quit(user);
		}
		user.setCompartment(this);
		this.users.put(user.toString(), user);
	}
	
	/**
	 * Manage when a user want to quit a compartment.
	 * 
	 * @param user User that quit this compartment.
	 */
	public void quit(User user) {
		this.users.remove(user.toString());
		user.setCompartment(null);
	}
	
	/**
	 * Count the number of users in this compartment.
	 * 
	 * @return the number of users.
	 */
	public int countUsers() {
		return this.users.size();
	}
	
	/**
	 * Retrieve a User by its name.
	 * 
	 * @param username Name of the user.
	 * @return the User corresponding to the username, or null if there is not User with this username.
	 */
	public User getUser(String username) {
		return this.users.get(username);
	}
	
	/**
	 * Retrieve all users in this compartment.
	 * 
	 * @return an unmodifiable Collection of all users in this compartment.
	 */
	public Collection<User> getUsers() {
		return Collections.unmodifiableCollection(this.users.values());
	}
	
	/**
	 * Send a message to all users in this compartment.
	 */
	public void writeAll(String message) {
		SocketManager manager = SocketManager.getInstance();
		for(User user : getUsers()) {
			manager.write(user.getPseudo(), message);
		}
	}
}
