package compartment;

/**
 * Interface that manage messages received by the server.
 */
public interface MessageHandler {
	/**
	 * Process a message received.
	 * 
	 * @param username Name of the user that sent this message.
	 * @param message Message to process.
	 */
	public void processMessage(String username, String message);
}
