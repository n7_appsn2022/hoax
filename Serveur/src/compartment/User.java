package compartment;

public class User {
	private String pseudo;
	private Compartment compartment;

	/**
	 * Constructor.
	 * 
	 * @param pseudo Username of this User.
	 */
	public User(String pseudo) {
		this.pseudo = pseudo;
	}
	
	/**
	 * Get the pseudo of user.
	 * 
	 * @return the username.
	 */
	public String getPseudo() {
		return this.pseudo;
	}
	
	/**
	 * Get the compartment in which is the user.
	 * 
	 * @return the compartment, or null if the user is not in a compartment.
	 */
	public Compartment getCompartment() {
		return compartment;
	}

	/**
	 * Modify the compartment of the user.
	 * 
	 * @param compartment New compartment.
	 */
	protected void setCompartment(Compartment compartment) {
		this.compartment = compartment;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.pseudo;
	}
}
