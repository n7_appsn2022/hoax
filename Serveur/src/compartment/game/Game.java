package compartment.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import compartment.Compartment;
import compartment.User;
import compartment.game.lobby.Lobby;
import compartment.game.playingGame.PlayingGame;
import compartment.gameList.GameList;

/**
 * A game has two states
 * - waiting : when the game has not started yet
 * - playing : when the game has started
 */
public class Game extends Compartment {

	/**
	 * The id of the game, iterate every time a new game is created
	 */
	private static int countGame = 0;
	
	private PlayingGame playingGame;
	private Lobby lobby;
	private boolean isPlaying;
	
	private int id;
	private String title;
	private User host;
	
	private GameList gameList;
	
	/**
	 * Initialize a Game from its Title, one player who will be the host of the waiting lobby and a listener
	 * which will listen different changes and update the game.
	 * It will also set a unique ID to the game and set the state to Waiting
	 * Finally, it will add the user in the game.
	 * 
	 * @param title Title of the game
	 * @param host Player that can run the game from the WaitingLobby
	 * @param listener Lobby that will listen the game to update itself
	 */
	public Game(String title, GameList gameList) {
		this.id = ++countGame;
		
		this.title = title;
		this.host = null;
		
		this.playingGame = new PlayingGame(this);
		this.lobby = new Lobby(this);
		
		this.isPlaying = false;
		this.gameList = gameList;
	}
	
	/**
	 * Make the host enter the game
	 * 
	 * @param host Host of the game.
	 */
	public void addHost(User host) {
		this.host = host;
		join(host);
	}
	
	/**
	 * Start the game if there are 3 players or more.
	 */
	public void start() {
		// Verify that there are enough players
		if(countUsers() >= 3) {
			changeGameStatus(true);
		}
	}
	
	/**
	 * Update status of the game. If the new status is equal to GameStatus.Playing
	 * It will start the game.
	 * 
	 * @param status New status.
	 */
	public void changeGameStatus(boolean isPlaying) {
		if(this.isPlaying != isPlaying) {
			this.isPlaying = isPlaying;
			this.gameList.statusChanged(this.id, this.isPlaying);
			
			if(this.isPlaying) {
				this.playingGame.start();
			}
		}
	}
	
	/**
	 * Allow to know if a game has started or not.
	 * 
	 * @return true if the game is running, false otherwise;
	 */
	public boolean isPlaying() {
		return this.isPlaying;
	}
	
	/**
	 * Retrieve the id of the game.
	 * 
	 * @return the unique id of the game.
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Retrieve the title of the game.
	 * 
	 * @return the title of the game.
	 */
	public String getTitle() {
		return this.title;
	}
	
	/**
	 * Retrieve the host of the game.
	 * 
	 * @return the current host of the game.
	 */
	public User getHost() {
		return host;
	}
	
	/**
	 * Retrieve the games list that contains all the games waiting for players or playing.
	 * 
	 * @return the games list.
	 */
	public GameList getGameList() {
		return this.gameList;
	}

	@Override
	public void processMessage(String username, String message) {
		if(this.isPlaying) {
			this.playingGame.processMessage(username, message);
		}
		else {
			this.lobby.processMessage(username, message);
		}
	}
	
	@Override
	public void join(User user) {
		if(!this.isPlaying) {
			super.join(user);
			this.lobby.join(user);
			
			this.gameList.playersChanged(this.id, countUsers());
		}
	}
	
	@Override
	public void quit(User user) {
		super.quit(user);
		
		List<User> users = new ArrayList<User>(getUsers());
		if(!users.isEmpty()) {
			if(user == this.host) {
				Collections.shuffle(users);
				this.host = users.get(0);
				writeAll("Disconnect#" + user + "#" + this.host);
			}
			else {
				writeAll("Disconnect#" + user);
			}
			
			if(isPlaying) {
				this.playingGame.disconnectPlayer(user.getPseudo());
			}
		}
		
		this.gameList.playersChanged(this.id, countUsers());
	}
}
