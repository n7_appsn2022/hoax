package compartment.game;

public interface GameListener {

	/**
	 * Process a status change from a game.
	 * 
	 * @param id Id of the game that has changed.
	 * @param status New status of the game.
	 */
	public void statusChanged(int id, boolean isPlaying);
	
	/**
	 * Process a change of the number of players in a game.
	 * 
	 * @param id Id of the game
	 * @param players Number of players updated.
	 */
	public void playersChanged(int id, int players);
}
