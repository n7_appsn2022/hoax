package compartment.game.lobby;

import compartment.MessageHandler;
import compartment.User;
import compartment.game.Game;
import network.SocketManager;

/**
 * This class manages a Game object before it start, it will wait that there are enough players 
 * and that the host launch the game. During the waiting time (when there are not enough players), it will 
 * signal to clients that new players joined. Then clients will be able to display the new connected player.
 * @author Anthony LAURENT
 *
 */
public class Lobby implements MessageHandler {
	
	private Game game;
	
	/**
	 * Constructor.
	 * Initialize the game.
	 * @param game
	 */
	public Lobby(Game game) {
		this.game = game;
	}

	@Override
	public void processMessage(String username, String message) {
		User user = this.game.getUser(username);
		if(user != null) {
			if(message.equals("WaitingLobbyStart")) {
				this.game.start();
			}
			else if(message.equals("WaitingLobbyLeave")) {
				this.game.getGameList().join(user);
			}
		}
	}

	/**
	 * This method notify the Client that a new user has joined.
	 * @param user
	 */
	public void join(User user) {
		// Send to user data about this game
		String reply = "EnterWaitingLobby#";
		for(User player : game.getUsers()) {
			
			// Send to others players that a new player has connected
			if(!player.equals(user)) {
				SocketManager.getInstance().write(player.toString(), "Connect#" + user);
			}

			reply += player + "#";
		}
		reply += game.getHost();
		SocketManager.getInstance().write(user.toString(), reply);
	}
}
