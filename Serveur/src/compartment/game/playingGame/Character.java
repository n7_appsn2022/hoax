package compartment.game.playingGame;

import java.lang.reflect.Constructor;
import java.util.List;

import compartment.game.playingGame.privilege.ButlerPrivilege;
import compartment.game.playingGame.privilege.ChefPrivilege;
import compartment.game.playingGame.privilege.DistantCousinPrivilege;
import compartment.game.playingGame.privilege.ExPrivilege;
import compartment.game.playingGame.privilege.GardenerPrivilege;
import compartment.game.playingGame.privilege.LoverPrivilege;
import compartment.game.playingGame.privilege.Privilege;
import compartment.game.playingGame.privilege.SonInLawPrivilege;
/**
 * The characters of the Hoax game.
 * There are seven of them, each with unique traits and possible actions.
 * Some of them can be immune to an other character.
 *
 */
public enum Character {
	Ex("theEx", ExPrivilege.class),
	SonInLaw("theSonInLaw", SonInLawPrivilege.class),
	Lover("theLover", LoverPrivilege.class),
	DistantCousin("theDistantCousin", DistantCousinPrivilege.class),
	Chef("theChief", ChefPrivilege.class),
	Gardener("theGardener", GardenerPrivilege.class),
	Butler("theButler", ButlerPrivilege.class);

	private String name;
	private Class<? extends Privilege> privilegeClass;
	
	/**
	 * Constructor of the class.
	 * @param name of the character
	 * @param privilegeClass the privilege of the character
	 */
	Character(String name, Class<? extends Privilege> privilegeClass) {
		this.name = name;
		this.privilegeClass = privilegeClass;
	}
	
	/**
	 * Retrieve the name of the character.
	 * @return character's name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Retrieve the character that the character is immune to.
	 * Null if the character has no immunity. 
	 * @return the character or null
	 */
	public static Character getImmuneTo(Character character) {
		Character result;
		
		switch(character) {
		case Ex:
			result = Butler;
			break;
			
		case SonInLaw:
			result = Lover;
			break;
			
		case Chef:
			result = DistantCousin;
			break;
			
		case Gardener:
			result = Chef;
			break;

		default:
			result = null;
		}
		return result;
	}
	
	/**
	 * This method build up the privilege for the player according to their character.
	 * @param player the current player
	 * @param game the ongoing game
	 * @param args correspond to the informations given by the Client to execute the privilege
	 * @return the privilege 
	 */
	public Privilege buildPrivilege(Player player, PlayingGame game, List<String> args) {
		Privilege result = null;
		try {
			Constructor<? extends Privilege> constructor = this.privilegeClass.getDeclaredConstructor(Player.class, PlayingGame.class, List.class);
			constructor.setAccessible(true);
			result = constructor.newInstance(player, game, args);
			
			if(!result.initialized()) {
				System.err.println("Impossible d'utiliser le personnage. Raison : " + result.getError());
				result = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Return the character based on the name given in parameter.
	 * @param name
	 * @return the character according to the name
	 */
	public static Character getFromName(String name) {
		Character result = null;
		Character[] characters = Character.values();
		int length = characters.length;
		int i = 0;
		
		while(i < length && result == null) {
			if(characters[i].getName().equals(name)) {
				result = characters[i];
			}
			i++;
		}
		return result;
	}
}
