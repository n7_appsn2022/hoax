package compartment.game.playingGame;

/**
 * This class correspond to the different game states.
 * During a game : 
 * - the round can be started
 * - a vote can occur
 * - we can wait for actions from the players 
 * - An immunity can be proclaimed, and the latter need to be accepted
 * - the game can end
 */
public enum GameState {
	Vote,
	WaitingAction,
	Immunity,
	AcceptationImmunity,
	Finish
}
