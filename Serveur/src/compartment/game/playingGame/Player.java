package compartment.game.playingGame;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import compartment.User;
import compartment.game.playingGame.resource.ResourceListener;
import compartment.game.playingGame.resource.ResourceOwner;

public class Player extends ResourceOwner {
	private User user;
	private Character character;
	private Set<Character> blockedCharacters;
	private Character lastClaimedCharacter;
	
	/**
	 * Number of the round where this player used its last immunity.
	 */
	private int roundLastImmunity;
	
	/**
	 * Constructor.
	 * 
	 * @param user User associated to this player for the game.
	 * @param character Character of the player.
	 */
	public Player(User user, Character character, ResourceListener listener) {
		super(user.getPseudo(), listener);
		
		this.user = user;
		this.character = character;
		this.blockedCharacters = new HashSet<Character>();
		this.lastClaimedCharacter = null;
		this.roundLastImmunity = -999;
	}
	
	/**
	 * Block a character that the player cannot pretend to be anymore.
	 * 
	 * @param character Character to block.
	 */
	public void blockCharacter(Character character) {
		this.blockedCharacters.add(character);
	}
	
	/**
	 * Retrieve all the characters that the player cannot be.
	 * 
	 * @return a collection of characters.
	 */
	public Set<Character> getBlockedCharacters() {
		return Collections.unmodifiableSet(this.blockedCharacters);
	}
	
	/**
	 * Claim to be a character.
	 * 
	 * @param character Character that the player pretend to be.
	 * @return true if the player can pretend to be this character, false otherwise.
	 */
	public boolean claimCharacter(Character character) {
		boolean ok = false;
		if(!this.blockedCharacters.contains(character)) {
			ok = true;
			this.lastClaimedCharacter = character;
		}
		return ok;
	}
	
	/**
	 * Retrieve the last claimed character.
	 * 
	 * @return the last claimed character.
	 */
	public Character getClaimedCharacter() {
		return this.lastClaimedCharacter;
	}
	
	public boolean isClaimedCharacterBlocked() {
		return this.blockedCharacters.contains(this.lastClaimedCharacter);
	}
	
	/**
	 * Get the character that has this player.
	 * 
	 * @return the character of the player.
	 */
	public Character getCharacter() {
		return this.character;
	}
	
	/**
	 * Get the character associated to this player.
	 * 
	 * @return the user of this player.
	 */
	public User getUser() {
		return this.user;
	}
	
	public void setRoundLastImmunity(int num) {
		this.roundLastImmunity = num;
	}
	
	public boolean canUseImmunity(RoundManager manager) {
		return manager.getRoundNumber() - this.roundLastImmunity >= manager.getNumberOfPlayer();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (character != other.character)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
}
