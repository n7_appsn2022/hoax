package compartment.game.playingGame;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import compartment.MessageHandler;
import compartment.User;
import compartment.game.Game;

import compartment.game.playingGame.privilege.Privilege;
import compartment.game.playingGame.resource.ResourceListener;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;
import compartment.game.playingGame.vote.Vote;
import compartment.game.playingGame.vote.VoteListener;
import network.SocketManager;

/**
 * This class manage the games that are started.
 * This class implements MessageHandler, VoteListener, BankListener.
 */
public class PlayingGame implements MessageHandler, VoteListener, ResourceListener {

	private Game game;
	private Map<String, Player> players;
	private RoundManager round;
	private ResourceOwner bank;
	private Vote vote;
	private GameState state;

	/**
	 * Constructor of the class.
	 * the method reset is called.
	 * The latter reset the value of each of the class's attributes to null and put the GameState to finish.
	 * @param game
	 */
	public PlayingGame(Game game) {
		this.game = game;
		this.players = null;
		this.round = null;
		this.bank = null;
		this.vote = null;
		this.state = GameState.Finish;
	}

	/**
	 * This method start the PlayingGame.
	 * Initialize the list of the playing players, 
	 * the round manager, the bank and it's resources.
	 * And inform the Client that the game has been created.
	 */
	public void start() {
		this.players = new HashMap<String, Player>();

		// Determine passage order
		ArrayList<User> copy = new ArrayList<User>(this.game.getUsers());
		List<Character> charactersList = new ArrayList<Character>(Arrays.asList(Character.values()));

		while (!copy.isEmpty()) {
			int indexOrder = (int) (Math.random() * copy.size());
			int indexCharacter = (int) (Math.random() * charactersList.size());
			User user = copy.remove(indexOrder);
			this.players.put(user.toString(), new Player(user, charactersList.remove(indexCharacter), this));
		}
		
		List<Player> ordonnedPlayers = new ArrayList<Player>(this.players.values());
		Collections.shuffle(ordonnedPlayers);
		
		this.round = new RoundManager(ordonnedPlayers);
		this.bank = new ResourceOwner("Bank", 13, 13, 13, this);
		

		// Construct the message to build the game
		String listPlayers = "";
		for(Player player : ordonnedPlayers) {
			listPlayers += ";" + player.getName();
		}
		
		if(!listPlayers.isEmpty()) {
			listPlayers = listPlayers.substring(1);
		}
		
		for(Player player : ordonnedPlayers) {
			SocketManager.getInstance().write(player.getName(), 
					"CreateGame#" + player.getCharacter().getName() + "#" + listPlayers);
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}

		for(Player player : ordonnedPlayers) {
			ResourceType type = ResourceType.getRandomResource();
			player.take(type, 1, this.bank);
		}
		
		this.state = GameState.WaitingAction;
		writeAll("NextRound#" + this.round.getCurrentPlayer().getName());
	}

	/**
	 * This method call the corresponding process based on the received message.
	 */
	@Override
	public void processMessage(String username, String message) {
		Player player = getPlayer(username);
		if (player != null) {
			switch(this.state) {
			case WaitingAction:
				if (message.equals("PassRound")) {
					processNextTurn(player);
				}
				else if (message.startsWith("Denounce")) {
					processDenounce(player, message.split("#"));
				}
				else if(message.startsWith("Investigate")) {
					processInvestigate(player, message.split("#"));
				}
				else if(message.startsWith("Announce")) {
					processAnnounce(player, message.split("#"));
				}
				break;
				
			case Vote:
				if(message.startsWith("Vote")) {
					processVote(player, message.split("#"));
				}
				break;
				
			case Immunity:
				if(message.startsWith("Immunity")) {
					processImmunity(player,  message.split("#"));
				}
				break;
				
			case AcceptationImmunity:
				if(message.startsWith("AcceptationImmunity")) {
					processAcceptationImmunity(player, message.split("#"));
				}
				break;
				
			case Finish:
				if(message.equals("Win")) {
					this.game.getGameList().join(this.game.getUser(username));
				}
				break;
			}
		}
	}
	
	/**
	 * This method start the process for the announce action.
	 * @param player the current player.
	 * @param args the informations given by the Client concerning the announcement,
	 * which are the proclaimed character and the information concerning the privilege. 
	 */
	public void processAnnounce(Player player, String[] args) {
		if(!this.round.hasAnnounced() && player == this.round.getCurrentPlayer()) {
			this.round.updateAnnounce();
			
			Character claimedCharacter = Character.getFromName(args[1]);
			if(claimedCharacter != null && player.claimCharacter(claimedCharacter)) {
				List<String> arguments = Arrays.asList(args).subList(2, args.length);
				Privilege privilege = claimedCharacter.buildPrivilege(player, this, arguments);
				
				if(privilege != null) {
					this.round.storePrivilege(privilege);
					
					String reply = "Announce#" + args[1];
					for(int i = 2; i < args.length; i++) {
						reply += "#" + args[i];
					}
					writeAll(reply);
					
					this.vote = new Vote(this);
					vote.start(this.round.getNumberOfPlayer() - 1); // moins le joueur courant
					this.state = GameState.Vote;
				}
			}
		}
	}
	
	/**
	 * This method start the voting process.
	 * @param player the current player.
	 * @param args the informations given by the Client concerning the Vote,
	 * which are if the player agree or revoke the proclamation.
	 */
	public void processVote(Player player, String[] args) {
		this.vote.vote(player, args[1].equals("Agree"));
	}
	
	public void processDenounce(Player player, String[] args) {
		if(!this.round.hasDenounced() && player == this.round.getCurrentPlayer()) {
			this.round.updateDenounced();
			
			Player target = players.get(args[1]);
			if (target.getCharacter().getName().equals(args[2])) {
				writeAll("Eliminated#" + target.getName());
				this.round.getCurrentPlayer().stealAllResources(target);
				
				this.round.eliminatePlayer(target);
				if (this.round.getNumberOfPlayer() == 1) {
					finishGame();
				}
			} 
			else {
				writeAll("Eliminated#" + this.round.getCurrentPlayer().getName());
				
				// Eliminate current player and change of current player
				this.bank.stealAllResources(this.round.getCurrentPlayer());
				this.round.eliminatePlayer(this.round.getCurrentPlayer());
				
				if(this.round.getNumberOfPlayer() == 1) {
					finishGame();
				}
				else {
					processNextTurn(this.round.getCurrentPlayer());
				}
			}
		}
	}
	
	/**
	 * This method start the investigate process.
	 * @param player the current player.
	 * @param args the informations given by the Client concerning the investigation,
	 * which is the pseudo of the targeted player.
	 */
	public void processInvestigate(Player player, String[] args) {
		if(args.length == 2 && player == this.round.getCurrentPlayer()) {
			Player playerInvestigate = getPlayer(args[1]);
			
			if(playerInvestigate != null && hasEnoughToInvestigate(player)) {
				this.bank.take(ResourceType.Cash, 1, player);
				this.bank.take(ResourceType.Evidence, 1, player);
				this.bank.take(ResourceType.Prestige, 1, player);
				
				List<Character> characters = new ArrayList<Character>(Arrays.asList(Character.values()));
				characters.remove(playerInvestigate.getCharacter());
				Collections.shuffle(characters);
				
				List<Character> investigation = new ArrayList<Character>(4);
				investigation.add(playerInvestigate.getCharacter());
				for(int i = 0; i < 3; i++) investigation.add(characters.get(i));
				Collections.shuffle(investigation);
				
				String reply = "Investigate";
				for(Character c : investigation) {
					reply += "#" + c.getName();
				}
				
				SocketManager.getInstance().write(player.getName(), reply);
			}
		}
	}
	
	/**
	 * This method start the immunity process.
	 * @param player the player that want to be immune.
	 * @param args the informations given by the Client concerning the immunity.
	 */
	public void processImmunity(Player player, String[] args) {
		if(args.length == 2 && player.canUseImmunity(this.round)) {
			if(args[1].equals("Yes") || args[1].equals("No")) {
				boolean vote = args[1].equals("Yes");
				
				// Dit aux autres qu'il s'immunise.
				if(vote) {
					writeAll("Immunity#" + player.getName());
				}
				
				if(this.round.addImmunityVote(player, vote)) {
					// Si on entre ici, ça veut dire que tous les votes ont été récupérés
					
					Set<Player> playerImmune = this.round.getImmunePlayers();
					if(playerImmune.size() > 0) {
						this.state = GameState.AcceptationImmunity;
						
						String reply = "AcceptationImmunity";
						for(Player p : playerImmune) {
							reply += "#" + p.getName();
							p.setRoundLastImmunity(this.round.getRoundNumber());
						}
						SocketManager.getInstance().write(this.round.getCurrentPlayer().getName(), reply);
					}
					else {
						this.round.getPrivilege().execute();
						this.state = GameState.WaitingAction;
					}
				}
			}
		}
	}
	/**
	 * This method start the immunity acceptation process. 
	 * Meaning that the current player can revoke the immunity's proclamation of an other player.
	 * @param player the current player.
	 * @param args the informations given by the Client.
	 */
	public void processAcceptationImmunity(Player player, String[] args) {
		if(player == this.round.getCurrentPlayer()) {
			int i = 1;
			boolean ok = true;
			Set<Player> players = new HashSet<Player>();
			
			// Récupération de tous les joueurs dont le joueur courant pense qu'ils mentent
			while(i < args.length && ok) {
				Player p = getPlayer(args[i]);
				ok = p != null && players.add(p);
				i++;
			}
			
			if(ok) {
				Character immuneToCurrentCaracter = Character.getImmuneTo(player.getClaimedCharacter());
				for(Player p : players) {
					if(p.getCharacter() == immuneToCurrentCaracter) {
						writeAll("DontBelieve#" + p.getName() + "#Wrongly");
						writeAll("Win#" + p.getName());
						this.state = GameState.Finish;
						ok = false;
						break;
					}
					else {
						writeAll("DontBelieve#" + p.getName() + "#Rightly");
						p.blockCharacter(immuneToCurrentCaracter);
						this.round.removeImmune(p);
					}
				}
				
				if(ok) {
					Privilege privilege = this.round.getPrivilege();
					for(Player p : this.round.getImmunePlayers()) {
						privilege.addImmune(p);
					}
					privilege.execute();
					this.state = GameState.WaitingAction;
				}
			}
		}
	}

	/**
	 * This method informed the players that the next round has started.
	 * @param currentPlayer
	 */
	public void processNextTurn(Player player) {
		if(player == this.round.getCurrentPlayer()) {
			Player newPlayer = this.round.next();
			this.state = GameState.WaitingAction;
			writeAll("NextRound#" + newPlayer.getName());
		}
	}
	
	/**
	 * This method execute the privilege of the current player.
	 * If the executed privilege is a resource stealing one the concerned players will be notified.
	 */
	public void executePrivilege() {
		Privilege privilege = this.round.getPrivilege();
		if(privilege != null) {
			Set<Player> stolenPlayers = privilege.stolenPlayers();
			
			if(stolenPlayers.size() > 0) {
				String reply = "Stealing";
				for(Player player : stolenPlayers) {
					if(player.canUseImmunity(this.round)) {
						reply += "#" + player.getName();
					}
				}
				writeAll(reply);
				this.state = GameState.Immunity;
			}
			else {
				privilege.execute();
				this.state = GameState.WaitingAction;
			}
		}
	}
	
	/**
	 * This method ends the game and makes all players returns to the lobby. 
	 */
	public void finishGame() {
		writeAll("Win#" + this.round.getCurrentPlayer().getName());
		this.state = GameState.Finish;
	}
	
	/**
	 * This method check that the current player has enough resources to launch an investigation.
	 * @param player
	 * @return true if the player has the necessaries resources, false otherwise.
	 */
	public boolean hasEnoughToInvestigate(Player player) {
		return player.getResource(ResourceType.Cash) > 0
			&& player.getResource(ResourceType.Evidence) > 0
			&& player.getResource(ResourceType.Prestige) > 0;	   
	}
	
	/**
	 * Write the message to all the players.
	 * @param message
	 */
	public void writeAll(String message) {
		game.writeAll(message);
	}
	
	/**
	 * Retrieve the player based on the username.
	 * @param username
	 * @return the player.
	 */
	public Player getPlayer(String username) {
		return this.players.get(username);
	}
	
	/**
	 * Retrieve the bank.
	 * @return the bank.
	 */
	public ResourceOwner getBank() {
		return this.bank;
	}
	
	/**
	 * Retrieve the round manager.
	 * @return the round manager.
	 */
	public RoundManager getRound() {
		return this.round;
	}
	
	/**
	 * Manage the disconnection of a player of the game.
	 * @param pseudo Pseudo of the player
	 */
	public void disconnectPlayer(String pseudo) {
		if(this.state != GameState.Finish) {
			Player playerRemoved = this.players.remove(pseudo);
			
			if(playerRemoved != null) {
				writeAll("Eliminated#" + playerRemoved.getName());
				this.bank.stealAllResources(playerRemoved);
				if(playerRemoved == this.round.getCurrentPlayer()) {
					this.round.eliminatePlayer(playerRemoved);
					
					if(this.players.size() > 1) {
						Player newPlayer = this.round.getCurrentPlayer();
						this.state = GameState.WaitingAction;
						writeAll("NextRound#" + newPlayer.getName());
					}
				}
				else {
					this.round.eliminatePlayer(playerRemoved);
				}
				
				if(this.players.size() < 2) {
					finishGame();
				}
			}
		}
	}

	@Override
	public void voteResult(boolean isMajorityDesagree) {
		this.state = GameState.WaitingAction;
		
		String reply = "VoteResult#" + (isMajorityDesagree ? "Revoke#" : "Agree");
		if(isMajorityDesagree) {
			Player currentPlayer = this.round.getCurrentPlayer();
			if(currentPlayer.getClaimedCharacter() == currentPlayer.getCharacter()) {
				reply += "False";
				finishGame();
			}
			else {
				reply += "True";
				currentPlayer.blockCharacter(currentPlayer.getClaimedCharacter());
			}
			writeAll(reply);
		}
		else {
			writeAll(reply);
			executePrivilege();
		}
	}
		
	@Override
	public void hasVoted(Player player, boolean agree) {
		writeAll("Vote#" + player.getName() + "#" + (agree ? "Agree" : "Revoke"));
	}
	
	@Override
	public void resourcesChanged(ResourceOwner owner, ResourceType type, int number) {
		writeAll("UpdateResources#" + type.getName() + "#" + number + (owner instanceof Player ? "#" + owner.getName() : ""));
	}
}
