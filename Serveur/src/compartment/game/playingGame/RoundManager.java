package compartment.game.playingGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import compartment.game.playingGame.privilege.Privilege;

/**
 * This class manage the round of the ongoing game.
 *
 */
public class RoundManager {

    private ArrayList<Player> players; // the playing players
    private int position;
    
    private boolean hasDenounced; // indicate if a denunciation has been made

    private boolean hasAnnounced;	// indicate if an announce has been made
    
    private Privilege currentPrivilege;
    
    private Map<Player, Boolean> immunePlayer;
    
    private int roundNumber;

    /**
     * Constructor of the class.
     * Initialize the list of playing player, the position at 0 and the immunePlayer.
     * Calls the method reset().
     * @param players
     */
    public RoundManager(List<Player> players) {
        this.players = new ArrayList<Player>(players);
        this.position = 0;
        this.immunePlayer = new HashMap<Player, Boolean>();
        this.roundNumber = 1;
        reset();
    }

    /**
     * Retrieve the next player and update the position.
     * @return the next player.
     */
    public Player next() {
        this.position = (this.position + 1) % this.players.size();
        this.roundNumber++;
	 	reset();
	 	return getCurrentPlayer();
    }

    /**
     * Retrieve the player based on the position.
     * @return a player.
     */
    public Player getCurrentPlayer() {
        return this.players.get(this.position);
    }
    
    /**
     * Retrieve the list of players.
     * The list can't be modified.
     * @return the player list.
     */
    public List<Player> getPlayers() {
    	return Collections.unmodifiableList(this.players);
    }

    /**
     * Removes a player from the round of players. If the player eliminated is the current player,
     * then the new current player is the one following the player eliminated.
     * 
     * @param player Player that is eliminated.
     */
    public void eliminatePlayer(Player player) {
    	int playerPosition = this.getPlayerPosition(player);
        if (playerPosition < this.position) {
        	this.position--;
        }
        else if(playerPosition == this.position) {
        	this.position = this.position % (this.players.size()-1);
        	reset();
        }
        this.players.remove(player);
    }

    /**
     * Retrieve the position of the player in the list.
     * @param player
     * @return the player's position in the list.
     */

    public int getPlayerPosition(Player player) {
        return this.players.indexOf(player);
    }

    /**
     * Retrieve the number of player.
     * @return the size of the list of players.
     */
    public int getNumberOfPlayer() {
        return players.size();
    }
    
    /**
     * Set the boolean hasDenounced as true. 
     * Meaning a denunciation has occured.
     */
    public void updateDenounced() {
    	this.hasDenounced = true;
    }
    
    /**
     * Set the boolean hasAnnounced as true.
     * Meaning an investigation has occured.
     */
    public void updateAnnounce() {
    	this.hasAnnounced = true;
    }

    /**
     * Tell if a denunciation has occured.
     * @return true if a denunciation is occuring, false otherwise.
     */
    public boolean hasDenounced() {
    	return this.hasDenounced;
    }
    
    /**
     * Tell if an announce has occured.
     * @return true if an announce has occured, false otherwise.
     */
    public boolean hasAnnounced() {
    	return this.hasAnnounced;
    }
    
    /**
     * This method reset the round attributes except for the list of player and the position.
     */
    private void reset() {
        this.hasDenounced = false;
        this.hasAnnounced = false;
        this.currentPrivilege = null;
        this.immunePlayer.clear();
    }
    
    /**
     * This method set the current privilege with the privilege pass in parameter.
     * @param privilege
     */
    public void storePrivilege(Privilege privilege) {
    	if(this.currentPrivilege == null) {
    		this.currentPrivilege = privilege;
    	}
    }
    
    /**
     * Retrieve the privilege.
     * @return the privilege.
     */
    public Privilege getPrivilege() {
    	return this.currentPrivilege;
    }
    
    /**
     * Considers if the player want to immune itself or not.
     * 
     * @param player Player that want to be immune.
     * @param vote Is true if the player want to be immune, false otherwise.
     * 
     * @return true if all players targeted by the privilege have replied, false otherwise.
     */
    public boolean addImmunityVote(Player player, boolean vote) {
    	Set<Player> stolenPlayer = this.currentPrivilege.stolenPlayers();
    	if(!this.immunePlayer.containsKey(player) && stolenPlayer.contains(player) && player.canUseImmunity(this)) {
    		
    		Character immuneToCurrentCharacter = Character.getImmuneTo(getCurrentPlayer().getClaimedCharacter());
    		if(immuneToCurrentCharacter != null && !player.getBlockedCharacters().contains(immuneToCurrentCharacter)) {
    			this.immunePlayer.put(player, vote);
    		}
    		else {
    			this.immunePlayer.put(player, false);
    		}
    	}
    	
    	int nbPlayerAskedToAnImminity = 0;
    	for(Player p : stolenPlayer) {
    		if(p.canUseImmunity(this)) {
    			nbPlayerAskedToAnImminity++;
    		}
    	}
    	
    	return this.immunePlayer.size() == nbPlayerAskedToAnImminity;
    }
    
    /**
     * Retrieve the players that are immune.
     * @return the immune players.
     */
    public Set<Player> getImmunePlayers() {
    	Set<Player> result = new HashSet<Player>();
    	for(Entry<Player, Boolean> entry : this.immunePlayer.entrySet()) {
    		if(entry.getValue()) {
    			result.add(entry.getKey());
    		}
    	}
    	return result;
    }
    
    /**
     * Remove the player from the immune list.
     * @param player
     */
    public void removeImmune(Player player) {
    	this.immunePlayer.remove(player);
    }
    
    /**
     * Retrieve the number of the round that is currently playing.
     * 
     * @return the number of the current round.
     */
    public int getRoundNumber() {
    	return this.roundNumber;
    }
}
