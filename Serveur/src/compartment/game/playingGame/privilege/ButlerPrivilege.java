package compartment.game.playingGame.privilege;

import java.util.List;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class ButlerPrivilege extends Privilege {
	
	private ResourceType type;
	private Player targetPlayer;
	private ResourceOwner bank;
	
	/**
	 * Constructor.
	 * @param player
	 * @param game
	 * @param args, information given by the Client concerning the privilege.
	 * In this case it's the type of the resource and the target player.
	 */
	private ButlerPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		if(args.size() == 2) {
			this.type = ResourceType.getFromName(args.get(0));
			this.targetPlayer = game.getPlayer(args.get(1));
			this.bank = game.getBank();
			
			if(this.type == null) {
				setError("Type de ressource invalide.");
			}
			else if(this.targetPlayer == null) {
				setError("Joueur cible invalide.");
			}
			else if(this.targetPlayer == this.player) {
				setError("Vous ne pouvez pas vous cibler vous-même.");
			}
			else if(this.bank.getResource(this.type) < 2) {
				setError("Pas assez de " + this.type.getName().toLowerCase() + " dans la banque.");
			}
			else {
				initialize();
			}
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}

	@Override
	public void execute() {
		if(initialized()) {
			this.player.take(this.type, 1, this.bank);
			this.targetPlayer.take(this.type, 1, this.bank);
			
			game.writeAll("Execute#" + this.type.getName() + "#" + this.targetPlayer.getName());
		}
	}
}
