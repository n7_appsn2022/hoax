package compartment.game.playingGame.privilege;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class ChefPrivilege extends Privilege {

	private boolean isStealing;
	
	private ResourceOwner target; // Bank or Player
	private ResourceType type1, type2;
	private int quantity;
	/**
	 * 
	 * @param player
	 * @param game
	 * @param args, information given by the Client concerning the privilege.
	 * In this case it's the either the resources and the target that can either be the bank or a player.
	 */
	private ChefPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		if(args.size() == 3 || args.size() == 4) {
			
			if(args.size() == 3) {
				this.target = game.getBank();
			}
			else {
				this.target = game.getPlayer(args.get(3));
				this.isStealing = true;
			}
			
			this.type1 = ResourceType.getFromName(args.get(0));
			this.type2 = ResourceType.getFromName(args.get(1));
			
			try {
				this.quantity = Integer.parseInt(args.get(2));
			}
			catch(NumberFormatException e) {
				this.quantity = 0;
			}
			
			if(this.quantity == 0) {
				setError("Quantité invalide (=0).");
			}
			else if(this.type1 == null || this.type2 == null) {
				setError("Type de ressource invalide.");
			}
			else if(this.target == null) {
				setError("Joueur cible invalide.");
			}
			else if(this.target == this.player) {
				setError("Vous ne pouvez pas vous cibler vous-même.");
			}
			else if(player.getResource(this.type1) < this.quantity) {
				setError(this.player.getName() + " n'a pas assez de " + this.type1.getName().toLowerCase() + "(<" + this.quantity + ").");
			}
			else if(this.target.getResource(this.type2) < this.quantity) {
				setError(this.target.getName() + " n'a pas assez de " + this.type2.getName().toLowerCase() + "(<" + this.quantity + ").");
			}
			else {
				initialize();
			}
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}

	@Override
	public void execute() {
		if(initialized() && !isImmunized(this.target)) {
			this.player.take(this.type2, this.quantity, this.target);
			this.target.take(this.type1, this.quantity, this.player);
			
			this.game.writeAll("Execute#" + this.type1.getName() + "#" + this.type2.getName() + "#" +
					this.quantity + (this.isStealing ? "#" + this.target.getName() : ""));
		}
	}
	
	
	@Override
	public Set<Player> stolenPlayers() {
		Set<Player> result = new HashSet<Player>();
		if(this.isStealing) {
			if(this.target instanceof Player) {
				result.add((Player)this.target);
			}
		}
		return result;
	}
}
