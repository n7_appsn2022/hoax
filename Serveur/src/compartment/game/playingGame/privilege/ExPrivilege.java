package compartment.game.playingGame.privilege;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.RoundManager;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class ExPrivilege extends Privilege {

	private boolean isStealing;

	private ResourceOwner bank; // Bank
	private ResourceType type1, type2;
	private Player player1, player2;

	/**
	 * Constructor.
	 * @param player
	 * @param game
	 * @param args, information given by the Client concerning the privilege.
	 * In this case it's the either the resource or an empty list depending on the privilege.
	 */
	private ExPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		//If the ex use the first privilege
		//meaning she is taking her resources from the bank
		if(args.size() == 1) {
			this.isStealing = false;
			this.type1 = ResourceType.getFromName(args.get(0));
			this.bank = game.getBank();
			
			if(type1 == null) {
				setError("Type de ressource invalide.");
			}
			else if(this.type1 != ResourceType.Cash && this.type1 != ResourceType.Evidence) {
				setError("Type de ressource invalide. Doit être cash ou prestige.");
			}
			else if(this.bank.getResource(this.type1) <= 0) {
				setError("Pas assez de " + this.type1.getName().toLowerCase() + " dans la banque.");
			}
			else {
				initialize();
			}
		}
		else if(args.isEmpty()) { //stealing from other players
			this.isStealing = true;
			RoundManager round = game.getRound();

			List<Player> playersList = round.getPlayers();
			int indexPlayer = playersList.indexOf(player);
			
			this.player1 = playersList.get(Math.floorMod(indexPlayer-1, playersList.size()));
			this.player2 = playersList.get((indexPlayer+1)%playersList.size());
			
			if(this.player1 == this.player2) {
				this.player2 = null;
			}
			
			this.type1 = null;
			this.type2 = null;
			
			if(this.player1 != null) {
				this.type1 = getRandomTypeOwned(this.player1);
			}
			
			if(this.player2 != null) {
				this.type2 = getRandomTypeOwned(this.player2);
			}
			
			initialize();
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}
	
	/**
	 * Allow to get a random ResourceType that the player has.
	 * 
	 * @param player Player
	 * @return a random ResourceType or null if the player has no resource.
	 */
	public ResourceType getRandomTypeOwned(Player player) {
		List<ResourceType> resourcesOwned = new ArrayList<ResourceType>();
		ResourceType resultType = null;
		
		for(ResourceType type : ResourceType.values()) {
			if(player.getResource(type) > 0) {
				resourcesOwned.add(type);
			}
		}
		
		if(!resourcesOwned.isEmpty()) {
			int random = (int) (Math.random() * resourcesOwned.size());
			resultType = resourcesOwned.get(random);
		}
		return resultType;
	}

	@Override
	public void execute() {
		if(initialized()) {
			String reply = "Execute";
			if(this.isStealing) {
				if(this.type1 != null && !isImmunized(this.player1)) {
					reply += "#" + this.player1.getName();
					this.player.take(this.type1, 1, player1);
				}
				
				if(this.type2 != null && !isImmunized(this.player2)) {
					reply += "#" + this.player2.getName();
					this.player.take(this.type2, 1, player2);
				}
			}
			else {
				this.player.take(this.type1, 1, this.bank);
				reply += "#" +this.type1.getName();
			}
			this.game.writeAll(reply);
		}
	}

	@Override
	public Set<Player> stolenPlayers() {
		Set<Player> result = new HashSet<Player>();
		if(this.isStealing) {
			if(this.type1 != null) {
				result.add(this.player1);
			}
			
			if(this.type2 != null) {
				result.add(this.player2);
			}
		}
		return Collections.unmodifiableSet(result);
	}
}
