package compartment.game.playingGame.privilege;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class GardenerPrivilege extends Privilege {
	
	private ResourceType type;
	private ResourceOwner targetPlayer;
	private int quantity;

	/**
	 * Constructor.
	 * @param player
	 * @param game
	 * @param args, information given by the Client concerning the privilege.
	 * In this case it's the type of the resource and the target player.
	 */
	private GardenerPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		if(args.size() == 2) {
			this.type = ResourceType.getFromName(args.get(0));
			this.targetPlayer = game.getPlayer(args.get(1));
			
			if(this.type == null) {
				setError("Type de ressource invalide.");
			}
			else if(this.targetPlayer == null) {
				setError("Joueur cible invalide.");
			}
			else if(this.targetPlayer == this.player) {
				setError("Vous ne pouvez pas vous cibler vous-même.");
			}
			else {
				this.quantity = this.targetPlayer.getResource(this.type);
				this.quantity = this.quantity >= 2 ? 2 : this.quantity;
				
				if(this.quantity < 1) {
					setError(this.targetPlayer.getName() + " n'a pas assez de " + this.type.getName().toLowerCase() + "(<2).");
				}
				else {
					initialize();
				}
			}
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}

	@Override
	public void execute() {
		if(initialized() && !isImmunized(this.targetPlayer)) {
			this.player.take(this.type, this.quantity, this.targetPlayer);
			this.game.writeAll("Execute#" + this.type.getName() + "#" + this.targetPlayer.getName());
		}
	}
	
	
	@Override
	public Set<Player> stolenPlayers() {
		Set<Player> result = new HashSet<Player>();
		if(this.targetPlayer instanceof Player) {
			result.add((Player)this.targetPlayer);
		}
		return result;
	}
}
