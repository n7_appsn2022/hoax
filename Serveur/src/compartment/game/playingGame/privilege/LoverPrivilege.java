package compartment.game.playingGame.privilege;

import java.util.List;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class LoverPrivilege extends Privilege {

	private ResourceType type;
	private ResourceOwner bank;
	
	/**
	 * Constructor.
	 * @param player
	 * @param game
	 * @param args, information given by the Client concerning the privilege.
	 * In this case it's the type of the resource.
	 */
	private LoverPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		if(args.size() == 1) {
			this.type = ResourceType.getFromName(args.get(0));
			this.bank = game.getBank();
			
			if(this.type == null) {
				setError("Type de ressource invalide.");
			}
			else if(this.type != ResourceType.Evidence && this.type != ResourceType.Prestige) {
				setError("Type de ressource invalide. Doit être evidence ou prestige.");
			}
			else if(this.bank.getResource(this.type) <= 0) {
				setError("Pas assez de " + this.type.getName().toLowerCase() + " dans la banque.");
			}
			else {
				initialize();
			}
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}

	@Override
	public void execute() {
		if(initialized()) {
			this.player.take(this.type, 1, this.bank);
			this.game.writeAll("Execute#" + this.type.getName());
		}
	}
}
