package compartment.game.playingGame.privilege;

import java.util.HashSet;
import java.util.Set;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.resource.ResourceOwner;

public abstract class Privilege {
	private Set<ResourceOwner> immunizedPlayers;
	private boolean initialized;// tell if the privilege has been initialized.
	private String error;
	
	protected Player player;
	protected PlayingGame game;
	
	/**
	 * Execute the privilege.
	 */
	public abstract void execute();
	/**
	 * Constructor.
	 * 
	 * @param player the current player.
	 * @param game the ongoing game.
	 */
	public Privilege(Player player, PlayingGame game) {
		this.immunizedPlayers = new HashSet<ResourceOwner>();
		this.initialized = false;
		this.player = player;
		this.game = game;
		this.error = new String();
	}
	
	/**
	 * Set the boolean initialized to true;
	 * Meaning that the privilege has been initialized.
	 */
	public void initialize() {
		this.initialized = true;
	}
	
	/**
	 * Tell if the privilege has been initialized.
	 * @return true if the privilege has been initialized, false otherwise
	 */
	public boolean initialized() {
		return this.initialized;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	public String getError() {
		return this.error;
	}
	
	/**
	 * Retrieve the list of the players victim of the steal.
	 * @return the players victim of the steal.
	 */
	public Set<Player> stolenPlayers() {
		return new HashSet<>();
	}
	
	/**
	 * Add the resource owner in the list of the immune player.
	 * @param owner
	 */
	public void addImmune(ResourceOwner owner) {
		this.immunizedPlayers.add(owner);
	}
	
	/**
	 * Tell if the owner of the resources is immune.
	 * @param owner
	 * @return true if the owner of the resources is immune, false otherwise.
	 */
	protected boolean isImmunized(ResourceOwner owner) {
		return this.immunizedPlayers.contains(owner);
	}
}
