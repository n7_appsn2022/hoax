package compartment.game.playingGame.privilege;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;
import compartment.game.playingGame.RoundManager;
import compartment.game.playingGame.resource.ResourceType;

public class SonInLawPrivilege extends Privilege {

	private ResourceType type;
	private Set<Player> players;
	
	private SonInLawPrivilege(Player player, PlayingGame game, List<String> args) {
		super(player, game);
		
		if(args.size() == 1) {
			this.type = ResourceType.getFromName(args.get(0));
			
			if(this.type != null) {
				this.players = new HashSet<Player>();
				RoundManager round = game.getRound();
				for(Player p : round.getPlayers()) {
					if(p != round.getCurrentPlayer() && p.getResource(this.type) > 0) {
						this.players.add(p);
					}
				}
				initialize();
			}
			else {
				setError("Type de ressource invalide.");
			}
		}
		else {
			setError("Nombre d'arguments invalide.");
		}
	}

	@Override
	public void execute() {
		if(initialized()) {
			String reply = "Execute";
			for(Player p : this.players) {
				if(!isImmunized(p)) {
					reply += "#" + p.getName();
					this.player.take(this.type, 1, p);
				}
			}
			this.game.writeAll(reply);
		}
	}
	
	@Override
	public Set<Player> stolenPlayers() {
		return Collections.unmodifiableSet(this.players);
	}
}
