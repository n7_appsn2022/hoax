package compartment.game.playingGame.resource;

public interface ResourceListener {
	/**
	 * Notify that the resources have been changed.
	 * @param owner
	 * @param type
	 * @param quantity
	 */
	public void resourcesChanged(ResourceOwner owner, ResourceType type, int quantity);
}
