package compartment.game.playingGame.resource;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import compartment.game.playingGame.Player;

 public class ResourceOwner {
	private Map<ResourceType, Integer> resources;
	private String name;
	private ResourceListener listener;
	
	/**
	 * Constructor that initiate the number of resources has zero.
	 * @param name of the owner of the resources.
	 * @param listener the BankListener.
	 */
	public ResourceOwner(String name, ResourceListener listener) {
		this.resources = new HashMap<ResourceType, Integer>();
		this.resources.put(ResourceType.Cash, 0);
		this.resources.put(ResourceType.Evidence, 0);
		this.resources.put(ResourceType.Prestige, 0);
		
		this.name = name;
		this.listener = listener;
	}
	
	/**
	 * Constructor that initiate the number of resources based on the given parameters.
	 * @param name of the owner of the resources.
	 * @param cash amount.
	 * @param evidence amount.
	 * @param prestige amount.
	 * @param listener the BankListener.
	 */
	public ResourceOwner(String name, int cash, int evidence, int prestige, ResourceListener listener) {
		this.resources = new HashMap<ResourceType, Integer>();
		this.resources.put(ResourceType.Cash, cash);
		this.resources.put(ResourceType.Evidence, evidence);
		this.resources.put(ResourceType.Prestige, prestige);
		
		this.name = name;
		this.listener = listener;
	}
	
	/**
	 * Retrieve the name of the resource owner.
	 * @return the name.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Retrieve the amount for the given resource type.
	 * @param type of the resource.
	 * @return the resource amount.
	 */
	public int getResource(ResourceType type) {
		return this.resources.get(type);
	}
	
	/**
	 * Retrieve the resources.
	 * @return the resources.
	 */
	public Map<ResourceType, Integer> getResources() {
		return Collections.unmodifiableMap(this.resources);
	}
	
	/**
	 * Take the resources from the target.
	 * @param type of the resource.
	 * @param quantity.
	 * @param target can either be the bank or a player.
	 */
	public void take(ResourceType type, int quantity, ResourceOwner target) {
		if(quantity > 0 && target != null) {
			int count = target.getResource(type);
			if(quantity <= count) {
				this.setResource(type, this.resources.get(type) + quantity);
				target.setResource(type, count-quantity);
			}
		}
	}
	
	/**
	 * Set the resources and notify the bank that the resources have changed.
	 * @param type of the resource.
	 * @param quantity.
	 */
	private void setResource(ResourceType type, int quantity) {
		this.resources.put(type, quantity);
		this.listener.resourcesChanged(this, type, quantity);
	}
	
	/**
	 * Steal all resources from a target.
	 * @param target Target which will loose all its resources.
	 */
	public void stealAllResources(Player target) {
		int nbCash = target.getResource(ResourceType.Cash);
		int nbEvidence = target.getResource(ResourceType.Evidence);
		int nbPrestige = target.getResource(ResourceType.Prestige);
		
		take(ResourceType.Cash, nbCash, target);
		take(ResourceType.Evidence, nbEvidence, target);
		take(ResourceType.Prestige, nbPrestige, target);
	}
}
