package compartment.game.playingGame.resource;

/**
 * This class represent the resource type.
 * There are different kind of resources,
 * which are cash, evidence and prestige.
 *
 */
public enum ResourceType {
	Cash("Cash"),
	Evidence("Evidence"),
	Prestige("Prestige");
	
	private String name;
	
	/**
	 * Constructor.
	 * Initialize the name of the resource.
	 * @param name
	 */
	ResourceType(String name) {
		this.name = name;
	}
	
	/**
	 * Retrieve the resource's name.
	 * @return the resource's name.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Return the type based on the name given in parameter.
	 * @param name
	 * @return the type according to the name
	 */
	public static ResourceType getFromName(String name) {
		ResourceType result = null;
		ResourceType[] types = ResourceType.values();
		int i = 0;
		
		while(i < types.length && result == null) {
			if(types[i].getName().equals(name)) {
				result = types[i];
			}
			i++;
		}
		return result;
	}
	
	/**
	 * Retrieve a random resource type.
	 * @return a resource type.
	 */
	public static ResourceType getRandomResource(){
		ResourceType typeResource;

		int random = (int) (Math.random() * 3);
		switch (random) {
		case 0:
			typeResource = ResourceType.Cash;
			break;
		case 1:
			typeResource = ResourceType.Evidence;
			break;
		case 2:
			typeResource = ResourceType.Prestige;
			break;
		default:
			typeResource = null;
		}
		
		return typeResource;
	}
}
