package compartment.game.playingGame.vote;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import compartment.game.playingGame.Player;
import compartment.game.playingGame.PlayingGame;

public class Vote {

	private int nbPlayers;
	private int agreeCount;
	private int revokeCount;
	private Set<Player> alreadyVoted;

	private Timer timer;

	private PlayingGame game;

	/**
	 * Constructor of the class
	 * initialize the yes and no counter
	 */
	public Vote(PlayingGame game) {
		this.agreeCount = 0;
		this.revokeCount = 0;
		this.nbPlayers = 0;
		this.game = game;
		this.alreadyVoted = new HashSet<Player>();
	}

	/**
	 * Initialize the number of player that need to vote,
	 * and set the timer. If the timer goes up to 60s then no matter if
	 * everyone has voted or not the close() method will be called
	 * 
	 * @param nbPlayers
	 */
	public void start(int nbPlayers) {
		if(!isVoting()) {
			this.nbPlayers = nbPlayers;
			this.timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					close();
				}
			}, 60L*1000);
		}
	}

	/**
	 * Increment the yes/no counter based on the given parameter
	 * If everyone has voted, the close() method will be called
	 * @param isYes
	 */
	public void vote(Player player, boolean agree) {
		List<Player> players = this.game.getRound().getPlayers();
		
		if (isVoting() && !this.alreadyVoted.contains(player) && players.contains(player)) {
			this.alreadyVoted.add(player);
			
			if (agree){
				this.agreeCount++;
			}
			else {
				this.revokeCount++;
			}
			
			this.game.hasVoted(player, agree);
			
			if (this.agreeCount+this.revokeCount == this.nbPlayers) {
				close();
			}
		}
	}

	/**
	 * Tell the Client if the others players accepted the proclamation for the other player
	 */
	public void close() {
		this.game.voteResult(revokeCount > agreeCount);
		timer.cancel();
		this.agreeCount = 0;
		this.revokeCount = 0;
		this.nbPlayers = 0;
	}

	/**Indicate if there is an ongoing vote
	 * @return true if there is an ongoing vote false otherwise
	 */
	public boolean isVoting() {
		return nbPlayers != 0;
	}

}
