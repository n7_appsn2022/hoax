package compartment.game.playingGame.vote;

import compartment.game.playingGame.Player;

public interface VoteListener {
	
	/**
	 * Give the result of the vote
	 * 
	 * @param isMajorityDesagree true if there is a majority of yes, false otherwise
	 */
	public void voteResult(boolean isMajorityDesagree);
	
	/**
	 * Indicate who has voted and what answer they gave
	 * @param isYes true if the person has voted yes false otherwise 
	 */
	public void hasVoted(Player player, boolean isYes);

}
