package compartment.gameList;

import java.util.HashMap;
import java.util.Map;

import compartment.Compartment;
import compartment.User;
import compartment.game.Game;
import compartment.game.GameListener;
import network.SocketManager;

/**
 * This class allow to manage a list of games. Thanks to it, we can run multiple games at a time.
 * The lobby send to a player the list of games. Then, the player can choose in which game he want to play.
 * @author Anthony LAURENT
 *
 */

public class GameList extends Compartment implements GameListener {
	private Map<Integer, Game> games;
	
	/**
	 * Initialize a lobby with a Map of integer and games
	 */
	public GameList() {
		this.games = new HashMap<Integer, Game>();
	}

	@Override
	public void processMessage(String username, String message) {
		User user = this.users.get(username);
		if(user != null) {
			if(message.startsWith("CreateGame")) {
				String[] msgSplit = message.split("#");
				if(msgSplit.length == 2) {

					// Create new game
					Game game = new Game(msgSplit[1], this);
					this.games.put(game.getId(), game);

					// Tell to other players that there is a new game
					writeAll("LobbyCreateGame#" + game.getId() + "#" + game.getTitle());
					
					try {
						Thread.sleep(100);
						game.addHost(user);
					} catch (InterruptedException e) {}
				}
			}
			else if(message.startsWith("JoinGame")) {
				String[] msgSplit = message.split("#");
				if(msgSplit.length == 2) {
					Game game = null;
					try {
						game = this.games.get(Integer.parseInt(msgSplit[1]));
					}
					catch(NumberFormatException e) {}
					
					if(game != null) {
						game.join(user);
					}
				}
			}
		}
	}
	
	@Override
	public void join(User user) {
		super.join(user);
		
		String reply = "LobbyJoined";
		for(Game game : this.games.values()) {
			reply += "#" + game.getId() + ";" + game.getTitle() + ";" + game.countUsers() + 
					";" + (game.isPlaying() ? "Playing" : "Waiting");
		}
		
		SocketManager.getInstance().write(user.toString(), reply);
	}

	@Override
	public void statusChanged(int id, boolean isPlaying) {
		String message = "LobbyStatusChanged#" + id + "#" + 
						 (isPlaying ? "Playing" : "Waiting");
		
		writeAll(message);
	}

	@Override
	public void playersChanged(int id, int players) {
		writeAll("LobbyPlayerChanged#" + id + "#" + players);
		
		if(players == 0) {
			this.games.remove(id);
		}
	}
}
