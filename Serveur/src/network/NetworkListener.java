package network;

import java.util.EventListener;

public interface NetworkListener extends EventListener {
	void connected(String username);
	void disconnected(String username);
	void messageReceived(String username, String message);
}
