package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.net.BindException;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.event.EventListenerList;

public class SocketManager implements Runnable {
	private Map<String, ServiceClient> clients;
	private ServerSocket server;
	private EventListenerList listeners;
	
	public final int port = 2022;
	
	private static SocketManager manager = new SocketManager();

	private final Object monitor = new Object();
	
	/**
	 * Constructor.
	 */
	private SocketManager() {
		this.listeners = new EventListenerList();
		this.clients = new HashMap<String, ServiceClient>();
		
		try {
			this.server = new ServerSocket(port);
		} 
		catch(BindException e) {
			System.err.println("Impossible de lancer le serveur. Vérifier qu'un autre serveur n'est pas déjà lancé.");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static SocketManager getInstance() {
		return manager;
	}

	@Override
	public void run() {
		if(server != null) {
			System.out.println("Le serveur est lancé !");
			while(true) {
				try {
					Socket socket = server.accept();
					Thread client = new ServiceClient(socket);
					client.start();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Write a message using the socket.
	 * 
	 * @param socket Socket
	 * @param message Message to send
	 */
	private void write(Socket socket, String message) {
		DataOutputStream out;
		
		try {
			out = new DataOutputStream(socket.getOutputStream());
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {}
	}
	
	/**
	 * Write the message to the specified pseudo.
	 * 
	 * @param pseudo Pseudo that will receive the message.
	 * @param message Message to send.
	 */
	public void write(String pseudo, String message) {
		if(!this.clients.containsKey(pseudo)) {
			throw new UnknownUserException();
		}

		System.out.println("Serveur écrit " + message + " -> " + pseudo);
		Socket socket = this.clients.get(pseudo).getSocket();
		write(socket, "Msg;" + message);
	}
	
	/**
	 * Write the message to all users connected to the server.
	 * 
	 * @param message Message to send.
	 */
	public void writeAll(String message) {
		for(ServiceClient client : this.clients.values()) {
			write(client.getSocket(), "Msg;" + message);
		}
	}
	
	/**
	 * Add a new NetworkListener to the listeners list.
	 * 
	 * @param listener New listener to add.
	 */
	public void addNetworkListener(NetworkListener listener) {
		this.listeners.add(NetworkListener.class, listener);
	}
	
	/**
	 * Remove a listener from the listeners list.
	 * 
	 * @param listener Listener to remove.
	 */
	public void removeNetworkListener(NetworkListener listener) {
		this.listeners.remove(NetworkListener.class, listener);
	}
	
	/**
	 * Retrieves an array of all listeners.
	 * 
	 * @return Array of listeners.
	 */
	private NetworkListener[] getNetworkListeners() {
        return listeners.getListeners(NetworkListener.class);
    }
	
	/**
	 * Class that manage all the messages received by a user.
	 */
	class ServiceClient extends Thread {
		
		private Socket socket;
		private DataInputStream in;
		private boolean isActive;
		private String pseudo;
		
		/**
		 * Constructor.
		 */
		public ServiceClient(Socket socket) throws IOException {
			this.socket = socket;
			this.in = new DataInputStream(this.socket.getInputStream());
			this.isActive = false;
		}
		
		@Override
		public void run() {
			this.isActive = true;
			
			try {
				while(this.isActive) {
					String command = in.readUTF();
					
					if(command.matches("^Co;.+$")) {
						acceptConnection(command.substring(3));
					} 
					else if(command.matches("^Msg;.+$")) {
						synchronized (monitor) {
							for(NetworkListener listener : getNetworkListeners()) {
								listener.messageReceived(this.pseudo, command.substring(4));
							}
						}
					}
					else if(command.equals("Disc")) {
						playerDisconnected();
					}
				}
			} catch (IOException e) {
				playerDisconnected();
				this.isActive = false;
			}
		}
		
		/**
		 * Accept a connection if there is not an other player with the same pseudo already connected.
		 * 
		 * @param pseudo Pseudo that someone want to use.
		 */
		public void acceptConnection(String pseudo) throws IOException {
			synchronized (monitor) {
				if(clients.containsKey(pseudo)) {
					write(socket, "RejCo");
					
					this.isActive = false;
					this.socket.close();
				}
				else {
					write(socket, "AckCo");
					clients.put(pseudo, this);
					this.pseudo = pseudo;
					
					for(NetworkListener listener : getNetworkListeners()) {
						listener.connected(this.pseudo);
					}
				}
			}
		}
		
		/**
		 * Disconnect this client.
		 */
		public void playerDisconnected() {
			this.isActive = false;
			clients.remove(this.pseudo);
			
			for(NetworkListener listener : getNetworkListeners()) {
				listener.disconnected(this.pseudo);
			}
			
			try {
				this.socket.close();
			} catch (IOException e) {}
		}
		
		/**
		 * Get the socket used by this client.
		 * 
		 * @return the socket.
		 */
		public Socket getSocket() {
			return this.socket;
		}
		
		/**
		 * Get the pseudo used by this client.
		 * 
		 * @return the pseudo.
		 */
		public String getPseudo() {
			return this.pseudo;
		}
	}
}
