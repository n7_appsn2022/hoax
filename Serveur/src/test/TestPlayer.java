package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import compartment.User;
import compartment.game.playingGame.Character;
import compartment.game.playingGame.Player;
import compartment.game.playingGame.resource.ResourceListener;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class TestPlayer {

  private Player player1;
  private User user1;
  private Character character;
  
  @Before
  public void setUp() {
    user1 = new User("Joey");
    character = Character.Ex;
    
    player1 = new Player(user1, character, new ResourceListener() {
		@Override
		public void resourcesChanged(ResourceOwner owner, ResourceType type, int quantity) {
			// DO NOTHING
		}
	});
  }
  
  @Test
  public void testPseudo() {
    assertEquals("The user.toString should be equal to the pseudo returned", 
        user1.toString(), player1.getName());
  }
  
  @Test
  public void testCharacter() {
    assertEquals("Character should be equal to the one in the constructor",
        character, player1.getCharacter());
  }
}
