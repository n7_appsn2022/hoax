package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import compartment.User;
import compartment.game.playingGame.Character;
import compartment.game.playingGame.Player;
import compartment.game.playingGame.RoundManager;
import compartment.game.playingGame.resource.ResourceListener;
import compartment.game.playingGame.resource.ResourceOwner;
import compartment.game.playingGame.resource.ResourceType;

public class TestRoundManager {

  private RoundManager rm;
  private ArrayList<Player> players;
  private Player p1;
  private Player p2;
  
  @Before
  public void setUp() {
    User u1 = new User("Joe");
    User u2 = new User("Mama");
    
    ResourceListener listener = new ResourceListener() {
		@Override
		public void resourcesChanged(ResourceOwner owner, ResourceType type, int quantity) {
			// DO NOTHING
		}
	};
    
    p1 = new Player(u1, Character.Butler, listener);
    p2 = new Player(u2, Character.DistantCousin, listener);
    
    players = new ArrayList<Player>();
    players.add(p1);
    players.add(p2);
    
    rm = new RoundManager(players);
  }
  
  @Test
  public void testCurrentPlayer() {
    assertEquals("Should be p1", p1, rm.getCurrentPlayer());
  }
  @Test
  public void testNext() {
    assertEquals("Should be p2", p2, rm.next());
    assertEquals("Should be p2", p2, rm.getCurrentPlayer());
    assertEquals("Sould be p1", p1, rm.next());
    assertEquals("Should be p1", p1, rm.getCurrentPlayer());
  }
  
  @Test
  public void testPlayerPosition() {
    assertEquals("Indexes should be equals", players.indexOf(p1), rm.getPlayerPosition(p1));
    assertEquals("Indexes should be equals", players.indexOf(p2), rm.getPlayerPosition(p2));
    rm.next();
    assertEquals("Indexes should be equals", players.indexOf(p1), rm.getPlayerPosition(p1));
    assertEquals("Indexes should be equals", players.indexOf(p2), rm.getPlayerPosition(p2));
  }
  
  @Test
  public void testNumberPlayer() {
    assertEquals(players.size(), rm.getNumberOfPlayer());
  }
  
  @Test
  public void testEliminatePlayer() {
    assertEquals(players.size(), rm.getNumberOfPlayer());
    rm.eliminatePlayer(p1);
    assertEquals(players.size()-1, rm.getNumberOfPlayer());
    assertEquals("Should be p2", p2, rm.next());
    assertEquals("Should be p2", p2, rm.getCurrentPlayer());
  }
}
