package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import compartment.Compartment;
import compartment.User;

public class TestUser {
  private User user1;
  private Compartment compartment;
  private User user2;
  
  @Before
  public void setUp() {
    user1 = new User("pseudo1");
    user2 = new User("pseudo2");
    
    compartment = new Compartment() {
    	@Override
    	public void processMessage(String username, String message) {
    		//DO NOTHING
    	}
    };
  }
  
  @Test
  public void testPseudo() {
    assertEquals("Bad pseudo", "pseudo1", user1.getPseudo());
  }
  
  @Test
  public void testString() {
    assertEquals("Bad string", "pseudo1", user1.toString());
  }
  
  @Test
  public void testCompartment() {
    assertEquals("Compartment not null",null, user1.getCompartment());
    compartment.join(user1);
    assertEquals("Compartment not set", compartment, user1.getCompartment());
  }
  
  @Test
  public void testEquals1() {
	  assertEquals("Both player are in the same compartment", user1.getCompartment(), user2.getCompartment());
	  compartment.join(user1);
	  assertNotEquals("Both player aren't in a compartment", user1.getCompartment(), user2.getCompartment());
	  compartment.join(user2);
	  assertEquals("Both player are in a compartment", user1.getCompartment(), user2.getCompartment());
  }
}
