16/06/2020
Esteban Baichoo

## Sprint 3

#### Ajout de nouvelles fonctionnalités

Lors de ce sprint, la possibilité de retourner au lobby des parties après la fin de la partie actuelle a été ajoutée

Il est également possible de pouvoir gérer une absence au niveau des votes et de fermer automatiquement la fenêtre de vote après un certain délai.

De plus, la fenêtre de sélection de choix a été remodelée en s'appuyant sur la JDialog de Java pour avoir plus d'aisance.

Un soucis d'imbrication de listener s'appelant l'un l'autre à l'appel de l'un a été résolu, lors de la mise à jour des choix que peut faire un joueur lorsqu'il sélectionne un privilège à exécuter.



#### Amélioration ergonomique

Une des premières étapes lors de ce Sprint a été de mettre en place divers vérifications sur le choix du pseudo par l'utilisateur.

Seuls des caractères alphanumériques (Regex) sont utilisables, et le pseudo choisi ne doit pas déjà être présent sur le Serveur.

Une autre grosse partie a été des améliorations ergonomiques quant à l'utilisation de l'application, pour prévenir que l'utilisateur n'est pas l'hôte de la partie et ne peut donc pas la lancer, ou que si il est hôte, il ne peut la lancer car il n'y a pas assez de personne, ou bien qu'un utilisateur ne peut rejoindre une partie car le nombre maximum de joueur dans le lobby de la partie a déjà été atteint, etc ...



#### Refactoring

Une autre petite partie a été du refactoring de code, en plaçant des méthode en statique lorsqu'elle était appelée depuis plusieurs endroits avec les mêmes paramètres, en mettant des lambas et en promouvant l'utilisation des stream pour chercher/supprimer/filtrer des valeurs.



#### Tests de fonctionnalité

Des tests de l'application globale ont été réalisé sur la fin du sprint afin de débusquer les différents bugs et de les corriger.