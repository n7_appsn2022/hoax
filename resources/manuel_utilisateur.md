# Bienvenue sur Hoax
<div style="text-align:center"><img src="https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/hoaxMainImg.jpeg" /></div>

## Résumé
Le jeu Hoax est un jeu d’enquête où chaque joueur se voit attribuer un rôle secret et doit faire en sorte d’éliminer les autres joueurs en trouvant leur rôle via un système d’enquête.

Le but du jeu est d'éliminer tous les autres joueurs en devinant leurs rôles secrets sans révéler le vôtre.

Une démonstration vidéo est disponible [ici sur youtube](https://youtu.be/5QTwoL6hlks).

**A son tour, chaque joueur** :

\- Doit prétendre être un personnage afin d’obtenir des ressources nécessaire pour pouvoir effectuer une enquête.

\- Peut dénoncer un autre joueur en lui montrant le rôle qu’il pense avoir découvert, au risque d’être éliminé s’il a tort.

\- Peut mener une enquête sur un joueur et obtenir 4 cartes aléatoires contenant le personnage du joueur visé. Par exemple, une enquête sur un joueur étant l'Ex donnera un set de 4 cartes à l'enquêteur. Ce set de 4 cartes contiendra le rôle de l'Ex avec 3 autres rôles aléatoires.




Lorsqu’un joueur prétend être un personnage, les autres joueurs peuvent effectuer un vote et refuser que le joueur actif utilise ce rôle. Si la majorité a raison, le joueur actif n’a plus le droit de prétendre être ce personnage, sinon le joueur actif gagne la partie. 

## Comment jouer ?

### But du jeu
Le but du jeu est d'éliminer tous les autres joueurs en devinant leurs rôles secrets sans révéler le vôtre.

### Se connecter à Hoax

![Ecran d'accueil](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/homepage.png) 

1. Choisir son pseudo

2. Cliquer sur le bouton jouer pour créer ou rejoindre une partie.

3. On peut aussi consulter les règles du jeu.

### Lancer la partie

Le premier joueur à se connecter sera l'hôte. Une fois le joueur connecté, une liste de parties est affichée avec le nombre de joueurs de la partie et l'état actuel de celle-ci.
Lors du lancement d'une première partie, l'hôte devra créer une nouvelle partie en appuyant sur le bouton "Créer une nouvelle partie". D'autres joueurs pourront par la suite rejoindre cette partie.

![GameList](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/GameList.png)

L'hôte attend que les joueurs rejoignent la partie. Il peut aussi abandonner la création de partie en cliquant sur "Quitter"

![Lobby](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/waiting_lobby.png) 

D'autres utilisateurs rejoindront par la suite la partie en sélectionnant une partie "en attente" et en cliquant sur "Rejoindre la partie" ou en double cliquant dessus.

![GetIntoGame](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/ChooseGame.png)

Une salle d'attente est ensuite affichée comme ci-dessous, avec les différents joueurs qui ont rejoint la partie. Une fois le nombre minimum de joueurs (3) atteint, l'hôte peut lancer la partie. 

![WaitingLobby-bis](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/WaitingLobby-bis.png)

Une fois la partie lancée, le jeu débute. 

### Plateau d'un joueur

![Plateau de joueur](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/PlayerBoardscreen.png) 

Chaque joueur de la partie dispose d'un plateau qui lui est propre.
Sur le plateau d'un joueur figure plusieurs éléments :

1. Une image qui illustre le rôle du joueur pendant toute la durée de la partie.
2. Le pseudo choisi par le joueur pour la partie.
3. La banque de ressources. 
   Il y a 13 ressources de chaque type, dans l'ordre : preuve, renommée et argent. Les joueurs viendront puiser dans la banque de ressources pour obtenir des ressources.
   Au début de la partie, chaque joueur reçoit une ressource, d'un type aléatoire, qui provient de la banque.
4. Une console qui affiche l'historique des différentes actions de la partie. 
   Dans cette image, on peut voir affiché le joueur pour qui c'est le tour.
5. Un tableau de ressources qui affiche les ressources de chaque type, pour chaque joueur de la partie.

Comme le montre l'image ci-dessus le bouton "Montrer la grille" permet d'afficher une grille, qui liste les privilèges et immunités de chacun des personnages du jeu. 
Les privilèges représentent les actions qui peuvent être effectuées par un joueur s'il annonce ce rôle, et les immunités représentent le personnage pour lequel un personnage est immunisé.
Par exemple le "Cousin distant" peut prendre une ressource de type argent ou prestige à la banque et est immunisé contre le "Chef". 
Certaines actions du jeu (qui seront détaillées plus bas) peuvent conduire à ce que des personnages soient bloqués pour un joueur. 
Quand un personnage est bloqué, alors il est grisé au niveau de la grille.

![Grille](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Grille.png)

De plus, vous trouverez sur le plateau d'un joueur différentes actions possibles. Ces actions sont représentées par des boutons : "Annoncer", "Dénoncer","Enquêter" et "Fin du tour".
Lorsque l'ensemble de ces boutons sont grisés, cela signifie que ce n'est pas à votre tour de jouer.
Ces actions sont expliquées ci-dessous. 



### Actions Possibles lors de votre tour

À son tour, un joueur peut choisir d'effectuer toutes ces actions dans l'ordre de son choix, mais une seule fois chacune. À l'exception d'enquêter qui peut être fait autant de fois que vous avez de triplet (1 preuve +1renomé +1 argent) de ressources.

Lorsque vous avez fini d'effectuer une action, cliquez sur le bouton "Fin du tour", afin de passer la main aux autres joueurs de la partie.


#### Dénoncer un autre joueur

Lorsque vous choisissez de dénoncer, deux fenêtres s'ouvrent successivement. Une liste des joueurs et des rôles s'affiche.  
Ciblez le joueur à dénoncer et désignez un rôle.   

![Denounce](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Denouncescreen.png)

Si le joueur dénoncé a bien ce rôle, vous l'éliminez et récupérez ses ressources.  

En revanche, si vous vous êtes trompé, c'est vous qui êtes éliminé. 

Dans tous les cas, les joueurs éliminés ne révèlent pas leurs rôles !

#### Annoncer un rôle
Lorsque vous choisissez d'annoncer un rôle, une fenêtre s'ouvre. Vous pouvez choisir le rôle à annoncer et le privilège de ce personnage à exécuter. 

![Annonce](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Announcescreen.png)

Par exemple sur la capture ci-dessus, le joueur décide d'annoncer qu'il est le beau fils. Le privilège du beau fils est que chaque joueur doit lui donner une ressource que lui aura choisi.
Ce joueur choisit l'argent. 

Lorsque vous terminez votre annonce, les votes sont automatiquement lancés pour une durée de 60 secondes. Les autres joueurs de la partie devront voter pour ou contre l'annonce que vous avez faite.
La capture ci-dessous illustre ces propos.

![Vote](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Vote.png)

S'il y a une majorité de "pour", alors vous pouvez exécuter le privilège du personnage annoncé.
Sinon, s'il y a une majorité de contre, alors deux options :
    1. Le personnage annoncé est réellement votre rôle, alors vous gagnez la partie.
        2. Le personnage annoncé n'est pas votre rôle, alors ce personnage est bloqué pour vous. Vous ne pourrez plus vous en servir lors de prochains tours. 
       Les personnages bloqués d'un joueur sont grisés au niveau de la grille. 

##### Immunité

Le privilège de certains personnages implique un vol de ressources à d'autres joueurs : L'ex, le beau fils, le jardinier et le chef. Si vous annoncez un personnage pour faire un vol de ressources,
le joueur auquel vous tentez de voler des ressources peut proclamer une immunité.

![Immunity](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Immunity.png)



Si durant l'exécution de votre privilège, des joueurs que vous essayez de voler tentent de s'immuniser, alors la fenêtre suivante s'affiche.

![DenyImmunity](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/DenyImmunity.png)

Vous pouvez soit :

- Accepter l'immunité (en cliquant directement sur Valider), dans ce cas, la personne en question sera immunisé contre contre privilège et vous n'aurez donc aucun effet sur elle.
- Refuser l'immunité (en cochant la case refuser), dans ce cas si le joueur n'avait pas un rôle qui l'immunisait alors il ne pourra plus utiliser ce personnage lors de prochains tours.
  Si au contraire il était vraiment un personnage immunisé alors ce joueur gagne la partie.



#### Enquêter sur un autre joueur

Lorsque vous disposez de suffisamment de ressources de chaque type, c'est-à-dire 1 argent, 1 renommée et 1 preuve, alors vous pouvez mener une enquête sur un autre joueur : le bouton enquêter n'est plus grisé.
Quand vous choisissez d'enquêter, une fenêtre s'ouvre pour désigner le joueur à enquêter :

![Investigate](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/Investigatescreen.png)



Une fois la cible de l'enquête choisie, le résultat de l'enquête vous est montré. Le résultat de l'enquête comporte 4 cartes de personnages du jeu. Seulement une seule carte parmi les quatre représente le rôle du personnage sur lequel vous avez enquêté. 

![InvestigateResult](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/InvestigateResult.png)

    Attention 
    Ce résultat n'est stocké nulle part, c'est à vous de retenir le résultat de cette enquête.
    Vous pourrez ensuite contredire un joueur lors d'une annonce ou le dénoncer si vous êtes sûr d'avoir deviné son rôle grâce à l'enquête.



## Gagner le jeu

Pour gagner le jeu, il faut réussir à éliminer tous les autres joueurs de la partie. Cependant il existe deux autres scénarios qui vous permettront de gagner le jeu :

- Vous annoncez un personnage, les autres joueurs contredisent votre annonce en votant contre, mais vous étiez réellement ce personnage alors vous gagnez la partie.
- Si un joueur tente de vous volez une ressource, vous proclamez une immunité valide (c'est-à-dire que vous êtes réellement un personnage immunisé par ce joueur) et que ce joueur refuse votre immunité, alors vous gagnez la partie.

Lorsque la partie est gagnée par un joueur, une fenêtre s'ouvre sur le plateau de chaque joueur pour afficher le nom du gagnant.


## Être éliminé du jeu

Il existe deux manières d'être éliminé de la partie :

- Vous dénoncez un joueur d'être un personnage mais il n'est pas ce personnage, vous êtes éliminé.
- Vous avez été dénoncé par un joueur et cette dénonciation est vraie, vous êtes éliminé.



## Lancer le Jeu


### Le serveur

Le serveur est actif en permanence, donc vous n'avez pas besoin de le lancer explicitement. Le serveur s'occupe de la gestion des différentes parties et de l'host de chaque partie.


### Les clients

Pour "Lancer" le client, double-cliquez sur le jar correspondant.
Si votre système ouvre le jar au lieu de l'exécuter, utiliser `java -jar Client.jar` dans un terminal. 
Lancez autant de client que vous souhaitez avoir de joueur (entre 3 et 6) ou invitez vos amis ! 

Il est désormais possible de jouer en réseau, c'est-à-dire que vous pouvez lancer un client sur un poste et jouer avec un ou des utilisateur(s) qui se trouvent sur d'autres postes, en vous assurant bien 
sûr de tous rejoindre la même partie.

Chaque joueur aura une fenêtre qui lui est propre.

Il vous est également possible de jouer en local ou sur un autre serveur distant en modifiant l'adresse IP du serveur. Pour cela, rendez-vous dans le menu "Option" en haut à gauche de la page d'accueil, puis modifiez l'IP.