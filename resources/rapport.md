# Rapport

Membres du groupe :

* Callune Gitenet
* Maëva Guerrier
* Anthony Laurent
* Esteban Baichoo
* Awa Tidjani
* Michaël Mora

## Sommaire :

[TOC]



## Qu'est ce que Hoax ? 

Le jeu Hoax est un jeu d’enquête où chaque joueur se voit attribuer un rôle secret et doit faire en sorte d’éliminer les autres joueurs en trouvant leur rôle via un système d’enquête.
À  son tour chaque joueur
- Doit prétendre être un personnage afin d’obtenir des ressources nécessaires pour pouvoir effectuer une enquête.
- Peut dénoncer un joueur en montrant face cachée le rôle qu’il pense avoir découvert, au risque d’être éliminé s’il a tort
- Peut mener une enquête sur un joueur et obtenir 4 cartes aléatoires contenant le personnage du joueur visé
Lorsqu’un joueur prétend être un personnage, les autres joueurs peuvent effectuer un vote et refuser que le joueur actif utilise ce rôle. Si la majorité a raison, le joueur actif n’a plus le droit de prétendre être ce personnage, sinon le joueur actif gagne la partie.

### Mode multijoueur 

L'application fonctionne avec un serveur permettant de jouer en mode multijoueur. 
Il existe donc une interaction client-serveur permettant le bon fonctionnement du jeu. Cette interaction sera détaillée par la suite.

### Vidéo Démo
Une démonstration vidéo est disponible [ici sur youtube](https://youtu.be/5QTwoL6hlks).

## Gestion de projet 

### Méthode de travail

Ce projet a été réalisé en s'inspirant de la méthode SCRUM. La méthode Scrum est une méthode de travail conforme aux principes des méthodes agiles. 

Ces principes visent à privilégier : 

* Les individus et leurs interactions plutôt que les processus et les outils,
* Des logiciels opérationnels plutôt qu’une documentation exhaustive,
* La collaboration avec les clients plutôt que la négociation contractuelle,
* L’adaptation au changement plutôt que le suivi d’un plan.

La méthode Scrum privilégie la livraison rapide d'un prototype et donc de fonctionnalité, ce qui permet aux membres du projet ainsi qu'au client de les évaluer et de fournir un retour.  
Cette dernière est décomposée en itération. Une itération, aussi connue sous le nom de sprint, est de l'ordre de quelques semaines, pendant laquelle on développe les fonctionnalités prévues. 
Chaque fin d'itération donne lieu à une livraison contenant idéalement un prototype du projet. Scrum donne lieu à plusieurs réunions, dont une journalière qui se nomme daily, où chacun discute de son avancé et de ses difficultés. 

Pour ce projet, la méthodologie SCRUM à proprement parler n'a pas été utilisée complètement. En effet, il n'y avait pas de scrum master désigné, chaque membre de l'équipe a joué ce rôle. Pendant le projet, chacun des membres se coordonnait avec les autres et choisissait ses tâches. Néanmoins, la similarité avec SCRUM était la présence d'un product owner. C'était Callune, possédant le jeu ```Hoax``` chez lui, il a donné les règles du jeu et les images associées. De plus, ayant déjà joué de nombreuses fois, il a pu aiguiller l'équipe lors de la création des Users Stories par rapport à des cas assez complexes du jeu. Par ailleurs, nous avons implémenté le système du Daily scrum lors du sprint 2. 

Pour réaliser ce projet, l'équipe a travaillé en itération sur trois semaines, soit une itération par semaine, avec la prise en compte de l'itération 0 où aucun livrable n'a été fait mais uniquement de la conception.
Au début de chaque sprint avec l’ensemble de l’équipe projet et du « product owner », la première action consistait à se mettre d’accord sur le planning du sprint, c'est-à-dire les fonctionnalités à développer(Ces dernières sont développées dans la section suivante). Puis une fois ces décisions prises nous le sprint débutait.

Pendant ce projet, l'outil Discord a été utilisé pour communiquer. Ainsi, lors des heures communes de travail (les heures ADE), l'équipe a pu communiquer via des salons vocaux et textuels.  
De plus, à partir du sprint 2, les membres de l'équipe ont commencé à être dispersé dans les tâches. Donc, pour éviter de perdre le contact avec les autres et pour éviter les blocages, un salon ```#daily-meeting``` a été créé. Le but de ce salon était que chacun y écrive l'état d'avancement de sa tâche à intervalle régulier. En revanche, certaines personnes travaillent en dehors des heures ADE, et donc des heures "communes" de travail, il a donc été décidé que ce salon serait textuel et non vocal. Ainsi, chaque personne pouvait faire état de ses avancements et de ses blocages. Et, en cas de blocage, les autres pouvaient essayer d'aider.

#### Projet Client / Projet Serveur

Pour créer le jeu Hoax, deux projets distincts ont été développés, soit le Client et le serveur. Le fait qu'il y ait eu deux projets a permis une répartition simple des tâches, en faisant notamment du pair programming côté client avec la mise en place des IHM et l'implémentation des contrôleurs et le serveur, soit 3 équipes de pair programming.

### Mise en place des users stories

En fonction des fonctionnalités définies dans le sprint 0, les users stories ont été mises en place en se mettant à la place de l'utilisateur et en définissant toutes les actions que l'utilisateur souhaite pouvoir faire. Pour ce faire, l'équipe a appliqué le modèle : en tant que * je souhaite pouvoir * afin de *. 

Une fois définies, les users stories similaires ont été regroupées et unifiés si elles traitent le même sujet. Ces users stories sont dans le product backlog du projet. 

#### Définition des priorités

La méthode MoSCoW a été utilisé afin de prioriser les user stories. L'équipe s'est réunie et à débattu sur les différentes fonctionnalités à développer. En utilisant cette méthode, les différentes users stories ont été reclassées par priorités. Les user stories prioritaires sont celles qui ont été considérées vitales pour avoir une application fonctionnelle minimale. Par exemple, l'user story : en tant qu'utilisateur je veux pouvoir annoncer un personnage pour pouvoir récupérer des ressources en utilisant son privilège; est en MUST car cette dernière est primordial pour le jeu. À contrario la traduction de l'application en anglais est une option et donc un COULD. 

### Fonctionnalités de l'application

Voici les users stories qui ont été définies en début de projet et leur sprint correspondant: 

![User stories : gagner ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_win.png)

* En tant que dernier joueur en vie, je veux gagner la partie, afin de ne pas jouer tout seul indéfiniment.

Cette user story a été réalisée et finit au cours du sprint. 
L'objectif de cette tâche est de mettre en place le mécanisme permettant de gagner et perdre une partie.  

![User stories : dénoncer ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_denounce.png)

* En tant que joueur, je veux pouvoir dénoncer un joueur afin de l’éliminer


Cette user story a été réalisée de façon minimale dans le sprint 1. Pour ce premier sprint, l'objectif est de pouvoir effectuer une partie sans aucune vérification pour la dénonciation. C'est dans le sprint 2 que les véirifications sur la dénonciation ont été mises en place.  

![User stories : consulter ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_consult.png)

* En tant qu’utilisateur je veux pouvoir recevoir un personnage et le consulter.

Cette user story a été réalisée et finie au cours du sprint. Chacun des joueurs s'est vu attribuer un personnage qui lui était visible sur l'Interface Homme-Machine. 

![User stories : rejoindre ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_join.png)

* En tant qu’utilisateur, je souhaite pouvoir rejoindre une partie afin de pouvoir jouer avec d’autres joueurs.

Cette user story a été réalisé et finit au cours du sprint. Plusieurs joueurs peuvent rejoindre une partie et donc pouvoir jouer au jeu par la suite. 


L'ensemble des users stories du sprint 1, sont implémentées côté client et côté serveur.

----------------------------------------------


![User stories : sprint 2 ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_sprint2.png)

* En tant qu’utilisateur, je veux pouvoir connaître le nombre de ressources de chaque joueurs (moi inclu) ainsi que le nombre disponible à la banque afin de choisir quel privilège utiliser.

Cette user story a été réalisée au niveau de l'Interface Homme-Machine pour le sprint 2, et a subit des correctif lors du sprint 3.

Le sprint 2 comporte d'autres users stories qui sont :

* Utiliser une immunité afin de protéger mes ressources : COULD
* Contredire un autre joueur : SHOULD
* Enquêter sur un joueur : MUST
* Annoncer un rôle pour utiliser son privilège : MUST


Ces dernières ont été commencées à ce sprint, mais n'ont pas eu le temps d'être implémentées côté client. En effet, durant ce sprint un refactoring côté client a été effectué. Le sprint 3 a permis l'implémentation de ces users stories. En revanche, ces fonctionnalités ont été développées côté serveur.

---------------------------------------------------

Les quatre users stories suivantes ont débuté lors du sprint 2 et terminé au sprint 3. 

![User stories : immunite ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_imunity.png)

* En tant que joueur je veux pouvoir utiliser une immunité afin de protéger mes ressources. 


![User stories : enquêter ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_investigate.png)

* En tant que joueur je veux enquêter sur un joueur afin d’essayer de le démasquer.

![User stories : annoncer ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_announce.png)

* En tant que joueur je veux pouvoir annoncer un personnage afin d’utiliser un de ses privilèges.

![User stories : contredire ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_contradict.png)

* En tant que joueur je veux pouvoir contredire un autre joueur afin de bloquer l’utilisation de ce personnage.


Les users stories suivante ont toutes été planifiées et terminées pour le sprint 3.

![User stories : créer ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_creategame.png)

* En tant qu'hôte de partie, je veux pouvoir créer une partie publique ou privée, et  inviter des joueurs afin de pouvoir choisir avec qui jouer (inviter des amis).


![User stories : historique ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_history.png)

* En tant qu’utilisateur je veux pouvoir voir l’historique des actions du jeu.


![User stories : rejoinde partie specifique ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_join_specific.png)

* En tant qu’utilisateur, je souhaite pouvoir rejoindre une partie spécifique afin de jouer avec des amis ou d’autres personnes.

![User stories : voir règles ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_viewrules.png)

* En tant qu’utilisateur, je souhaite pouvoir m’informer sur les règles afin de les connaître en cas de doute.

En cours de sprint, de nombreuses autres fonctionnalités ont été suggérées tel que par exemple permettre d'utiliser l'application dans d'autres langues que la langue française. 

![User stories : Langues multiples ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_multilang.png)

### Gestion du git

Pour la gestion de notre git l'équipe s'est mis d'accord sur le fait d'assigner les milestones (Sprint 1, Sprint2, Sprint3) et les labels (Documentation, Dev Task, To Do, Doing) correspondant aux tâches pour faciliter la gestion des tâches. 

Si l'un des membres de l'équipe possède une nouvelle idée pour l'application, il peut créer l'issue correspondante et la mettre dans le product backlog. De ce fait, si l'un des membres a fini ses tâches et qu'il ne reste plus de tâches pour le sprint courant, il peut donc en prendre une dans le backlog en privilégiant les plus prioritaires. 

Il y a une branche associé à chaque tâche qui est en cours d'implémentation. Une fois la tâche finie, une demande de merge request vers la branche ```Dev``` doit être faite. La merge request ne pourra être effectué que lorsqu'au moins deux membres de l'équipe ont approuvé le travail. Cela permet de vérifier, étudier le travail des autres, mais aussi de donner un feedback. La branche ```Dev``` et ```master``` doivent contenir du code stable.

La branche ```master``` est protégée, c'est-à-dire qu'aucun push n'est possible depuis la branche. Ainsi, pour y intégrer des éléments, il faut créer une merge request qui devra être approuvée par le créateur du projet. Ainsi, la branche master n'est actualisée qu'à chaque fin d'itération et permet d'avoir une application stable et fonctionnelle. 


### Arborescence du projet Git

Voici l'arborescence du git.
![Arborescence du projet Git](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/git_folders.png)

* *Client* contient le code source de l'application client.
* *Serveur* contient le code source de l'application serveur.
* *livrables* contient les rapports en pdf, ainsi que les JARs exécutables des deux applications.
* *resources* contient les images, maquettes, diagrammes utilisés dans les applications ou dans la documentation. On y trouve aussi les versions editables(.md pour les rapports, .pu pour les diagrammes).

## Architecture et conception

Cette partie contient les explications sur l'architecture et les choix technique ainsi que les différents documents de conceptions qui représentent l'état du projet.  
Le projet est développé en technologie objet **Java**. On retrouve donc certains patrons de conception classique tel que MVC, Strategie, etc. Ils seront cités lors de leurs utilisations.  
Hoax est un jeu qui se veut multijoueur, chaque joueur a un client qui lui permet d'interagir avec les autres joueurs par l'intermédiaire du serveur et de représenter l'état courant de la partie. C'est le serveur qui calcule l'état courant.  

### Interaction Client-Serveur

Les clients se chargent de traduire la volonté de l'utilisateur en une instruction précise pour le serveur. Ils transmettent l'instruction grâce au [protocole de communication d'Hoax](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Protocole_Hoax.pdf). Le serveur est chargé de refuser les instructions invalides, et de calculer le résultat des instructions valides. Une fois le nouvel état de la partie calculé, le serveur va signaler aux clients de se mettre à jour. Ainsi, le serveur conserve les données complètes des parties, mais les clients reçoivent le minimum d'information nécessaire pour présenter le jeu à l'utilisateur.  
Toutes les trames possibles entre un client et un serveur sont expliquées dans le [protocole de communication d'Hoax](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Protocole_Hoax.pdf).  
![Séquence d'échange clients-serveur](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/DS_accuse.png)

### UML
Cette partie contient les diagrammes UML généraux du jeu, notament les cas d'utilisations et d'activité.
#### Diagramme de cas d'utilisation

![Diagramme de cas d'utilisation](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/UseCase.png)

Chaque joueur peut *dénoncer un autre joueur*, *mener une enquête* s'il possède le nombre de ressources nécessaire et *annoncer un personnage*. Chacune de ces actions est réalisable une fois par tour. De plus, pour enquêter le joueur doit posséder au moins 1 de chaque ressource. Pour annoncer un personnage, il faut une majorité d'approbation. En effet, lorsqu'un joueur annonce un personnage, les autres joueurs doivent *voter pour savoir s'ils acceptent cette déclaration*. 

Pour que ces actions soient explicites, chacune d'entre elle à été détaillée par un diagramme d'activité.

#### Diagrammes d’activité

Les actions étant complexes, les diagrammes d'activité sont là pour les **détailler** au mieux. 
D'une certaine manière, cela permet de **raffiner** le problème posé par chaque action.  

##### Annoncer un personnage
![Diagramme d'activité : Annoncer un rôle](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/AD_Announce_Character.png)

Un joueur peut annoncer être un personnage afin d'utiliser le ou les privilèges du personnage. Les autres joueurs peuvent voter contre l'utilisation de ce personnage, au risque de perdre s'ils ont eu tort. Certains privilèges donnent lieu à des vols de ressources et les autres joueurs peuvent proclamer une immunité s'ils sont la cible de ce vol. 

##### Dénoncer un personnage

![Diagramme d'activité : Dénoncer un joueur](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/AD_Denounce_Player.png)

Un joueur peut dénoncer un autre joueur et espérer l'éliminer s'il a raison et récupérer ses ressources. Si ce dernier à tort, alors il sera éliminé. Ainsi, à chaque dénonciation il y a un joueur qui est éliminé. Il faut préciser que les rôles manipulés pendant la dénonciation sont gardés secret. C'est à dire qu'en cas d'élimination d'un joueur suite à une dénonciation, son rôle n'est pas communiqué aux autres. Ainsi, lors de la dénonciation, seuls 2 joueurs sont au courant du rôle accusé : le dénonciateur et le dénoncé. Si le dénonciateur se trompe, alors il est éliminé et son rôle n'est pas communiqué. 

##### Enquêter

![Diagramme d'activité : Enquêter](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/AD_Investigate.png)

Afin de connaître le rôle des autres joueurs, le joueur peut effectuer des enquêtes s'il possède au moins 1 ressource de chaque type. Après avoir reçu un message "Investigate" de la part du Client, le serveur envoie le résultat de l'enquête soit le nom du personnage du joueur ciblé, mélangé avec d'autres noms de personnages. 


### Client
Le client est chargé de réagir aux messages du serveur, de tenir à jour le modèle minimal qu'il possède et de présenter l'interface de jeu.  
Il devra aussi traduire les saisies utilisateurs en instruction pour le serveur.

#### MVC et paquetages
![Différents MVC présentés en CPO](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/MVC_CPO.png)  
Le client respecte le patron MVC avec modèle passif.  
Dans le paquetage **Model** se trouvent les structures de donnée avec les méthodes de calcul.  
Dans le paquetage **View** se trouvent toutes les interfaces graphique. Elles utilisent la technologie SWING.  
Dans le paquetage **Controller** se trouve les classes d'interactions, elles envoient des messages depuis les clients vers le serveur, reçoivent et traite les messages en provenance du serveur et mettent à jour la vue et le model.  

Nous pouvons ainsi modéliser les interactions client-serveur comme suie.  
![Interactions Client-serveur vue depuis le client](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Architecture.png)  

#### Diagramme de classe
Voici les différents diagrammes de classe du client. Ils ont été générés dynamiquement grâce à l'outil [objectaid](https://www.objectaid.com/). 

Le premier diagramme présente les classes importantes épuré de toutes leurs méthodes et attributs. Il met en évidence les relations entre ces dernières et permet de les situer dans leurs paquetages.   
On remarque l'implémentation du patron MVC cité précédemment.   
![Diagramme de classe coté client : Global ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_Global_dc.png)  

![Diagramme de classe coté client : Network](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_network_dc.png)

Le paquetage Network offre une interface pour faciliter les communications entre clients et serveur. 
On peut ouvrir une connexions sur le serveur avec un pseudonyme unique, envoyer un message au serveur, ainsi que traiter les messages et erreurs reçus. Pour pouvoir traiter ces communications, il faut implémenter *ServerEventHandler*.  
On remarque l'utilisation du **patron singleton** pour instancier le *NetworkClient*.

![Diagramme de classe coté client : Homepage](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_Homepage_dc.png)

La page d'accueil est l'état le plus simple. On a une interface qui présente les règles et qui obtient le pseudo du joueur. 
Quand on clique sur jouer, le contrôleur prends la main et connecte l'utilisateur au serveur, l'amenant a la liste des parties.  
Le *Handler* implémente l'interface *ServerEventHandler* permetant de traiter les messages envoyés par le serveur.  
On remarque qu'a cette étape, il n'y a pas de modèle. C'est normal, le modèle n'est qu'une chaîne de caractère correspondant au pseudonyme de l'utilisateur, pas besoin de classe.

![Diagramme de classe coté client : GameList](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_GameList_dc.png)

Une liste de partie possède presque la même architecture qu'une page d'accueil pour la vue et le contrôleur. Chaque *action listener* donne lieu à une sous-classe dans la vue. *GameListTableModel* est fournis à la table pour pouvoir manipuler la liste de **GameList**, .
*GameListTableModel* correspond à notre propre *TableModel*. Il est nécessaire dans le cas de la liste des partie puisque le modèle par défaut offert par Swing ne permet pas de faire toutes les manipulations souhaitées.  
Les données se trouvent dans *GameListInfo*, mit en liste par *GameList*.

![Diagramme de classe coté client : Lobby](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_Lobby_dc.png)

Le lobby est simple à manipuler, car il nécessite seulement des chaînes de caractère pour modéliser le nom d'hôte et les noms des joueurs.  



![Diagramme de classe coté client : PlayerBoard](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Client_Player_Board_DC.png)
*PlayerBoard* est de loin la classe la plus **riche et remplie** de notre projet. Elle inclut une classe par action listener. 



 Le contôleur est divisé en 2 parties, le *Querier* chargé de transformer la saisie de l'utilisateur en message pour le serveur, ainsi que le *Handler* qui lui reçoit et traite les messages du serveur, notifie la vue et met a jour le modèle. 
Le modèle est constitué d'une Game et des joueurs. Ces 2 classes implémentent l'**interface** ResourcesOwner qui permet de gérer la possession de ressource. En effet, la partie possède des ressources, la banque. 
On utilise des **énumérations** pour les types de ressources et pour les caractères. 
On remarque l'utilisation du **patron stratégie** pour l'implémentation des privilèges.



### Serveur

Le serveur interagit avec le client et lui communique les informations concernant l'état du jeu. Pour ce projet, un protocole d'envoi de messages a été défini dans le [protocole de communication d'Hoax](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Protocole_Hoax.pdf). Le serveur répondra en fonction des demandes du client.

#### Diagrammes de classe
La partie serveur de notre application possède de nombreuses classes, c'est pourquoi il a été décidé de découper le diagramme en plusieurs parties.

![Diagramme de classe : Le jeu côté serveur ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Game_server_DC.png)

Ce diagramme représente la partie Jeu côté serveur. Il y a le jeu, le jeu en cours et un gestionnaire de tour. En cours de jeu, il est également possible de voter et de déclarer une immunité. Le lobby correspond à la salle d'attente du jeu. C'est le serveur qui crée le jeu et attribut un identifiant à ce dernier. Il est possible d'avoir plusieurs jeux en cours, chacun de ces jeux possède donc un identifiant et leurs propres joueurs. 


![Diagramme de classe : Les joueurs côté serveur ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Player_server_DC.png)

Les joueurs possèdent des ressources, et chacun des joueurs possède son propre personnage.
La banque possède un nombre limité de ressources. Les joueurs peuvent acquérir des ressources en utilisant le privilège de leur personnage.  


![Diagramme de classe : Les privilèges côté serveur ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Privilege_server_DC.png)

Selon les messages envoyés au serveur, il exécutera le privilège concerné et communiquera les informations au Client. Le fichier ```livrables/Protocole Hoax.pdf``` présente le protocole qui est mis en place pour la communication Client-Serveur, dont les types de message utilisé pour effectuer les privilèges.


![Diagramme de classe : La liste des jeux côté serveur ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/GameList_server_DC.png)

Ce diagramme représente la liste des jeux. Cette liste ne contient pas d'objet Game en effet, cette dernière contient un objet contenat l'id du jeu, le titre de la partie, le nombre de joueur présent, et le status du jeu. C'est le serveur qui communique l'id du jeu, en effet, c'est lui qui gère la création d'un jeu et qui attribue un identifiant à chaque jeu crée. 

![Diagramme de classe : La classe compartiment côté serveur ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/diagramme/Compartement_DC.png)

Un compartiment est une zone du serveur où les utilisateurs peuvent entrer ou sortir. Si un utilisateur rejoint un autre compartiment, il va automatiquement partir du compartiment auquel il était affecté. En effet, un utilisateur peut appartenir à un seul compartiment, tandis qu'un compartiment possède plusieurs utilisateurs. De cette façon, dès que le serveur reçoit un message, il peut récupérer le pseudonyme de l'utilisateur. Un jeu Game étend compartiment, un jeu possède un certain nombre de joueurs qui sont affectés uniquement à ce jeu-là et pas à un autre car il est impossible qu'un joueur joue à deux jeux en même temps.



### Maquettes et IHM

Des maquettes ont été réalisées pour pouvoir mettre en place l'IHM de l'application.
Au cours des sprints, des modifications ont été effectuées par rapport aux maquettes qui ont été définies.

#### Accueil

![Maquette : Page d'accueil](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/page_accueil.png)

La page d'accueil permettra au joueur de rejoindre un lobby en attendant d'autres joueurs, il pourra également consulter les règles au préalable. En revanche, pour pouvoir jouer le joueur devra renseigner un pseudo.

![Screen : Page d'accueil](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_page_accueil.png)

L'IHM de la page d'accueil correspond bien à la maquette qui avait été définie. Il est possible de consulter les règles et il est bien indiqué à l'utilisateur qu'il doit saisir son pseudonyme avant de pouvoir jouer.

#### Lobby

![Maquette : Attendre la partie](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/attendre_partie.png)

Lorsque des joueurs rejoignent une partie, ils doivent attendre que l'hôte de la partie la démarre. Les joueurs peuvent quitter la partie à tout moment. 
De même, si l'hôte quitte la partie, un nouvel hôte sera déclaré. 

![Screen : Attendre la partie](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_attendre_partie.png)

Là aussi l'IHM correspondante correspond bien à la maquete qui avait été définie.

![Maquette : Accepter la partie (Client du joueur hôte)](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/accepter_partie.png)

Les joueur vont attendre dans un lobby, seul le joueur hôte pourra lancer la partie. Tant qu'il n'y a pas au minimum 3 joueurs, la partie ne pourra pas être lancée. 

![Screen : Accepter la partie (Client du joueur hôte)](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_accepter_partie.png)

De même, l'IHM implémentée reste fidèle à la maquette conçue.

![Maquette : Liste des parties](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/parties.png)

Les joueurs peuvent voir la liste des parties en cours, et rejoindre les parties qui ne sont pas encore commencées. Une partie peut soit être publique et être accessible par tout le monde, soit privée et donc accessible uniquement si l'on détient le mot de passe correspondant.

![Screen : Liste des parties](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_parties.png)

L'IHM diffère légèrement de la maquette, seul le titre, le nombre de joueurs et le status de la partie ont été implémentés. 


![Maquette : Créer la partie](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/creer_partie.png)

Un joueur peut créer une partie, cette dernière sera affichée aux autres joueurs dans la liste des parties. 

![Screen : Créer la partie](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_creer_partie.png)

Là aussi l'IHM diffère quelque peu, la possibilité d'affecter un mot de passe à une partie et donc d'avoir des parties privées n'a pas été implémentée. 

#### Interface Joueur

![Maquette : Tour du joueur courant](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/tour_joueur.png)

Le joueur courant peut effectuer diverses actions. Il peut consulter les rôles des personnages du jeu et donc voir les actions que peut effectuer son personnage, annoncer un personnage, dénoncer un autre joueur et enquêter. Il a également la possibilité de revoir les rôles qui ont été interdits à ses adversaires.

![Screen : Tour du joueur courant](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/screen/screen_tour_joueur.png)

Pour l'IHM du tour d'un joueur, on retrouve le tableau des autres joueurs, l'historique des actions, les ressources disponibles à la banque. En revanche, contrairement aux maquettes, il a été décidé d'uniquement consulter sa propre grille de personnage disponible. 

![Maquette : Tour d'un autre joueur](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/Maquettes/tour_autre_joueur.png)

Les joueurs qui ne sont pas le joueur courant du tour dispose de la même interface précédente, mais ne peuvent pas effectuer d'action. 


## Historique des sprints
Cette partie contient l'historique du projet, itération par itération.

### Sprint 1

#### Objectif sprint 1

Pour ce premier sprint, notre objectif était de développer les fonctionnalités nécessaires pour pouvoir joueur une partie avec le minimum requis. C'est-à-dire pouvoir lancer une partie, la gagner ou la perdre, et de dénoncer un joueur. Nous avons également ajouté l'option de passer son tour.  

#### Conception 

Pour le sprint 0, nous nous sommes consacré à la conception afin d'avoir une vue plus globale sur le projet. Nous avons réalisé un diagramme des cas d'utilisation, des diagrammes d'activité pour chacune des actions possibles, un diagramme d'analyse ainsi qu'un diagramme de séquence.
Nous avons également réalisé des maquettes pour notre IHM afin d'avoir une idée de ce que nous avions besoin d'afficher.

#### Procédure de tests

Dans ce premier sprint, seul le modèle créé a été testé. Ainsi, seules les classes Game et Player du client ont été testées. Ces tests permettent de connaitre l'intégrité du modèle existant. Le modèle évoluera forcément donc ces tests permettront de garantir l'intégrité de celui-ci.

#### Avancement

A la fin de ce sprint, notre jeu permet bel et bien d'obtenir un rôle auprès du serveur. Et nous pouvons également dénoncer les autres joueurs. Ainsi, nous avons réussi à atteindre les objectifs de ce sprint.

#### Planification Sprint 2

Pour le prochain sprint, nous souhaitons mieux tester les classes que nous avons créé. En effet, le serveur et les contrôleurs ne sont pas testés. Néanmoins, ce n'est pas l'objectif principal du prochain sprint. Il faudrait au moins que le jeu permette d'enquêter et d'annoncer un rôle. 
### Sprint 2

#### Objectif Sprint 2

A la fin du dernier sprint, nous avons remarqué un manque de tests au sein des contrôleurs et du serveur. Ainsi, la création de tests était un des principaux objectifs de ce sprint. L'étape suivante de l'application était la gestion des enquêtes, des ressources et de l'annonce de rôles.  Ainsi, parmi toutes les user stories créées au *Sprint 0*, nous avons sélectionnées les suivantes pour ce sprint :

* Utiliser une immunité afiin de protéger mes ressources : <font color="red">COULD</font>

* Contredire un autre joueur : <font color="red">SHOULD</font>

* Enquêter sur un joueur : <font color="red">MUST</font>

* Annoncer un rôle pour utiliser son privilège : <font color="red">MUST</font>


#### Avancement



A la fin de ce sprint, nous pouvons noter de nombreux avancements. En effet, la partie serveur a notamment la possibilité de gérer les enquêtes, les immunités, les changements de tour, les annonces de rôle et les dénonciations. En revanche, le client n'a pas pu avancer aussi bien. Il a du avoir une restructuration du code. Celui-ci n'était pas adapté à l'achitecture du serveur dû à un manque de compréhension de la part de "l'équipe" client par rapport à la structure globale du projet. Malgré la conception déjà existante, les intéractions n'étaient pas comprise de tous. Ainsi, nous avons détaillé le protocole de communication entre le client et le serveur dans un fichier. De cette manière, tout le monde a compris comment les deux parties du projet devaient fonctionner. Ce fichier contenant le protocole est disponible dans le dossier ```livrables/Protocole Hoax.pdf```. 

Nous pouvons donc noter, que le client est un peu en retard par rapport aux attentes de ce sprint. En revanche, cela est du à un manque de compréhension. Ce retard sera assurément comblé au prochain sprint. 

  #### Tests

Pendant ce sprint, nous avons axés une partie de nos efforts sur les tests. Ainsi, nous avons pu créer des tests supplémentaires. En revanche, pendant le sprint la partie client et la partie serveur se sont accordés pour uniformiser leurs architectures. Ainsi, les tests créés ont du être modifié en fonction de la nouvelle architecture. 

Ces tests ont été assez dur à réaliser. La partie client et la partie serveur étant 2 projets distincts il était impossible de créer des tests reliant les 2. Or, la plupart des fonctionnalités et méthodes dépendent de cette intéraction.

#### Planification Sprint 3

Pour le prochain sprint, nos objectifs sont de compléter le client. Celui-ci devra afficher les images des ressources. De plus, il devra maintenant assurer les affichages comme il faut. Enfin, il doit communiquer les bons messages au serveur en suivant le protocole écrit. Par la suite, il faudrait ajouter des petites fonctionnalités comme l'affichage d'une grille qui fait état de quels personnages sont disponible pour chaque joueurs.

### Sprint 3 

#### Objectif Sprint 3

Pour ce troisième et dernier sprint l'objectif est de finir de compléter le Client afin d'implémenter les dernières fonctionnalités restantes en suivant le protocole défini par le Serveur. Ainsi que de finir les tâches qui avaient été commencées dans le sprint 2.

#### Avancement

Le Client implémente toute les fonctionnalités qui avait été prévues pour ce sprint. 
Ci-dessous l'ensemble des Users stories implémentées pour ce sprint :

![User stories : sprint 3 ](https://gitlab.com/n7_appsn2022/hoax/-/raw/Dev/resources/user_stories/user_stories_sprint3.png)


## Bilan du projet

### Bilan Humain

Chaque membre de l'équipe a pu utiliser les compétences acquises en cours de conception et programmation objet, mais aussi leurs compétences professionnelles. En effet, la plupart des membres de l'équipe travaillent en utilisant des méthodes agiles, dont notamment scrum. L'un des membres de l'équipe a partagé des consignes de bonnes pratiques concernant la gestion du git qu'il appliquait à son travail et cela a servi à tout le monde étant donné que ce sont ces règles qui ont été appliquées. On peut citer le système de merge request qui a été mis en place et la protection de la branche master.

### Bilan technique

L'application Hoax permet de jouer au jeu HOAX avec l'ensemble des fonctionnalités du jeu. Les dates de rendus ont été respectées et le projet a été livré dans les temps.
Les horaires planifiés sur ADE n'ont pas suffi au projet et donc des horaires supplémentaires ont été nécessaires pour développer ce projet. 

En terme de ligne de code le projet avoisine les 7 021 lignes. 

Ce projet a été réalisé à 100% toutes les fonctionnalités de bases qui avaient été prévues sont implémentées.

En perspective d'amélioration, il serait tout à fait envisageable de mettre en place un système de compte et de connexion, de mettre en place la possibilité de jouer en Anglais, Espagnol ou encore Allemand.